from Mihlib import *

def test_uniqueRandint_1():
	prev = uniqueRandint(0, 100)

	for i in range(100):
		value = uniqueRandint(0, 100)
		if prev == value:
			return False
		prev = value
	return True

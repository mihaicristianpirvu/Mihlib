from Mihlib import *

def test_collapse_array_1():
	a = np.zeros((100, 10, 3))
	a[:, :, 1] = 1
	a[:, :, 2] = 2

	a0 = collapse_array(a, 0)
	a1 = collapse_array(a, 0)
	a2 = collapse_array(a, 0)

	check01 = a0.shape == (100, 10)
	check02 = np.sum(a0) == 0
	check11 = a1.shape == (100, 10)
	check12 = np.sum(a1) == 1000
	check21 = a0.shape == (100, 10)
	check22 = np.sum(a2) == 2000
	return check01 == True

def test_squash_array_1():
	a = np.ones((100, 100, 3))
	return squash_array(a, [2, 1]).shape == (10000, 3)

def test_minMax_1():
	a, b = 100, 10
	c, d = 10, 100
	res = minMax(a, b)
	return res[0] == 10 and res[1] == 100 and minMax(a, b) == minMax(c, d)

def test_dict_argmax_1():
	d = {
		"a" : 12,
		"b" : 8,
		(1, 2) : 3,
		"winner" : 99
	}
	result = dict_argmax(d)
	return result == "winner"

def test_getNeighbourIndexes_1():
	pos = (9, 10)
	result = getNeighbourIndexes(pos[I], pos[J])
	count = 0
	for i in range(-1, 2):
		for j in range(-1, 2):
			if i == 0 and j == 0:
				continue
			if result[count] != (pos[I] + i, pos[J] + j):
				return False
			count += 1
	return True

def test_getNeighbouringIndex_2D_1():
	data =  np.zeros((640, 480))
	position = (5, 5)
	Range = (3, 3)

	count = 0
	result = getNeighbouringIndex_2D(data.shape, position, Range)
	for i in range(-1, 2):
		for j in range(-1, 2):
			if result[count] != (position[I] + i, position[J] + j):
				return False
			count += 1
	return True

def test_flrange_1():
	result = flrange(0, 100, 2.5, method="step")
	if len(result) != 41:
		return False

	for i in range(len(result)):
		if result[i] != 2.5 * i:
			return False
	return True

def getStepIndex_1():
	base = 10
	value = 20
	step = 3
	result = getStepIndex(value, base, step)
	return result == 3

def test_isNumber_1():
	return isNumber("1") == False and isNumber(1) == True and isNumber(float(1)) == True \
		and isNumber(1.0) == True and isNumber(np.ones((1), dtype="float")[0])

def test_close_enough_1():
	return close_enough(0.000001, 0.000002) and close_enough(1, 2, eps=1.1)

def test_getIntervalArray_1():
	expected = [0, 64, 128, 192, 256]
	result = getIntervalArray(0, 4, 256)
	if len(result) != len(expected):
		return False
	for i in range(len(result)):
		if result[i] != expected[i]:
			return False
	return True

def test_getIntervalArray_2():
	expected = [0, 3, 6, 10]
	result = getIntervalArray(0, 3, 10)
	if len(result) != len(expected):
		return False
	for i in range(len(result)):
		if result[i] != expected[i]:
			return False
	return True

def test_histogram_1():
	data = np.array([0, 1, 5, 1, 0, 2, 3, 1, 3, 4, 5, 9])
	bins = [0, 1, 3, 9]
	result = histogram(data, bins)
	expected = np.array([2, 4, 5])
	return close_enough(result, expected)

def test_npGetInfo_1():
	data = np.array([2, 2, 2, 2, 2, 2])
	shape, min, max, mean, std = npGetInfo(data)
	return min == 2 and max == 2 and mean == 2 and std == 0 and shape == (6, )

def test_getPlottableHistogramArray_1():
	data = np.array([0, 1, 5, 1, 0, 2, 3, 1, 3, 4, 5, 9])
	bins = [0, 1, 3, 9]
	result_hist = histogram(data, bins)
	result = getPlottableHistogramArray(result_hist)
	expected_result = np.array([0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 2])
	return close_enough(result, expected_result)

def main():
	pass

if __name__ == "__main__":
	main()
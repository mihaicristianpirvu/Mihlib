import numpy as np
from Mihlib import *
from machine_learning.supervised_learning.neural_networks.layers.convolutional_numpy_1 import ConvolutionalLayer
from machine_learning.supervised_learning.neural_networks.layers.convolutional_6for import ConvolutionalLayer \
	as ConvolutionalLayer_6for

def test_ConvolutionalLayer_forward_1():
	input = [
		[
			[0, 0, 0, 0, 0, 0, 0],
			[0, 2, 2, 0, 2, 0, 0],
			[0, 2, 0, 2, 0, 0, 0],
			[0, 1, 0, 2, 2, 0, 0],
			[0, 2, 2, 0, 0, 2, 0],
			[0, 2, 1, 2, 2, 1, 0],
			[0, 0, 0, 0, 0, 0, 0],
		],
		[
			[0, 0, 0, 0, 0, 0, 0],
			[0, 0, 2, 2, 1, 0, 0],
			[0, 0, 1, 1, 2, 1, 0],
			[0, 2, 2, 1, 0, 0, 0],
			[0, 1, 0, 0, 0, 0, 0],
			[0, 2, 2, 2, 2, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
		],
		[
			[0, 0, 0, 0, 0, 0, 0],
			[0, 2, 0, 2, 1, 0, 0],
			[0, 0, 2, 0, 0, 0, 0],
			[0, 0, 1, 0, 1, 0, 0],
			[0, 0, 1, 1, 0, 2, 0],
			[0, 1, 2, 0, 2, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
		],
	]

	weights = [
		[
			[
				[0, 1, 0],
				[-1, 0, 0],
				[1, 0, 1]
			],
			[
				[1, -1, 1],
				[0, 0, 0],
				[-1, -1, 1]
			],
			[
				[1, 0, 0],
				[1, -1, 0],
				[-1, 0, 1]
			]
		],
		[
			[
				[-1, 0, 1],
				[-1, 1, 0],
				[0, 0, -1]
			],
			[
				[0, 1, -1],
				[0, 0, 0],
				[0, -1, 0]
			],
			[
				[0, -1, -1],
				[-1, 1, -1],
				[1, -1, -1]
			]
		]
	]

	biases = np.zeros((2, 1, 1, 1))
	biases[0, 0, 0, 0] = 1
	biases[1, 0, 0, 0] = 0

	output = [
		[
			[2, -5, -3],
			[6, 9, 1],
			[1, 3, 3]
		],
		[
			[2, 0, -4],
			[-7, -1, -4],
			[3, -6, -5]
		]
	]

	input = np.array(input)
	weights = np.array(weights)
	output = np.array(output)
	a = ConvolutionalLayer((3, 7, 7), kernelSize=3, stride=2, numKernels=2)
	a.weights = weights
	a.biases = biases
	input = np.expand_dims(input, axis=0)
	res = a.forward(input)
	return close_enough(res, output)

def test_ConvolutionalLayer_forward_2():
	l = ConvolutionalLayer((1, 5, 5), kernelSize=3, stride=1, numKernels=2)

	input = np.array([
		[
			[3, 2, 1, 5, 2],
			[3, 0, 2, 0, 1],
			[0, 6, 1, 1, 10],
			[3, -1, 2, 9, 0],
			[1, 2, 1, 5, 5]
		]
	])

	weights = np.array([
		[
			[
				[0.3, 0.1, 0.2],
				[0.0, -0.1, -0.1],
				[0.05, -0.2, 0.05]
			]
		],
		[
			[
				[0.0, -0.1, 0.1],
				[0.1, -0.2, 0.3],
				[0.2, -0.3, 0.2]
			]
		]
	])

	expected_result = np.array([
		[
			[-0.05, 1.65, 1.45],
			[1.05, 0.00, -2.0],
			[0.40, 1.15, 0.80]
		],
		[
			[-0.8, 1.1, 2.1],
			[0.6, 1.5, 0.7],
			[0.4, 3.3, -1.0]
		]
	])

	l.weights = weights
	l.biases = np.zeros((2,1))
	input = np.expand_dims(input, axis=0)
	result = l.forward(input)

	return close_enough(result, expected_result)

def test_ConvolutionalLayer_backward_1(): 
	input = np.array(
	[
		[
			[0, 0, 0, 0, 0, 0, 0],
			[0, 2, 2, 0, 2, 0, 0],
			[0, 2, 0, 2, 0, 0, 0],
			[0, 1, 0, 2, 2, 0, 0],
			[0, 2, 2, 0, 0, 2, 0],
			[0, 2, 1, 2, 2, 1, 0],
			[0, 0, 0, 0, 0, 0, 0],
		]
	])

	weights = np.array(
	[
		[
			[
				[0, 1, 0],
				[-1, 0, 0],
				[1, 0, 1]
			]
		]
	])

	biases = np.zeros((1, 1, 1, 1))

	expected_backward_result = np.array(
	[
		[
			[0.0, 1.0, 0.0, -2.0, 0.0, -2.0, 0.0],
			[-1.0, 0.0, 2.0, 0.0, 2.0, 0.0, 0.0],
			[1.0, 4.0, -1.0, 4.0, -4.0, -2.0, -2.0],
			[-4.0, 0.0, -4.0, 0.0, 2.0, 0.0, 0.0],
			[4.0, 2.0, 8.0, -1.0, 2.0, 0.0, -2.0],
			[-2.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
			[2.0, 0.0, 1.0, 0.0, -1.0, 0.0, 0.0]
		]
	])

	expected_g_weights = np.array(
	[
		[
			[
				[-2.0, 20.0, 4.0],
				[-13.0, 16.0, 6.0],
				[ 8.0, 2.0, 8.0]
			]
		]
	])

	expected_g_biases = np.array(
	[
		[4.0]
	])

	l = ConvolutionalLayer((1, 7, 7), kernelSize=3, stride=2, numKernels=1)
	l.weights = weights
	l.biases = biases

	input = np.expand_dims(input, axis=0)
	forward_result = l.forward(input)
	error = forward_result
	error[0, 0, 0, 0] = 1

	backward_result = l.backward(input, error)
	new_weights = l.weights
	new_biases = l.biases
	g_weights = l.gradientWeights
	g_biases = l.gradientBiases

	return close_enough(weights, new_weights) and close_enough(biases, new_biases) and \
		close_enough(g_weights, expected_g_weights) and close_enough(g_biases, expected_g_biases) and \
		close_enough(backward_result, expected_backward_result)

def test_ConvolutionalLayer_backard_2():
	input = np.array(
	[
		[
			[0, 0, 0, 0, 0, 0, 0],
			[0, 2, 2, 0, 2, 0, 0],
			[0, 2, 0, 2, 0, 0, 0],
			[0, 1, 0, 2, 2, 0, 0],
			[0, 2, 2, 0, 0, 2, 0],
			[0, 2, 1, 2, 2, 1, 0],
			[0, 0, 0, 0, 0, 0, 0],
		],
		[
			[0, 0, 0, 0, 0, 0, 0],
			[0, 0, 2, 2, 1, 0, 0],
			[0, 0, 1, 1, 2, 1, 0],
			[0, 2, 2, 1, 0, 0, 0],
			[0, 1, 0, 0, 0, 0, 0],
			[0, 2, 2, 2, 2, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
		],
		[
			[0, 0, 0, 0, 0, 0, 0],
			[0, 2, 0, 2, 1, 0, 0],
			[0, 0, 2, 0, 0, 0, 0],
			[0, 0, 1, 0, 1, 0, 0],
			[0, 0, 1, 1, 0, 2, 0],
			[0, 1, 2, 0, 2, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
		],
	])

	weights = np.array(
	[
		[
			[
				[0, 1, 0],
				[-1, 0, 0],
				[1, 0, 1],
			],
			[
				[1, -1, 1],
				[0, 0, 0],
				[-1, -1, 1],
			],
			[
				[1, 0, 0],
				[1, -1, 0],
				[-1, 0, 1],
			],
		],
		[
			[
				[-1, 0, 1],
				[-1, 1, 0],
				[0, 0, -1],
			],
			[
				[0, 1, -1],
				[0, 0, 0],
				[0, -1, 0],
			],
			[
				[0, -1, -1],
				[-1, 1, -1],
				[1, -1, -1],
			],
		],
	])

	biases = np.zeros((2, 1, 1, 1))
	biases[0 ,0, 0, 0] = 1
	biases[1, 0, 0, 0] = 0

	expected_backward_result = np.array(
	[
		[
			[ -1.0, 1.0, 1.0, -5.0, 4.0, -3.0, -4.0],
			[ -2.0, 1.0, 5.0, 0.0, 7.0, -4.0, 0.0],
			[8.0, 6.0, -11.0, 9.0, -5.0, 1.0, -3.0],
			[1.0, -7.0, -8.0, -1.0, 3.0, -4.0, 0.0],
			[3.0, 1.0, 31.0, 3.0, 10.0, 3.0, 0.0],
			[ -4.0, 3.0, 3.0, -6.0, 2.0, -5.0, 0.0],
			[1.0, 0.0, 1.0, 0.0, 12.0, 0.0, 8.0]
		],
		[
			[1.0, 0.0, -5.0, 5.0, -8.0, -1.0, 1.0],
			[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
			[5.0, -15.0, 28.0, -5.0, 9.0, 2.0, 2.0],
			[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
			[ -5.0, 3.0, -2.0, -17.0, 20.0, -5.0, 9.0],
			[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
			[ -1.0, -4.0, -2.0, 3.0, 0.0, 2.0, 3.0]
		],
		[
			[1.0, -1.0, -6.0, 0.0, -3.0, 4.0, 4.0],
			[0.0, 0.0, -6.0, 5.0, 1.0, -1.0, 4.0],
			[6.0, 6.0, 21.0, 1.0, -4.0, 8.0, 5.0],
			[ 13.0, -13.0, 17.0, -10.0, 6.0, -5.0, 4.0],
			[-12.0, 4.0, 3.0, 7.0, 14.0, 9.0, 10.0],
			[ -2.0, 2.0, 6.0, -9.0, 14.0, -8.0, 5.0],
			[2.0, -3.0, -11.0, 6.0, 1.0, 5.0, 8.0]
		],
	])

	expected_g_weights = np.array(
	[
		[
			[
				[ 6.0, 38.0, 2.0],
				[ -5.0, 37.0, 17.0],
				[ 18.0, 6.0, 12.0]
			],
			[
				[ 11.0, 11.0, 24.0],
				[ 17.0, 19.0, 17.0],
				[-11.0, -2.0, -9.0]
			],
			[
				[ 21.0, 9.0, 13.0],
				[ 19.0, -7.0, 18.0],
				[ -1.0, 11.0, 8.0]
			]
		],
		[
			[
				[-12.0, -20.0, 6.0],
				[-32.0, -18.0, -9.0],
				[ -2.0, -20.0, -14.0]
			],
			[
				[ -9.0, -2.0, -9.0],
				[-28.0, -21.0, -18.0],
				[ -8.0, -11.0, 1.0]
			],
			[
				[ -8.0, -16.0, -11.0],
				[-31.0, 5.0, -14.0],
				[ -1.0, -9.0, -5.0]
			]
		]
	])

	expected_g_biases = np.array(
	[
		[16.0],
		[-23.0]
	])

	l = ConvolutionalLayer((3, 7, 7), kernelSize=3, stride=2, numKernels=2)
	l.weights = weights
	l.biases = biases

	input = np.expand_dims(input, axis=0)
	forward_result = l.forward(input)
	error = forward_result
	error[0, 0, 0, 0] = 1
	error[0, 1, 0, 0] = 1

	backward_result = l.backward(input, error)
	new_weights = l.weights
	new_biases = l.biases
	g_weights = l.gradientWeights
	g_biases = l.gradientBiases
	return close_enough(weights, new_weights) and close_enough(biases, new_biases) and \
		close_enough(g_weights, expected_g_weights) and close_enough(g_biases, expected_g_biases) and \
		close_enough(backward_result, expected_backward_result)

def test_ConvolutionalLayer_complete_1():
	l = ConvolutionalLayer((1, 5, 5), kernelSize=3, stride=2, numKernels=1)

	input = np.array([
		[
			[0.0, 1.0, 2.0, 3.0, 4.0],
			[1.0, 2.0, 3.0, 4.0, 5.0],
			[2.0, 3.0, 4.0, 5.0, 6.0],
			[3.0, 4.0, 5.0, 6.0, 7.0],
			[4.0, 5.0, 6.0, 7.0, 8.0],
		]
	])

	weights = np.array([
		[
			[
				[0.5, 0.5, 0.5],
				[0.5, 0.5, 0.5],
				[0.5, 0.5, 0.5]
			]
		]
	])

	biases = np.array([[0.5]])
	l.weights = weights
	l.biases = biases

	expected_result_forward = np.array([
		[
			[9.5, 18.5],
			[18.5, 27.5]
		]
	])

	input = np.expand_dims(input, axis=0)
	result_forward = l.forward(input)
	errors = np.array([
		[
			[-1.0, 2.0],
			[3.0, 0.0]
		]
	])

	expected_result_backward = np.array([
		[
			[-0.5, -0.5, 0.5, 1.0, 1.0],
			[-0.5, -0.5, 0.5, 1.0, 1.0],
			[ 1.0,  1.0, 2.0, 1.0, 1.0],
			[ 1.5,  1.5, 1.5, 0.0, 0.0],
			[ 1.5,  1.5, 1.5, 0.0, 0.0]
		]
	])

	expected_gradient_weights = np.array([
		[
			[10.0, 14.0, 18.0],
			[14.0, 18.0, 22.0],
			[18.0, 22.0, 26.0]
		]
	])

	expected_gradient_biases = np.array([
		[4.0]
	])

	result_backward = l.backward(input, errors)
	return close_enough(expected_result_forward, result_forward) and \
		close_enough(expected_gradient_weights, l.gradientWeights) and \
		close_enough(expected_gradient_biases, l.gradientBiases) and \
		close_enough(expected_result_backward, result_backward)

##### Regression tests --- compare this implementation with baseline on (6-for) #####

def test_regression_ConvolutionalLayer_forward_1():
	input = np.random.normal(size=(3, 7, 7))
	weights = np.random.normal(size=(2, 3, 3, 3))
	biases = np.random.normal(size=(2, 1, 1, 1))

	l1 = ConvolutionalLayer((3, 7, 7), kernelSize=3, stride=2, numKernels=2)
	l1.weights = weights
	l1.biases = biases

	l2 = ConvolutionalLayer((3, 7, 7), kernelSize=3, stride=2, numKernels=2)
	l2.weights = weights
	l2.biases = biases

	input = np.expand_dims(input, axis=0)
	result1 = l1.forward(input)
	result2 = l2.forward(input)
	return close_enough(result1, result2)

def test_regression_ConvolutionalLayer_backward_1():
	input = np.random.normal(size=(3, 7, 7))
	weights = np.random.normal(size=(2, 3, 3, 3))
	biases = np.random.normal(size=(2, 1, 1, 1))

	l1 = ConvolutionalLayer((3, 7, 7), kernelSize=3, stride=2, numKernels=2)
	l1.weights = weights
	l1.biases = biases
	input = np.expand_dims(input, axis=0)
	error = l1.forward(input)
	error[0, 0, 0, 0] = 1
	res1 = l1.backward(input, error)

	l2 = ConvolutionalLayer((3, 7, 7), kernelSize=3, stride=2, numKernels=2)
	l2.weights = weights
	l2.biases = biases
	error = l2.forward(input)
	error[0, 0, 0, 0] = 1	
	res2 = l2.backward(input, error)

	return close_enough(res1, res2) and close_enough(l1.gradientWeights, l2.gradientWeights) \
		and close_enough(l1.gradientBiases, l2.gradientBiases)

def test_regression_ConvolutionalLayer_complete_1():
	l1 = ConvolutionalLayer((1, 6, 6), kernelSize=3, stride=1, numKernels=1)
	l2 = ConvolutionalLayer_6for((1, 6, 6), kernelSize=3, stride=1, numKernels=1)
	l2.weights = np.copy(l1.weights)
	l2.biases = np.copy(l1.biases)

	# TODO, allow forward and backward of a batch (N>1), not add them one by one
	N = 10
	images = (np.random.rand(N, 1, 6, 6) - 0.5) * 2
	labels = (np.random.rand(N, 1) * 10).astype(int)

	forward_result_1 = l1.forward(images)
	error1 = forward_result_1
	error1[:, 0, 0, 0] = 1
	forward_result_2 = l2.forward(images)
	error2 = forward_result_2
	error2[:, 0, 0, 0] = 1

	backward_result_1 = l1.backward(images, error1)
	backward_result_2 = l2.backward(images, error2)
	return close_enough(forward_result_1, forward_result_2) and close_enough(backward_result_1, backward_result_2)
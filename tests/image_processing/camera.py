from Mihlib import *
import random

def test_camera_0():
	return Camera() != None

def test_camera_project_0():
	c = Camera()
	result = c.project([1, 2, 3])[0]
	return close_enough(result, np.array([1/3, 2/3]))

def test_camera_project_1():
	c = Camera(focalLength = 0.015)
	point = [0.3, 0.4, 3.0]
	result = c.project(point)[0]
	return close_enough(result, np.array([0.0015, 0.002]))

def test_camera_project_2():
	c = Camera()
	big = np.random.normal(size=(100, 3))
	result = c.project(big)
	return result.shape == (100, 2)

def test_camera_3():
	c = Camera(focalLength=0.015, pixelShape=(1e-05, 1e-05), principalPoint=(640, 512))
	result = c.project([0.3, 0.4, 3.0])[0]
	return close_enough(result, np.array([790, 712]))

def test_camera_calibrate_1():
	c1 = Camera(focalLength = 0.015, pose=(HTranslation(1, 2, 3) * HRotationX(30)).matrix)

	# At least 6 points required
	N = randint(6, 100)
	worldPoints = np.random.normal(size=(N, 3))
	cameraPoints = c1.project(worldPoints)

	c2 = Camera()
	c2.calibrateCamera(worldPoints, cameraPoints)
	newCameraPoints = c2.project(worldPoints)

	return close_enough(cameraPoints, newCameraPoints)

def test_camera_triangulation_1():
	c0 = Camera()
	T = getRandomPose()
	c1 = Camera(focalLength = 0.015, pose=T.matrix)
	c2 = Camera(focalLength = 0.030, pixelShape=(1e-05, 1))
	cameras = [c0, c1, c2]

	worldPoint = np.random.normal(size=(1, 3))
	projectedPoints = []
	for camera in cameras:
		projectedPoints.append(camera.project(worldPoint))

	newWorldPoint = solveTriangulation(cameras, projectedPoints)
	return close_enough(worldPoint, newWorldPoint)

def test_camera_computeFundamentalFromParameters_1():
	T1 = getRandomPose()
	T2 = getRandomPose()

	c1 = Camera(focalLength=1, pose=T1.matrix)
	c2 = Camera(focalLength=1, pose=T2.matrix)

	p = [0.5, 0.1, 0.8]
	p1 = c1.project(p, homogeneousResult=True).reshape((3, 1))
	p2 = c2.project(p, homogeneousResult=True).reshape((3, 1))

	fundamental = c1.computeFundamentalFromParameters(c2)
	result = p2.T @ fundamental @ p1
	return np.abs(result) < EPS and np.linalg.matrix_rank(fundamental) == 2 and \
		close_enough(np.linalg.det(fundamental), 0)

# def test_camera_computeFundamentalFromProjection_alt_1():
# 	T1 = getRandomPose()
# 	T2 = getRandomPose()

# 	c1 = Camera(focalLength=1, pose=T1.matrix)
# 	c2 = Camera(focalLength=1, pose=T2.matrix)

# 	p = [0.5, 0.1, 0.8]
# 	p1 = c1.project(p, homogeneousResult=True)
# 	p2 = c2.project(p, homogeneousResult=True)

# 	fundamental = c1.computeFundamentalFromProjection_alt(c2)
# 	result = p1 @ fundamental @ p2.T
# 	return np.abs(result) < 0.05

def test_camera_computeFundamentalFromProjection_1():
	T1 = getRandomPose()
	T2 = getRandomPose()

	c1 = Camera(focalLength=1, pose=T1.matrix)
	c2 = Camera(focalLength=1, pose=T2.matrix)

	p = [0.5, 0.1, 0.8]
	p1 = c1.project(p, homogeneousResult=True).reshape((3, 1))
	p2 = c2.project(p, homogeneousResult=True).reshape((3, 1))

	fundamental = c1.computeFundamentalFromProjection(c2)
	result = p2.T @ fundamental @ p1
	return np.abs(result) < EPS and np.linalg.matrix_rank(fundamental) == 2 and \
		close_enough(np.linalg.det(fundamental), 0)

def test_camera_solveFundamental_1():
	T1 = getRandomPose()
	T2 = getRandomPose()

	c1 = Camera(focalLength=random.random() * 10, pose=T1.matrix)
	c2 = Camera(focalLength=random.random() * 10, pose=T2.matrix)

	N = random.randint(0, 92) + 8
	points = []
	for i in range(N):
		P = np.random.normal(size=(1, 3))
		p1 = c1.project(P)[0]
		p2 = c2.project(P)[0]
		points.append((p1, p2))
	fundamental = solveFundamentalMatrix(points, isotropicScaling=False)

	results = np.zeros((N, ))
	expected = np.zeros((N, ))
	for i in range(N):
		p1 = toHomogeneous(points[i][0])
		p2 = toHomogeneous(points[i][1])
		results[i] = p2.T @ fundamental @ p1

	return close_enough(results, expected) and np.linalg.matrix_rank(fundamental) == 2 and \
		close_enough(np.linalg.det(fundamental), 0)

def test_camera_solveFundamental_2():
	T1 = getRandomPose()
	T2 = getRandomPose()

	c1 = Camera(focalLength=random.random() * 10, pose=T1.matrix)
	c2 = Camera(focalLength=random.random() * 10, pose=T2.matrix)

	N = random.randint(0, 92) + 8
	points = []
	for i in range(N):
		P = np.random.normal(size=(1, 3))
		p1 = c1.project(P)[0]
		p2 = c2.project(P)[0]
		points.append((p1, p2))
	fundamental = solveFundamentalMatrix(points, isotropicScaling=True)

	results = np.zeros((N, ))
	expected = np.zeros((N, ))
	for i in range(N):
		p1 = toHomogeneous(points[i][0])
		p2 = toHomogeneous(points[i][1])
		results[i] = p2.T @ fundamental @ p1

	return close_enough(results, expected) and np.linalg.matrix_rank(fundamental) in (2, 3) and \
		close_enough(np.linalg.det(fundamental), 0)

def test_camera_solveHomography_1():
	T1 = getRandomPose()
	T2 = getRandomPose()

	c1 = Camera(focalLength=random.random() * 10, pose=T1.matrix)
	c2 = Camera(focalLength=random.random() * 10, pose=T2.matrix)

	N = 4
	points = []
	for i in range(N):
		P = np.random.normal(size=(1, 3))
		p1 = c1.project(P)[0]
		p2 = c2.project(P)[0]
		points.append((p1, p2))
	homography = solveHomography(points)

	points = np.array(points)
	res = toHomogeneousData(points[:,0])
	res = np.matmul(homography, reshapeData(res, (3, 1)))
	res = fromHomogeneousData(reshapeData(res, (1, 3)))
	res = reshapeData(res, (2, ))
	return close_enough(res, points[:, 1], eps=1e-3)

def test_camera_getCenter_1():
	T = getRandomPose()
	c = Camera(focalLength=random.random() * 10, pose=T.matrix)
	center = c.getCenter()
	center_alt = c.getCenter_alt()
	return close_enough(center, center_alt)

def test_camera_factorCameraMatrix_1():
	T = getRandomPose()
	c = Camera(focalLength=random.random() * 10, pose=T.matrix)
	K, R, t = c.factorCameraMatrix()
	# TODO, see if i can factor the correct signs, so abs checking is not required.
	return close_enough(c.K, K) and close_enough(c.R, R) and close_enough(c.t, t)
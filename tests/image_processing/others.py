from Mihlib import *

def test_bilinear_interpolation_1():
	# Example from wikipedia
	P1, P2, P3, P4 = (14, 21, 162), (15, 21, 95), (15, 20, 210), (14, 20, 91)
	desired = (14.5, 20.5)
	result = bilinear_interpolation([P1, P2, P3, P4], desired)
	return result == 150.5

def test_intersection_over_union_1():
	a = np.ones((3, 3))
	b = np.ones((3, 3))

	result = intersectionOverUnion(a, b)
	return result == 1

def test_intersection_over_union2():
	a = np.ones((3, 3))
	b = np.ones((3, 3))
	a[1, 1] = 0
	b[1, 0] = 0 

	result = intersectionOverUnion(a, b)
	expected_result = 7 / 9
	return abs(result - expected_result) < EPS

def test_intersection_over_union3():
	a = np.zeros((3, 3))
	b = np.zeros((3, 3))
	a[0, 0] = 1
	b[0, 1] = 1
	result = intersectionOverUnion(a, b)
	return result == 0

def test_intersection_over_union4():
	a = np.zeros((3, 3))
	b = np.zeros((3, 3))
	a[0, 0] = 1
	a[0, 1] = 1
	b[0, 1] = 1
	result = intersectionOverUnion(a, b)
	return result == 1 / 2

# Example from wikipedia
def test_computeIntegralImage_1():
	image = np.array([[31, 2, 4, 33, 5, 36], [12, 26, 9, 10, 29, 25], [13, 17, 21, 22, 20, 18], \
		[24, 23, 15, 16, 14, 19], [30, 8, 28, 27, 11, 7], [1, 35, 34, 3, 32, 6]])
	expectedIntegralImage = np.array([[31, 33, 37, 70, 75, 111], [43, 71, 84, 127, 161, 222], \
		[56, 101, 135, 200, 254, 333], [80, 148, 197, 278, 346, 444], [110, 186, 263, 371, 450, 555], \
		[111, 222, 333, 444, 555, 666]])

	integralImage = computeIntegralImage(image)
	return close_enough(expectedIntegralImage, integralImage)

# Example from wikipedia
def test_getIntegralImageValue_2():
	image = np.array([[31, 2, 4, 33, 5, 36], [12, 26, 9, 10, 29, 25], [13, 17, 21, 22, 20, 18], \
		[24, 23, 15, 16, 14, 19], [30, 8, 28, 27, 11, 7], [1, 35, 34, 3, 32, 6]])
	integralImage = computeIntegralImage(image)

	value = getIntegralImageValue(integralImage, (3, 2), (4, 4))
	return value == 111
from Mihlib import *
from skimage.feature import hog

def test_HogFeature_norm_1():
	input = np.random.normal(size=(1280, 640))
	norm = "L1"
	sk_hog = hog(input, orientations=9, pixels_per_cell=(8, 8), cells_per_block=(2, 2), block_norm=norm)
	cpp_hog = HogFeature(input, numOrientations=9, pixelsPerCell=(8, 8), cellsPerBlock=(2, 2), normalization=norm)
	return close_enough(sk_hog, cpp_hog)

def test_HogFeature_norm_2():
	input = np.random.normal(size=(1280, 640))
	norm = "L1-sqrt"
	sk_hog = hog(input, orientations=9, pixels_per_cell=(8, 8), cells_per_block=(2, 2), block_norm=norm)
	cpp_hog = HogFeature(input, numOrientations=9, pixelsPerCell=(8, 8), cellsPerBlock=(2, 2), normalization=norm)
	return close_enough(sk_hog, cpp_hog)

def test_HogFeature_norm_3():
	input = np.random.normal(size=(1280, 640))
	norm = "L2"
	sk_hog = hog(input, orientations=9, pixels_per_cell=(8, 8), cells_per_block=(2, 2), block_norm=norm)
	cpp_hog = HogFeature(input, numOrientations=9, pixelsPerCell=(8, 8), cellsPerBlock=(2, 2), normalization=norm)
	return close_enough(sk_hog, cpp_hog)

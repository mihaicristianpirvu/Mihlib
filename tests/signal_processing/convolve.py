from Mihlib import *

def test_convolve_3d_classic_1():
	input = np.ones((5, 5, 1))
	filter = np.ones((3, 3)) / 9

	input[0, :, :] = 2
	input[4, :, :] = 2
	input[:, 0, :] = 2
	input[:, 4, :] = 2

	result = convolve_3d_classic(input, filter)

	expected_result = np.array([
		[ 2.0, 2.0, 2.0, 2.0, 2.0],
		[ 2.0, 1.55555556, 1.33333333, 1.55555556, 2.0],
		[ 2.0, 1.33333333, 1.0, 1.33333333, 2.0],
		[ 2.0, 1.55555556, 1.33333333, 1.55555556, 2.0],
		[ 2.0, 2.0, 2.0, 2.0, 2.0]
	]).reshape((5, 5, 1))

	return close_enough(result, expected_result)
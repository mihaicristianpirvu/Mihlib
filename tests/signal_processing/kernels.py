from Mihlib import *

def test_identity_kernel_1():
	for i in range(1, 100, 2):
		kernel = identity_kernel(shape=(i, i))
		expected = np.zeros((i, i))
		expected[i // 2, i // 2] = 1
		if not close_enough(kernel, expected):
			return False
	return True

def test_identity_kernel_2():
	input = np.random.uniform(size=(100, 100))

	for i in range(1, 35, 2):
		kernel = identity_kernel(shape=(i, i))
		result = convolve_3d_classic(input, kernel)

		if not close_enough(input, result):
			return False
	return True

def test_box_kernel_1():
	for i in range(1, 35, 2):
		kernel = box_kernel(shape=(i, i)) - 1
		if np.abs(np.sum(kernel)) > EPS:
			return False
	return True

def test_box_kernel_2():
	input = np.ones((11, 11))
	input[0, :] = 0
	input[-1, :] = 0
	input[:, 0] = 0
	input[:, -1] = 0
	kernel = box_kernel(shape=(3, 3))
	result = convolve_3d_classic(input, kernel)

	expected_result = np.ones((11, 11)) * 9
	expected_result[1, :] = 6
	expected_result[-2, :] = 6
	expected_result[:, 1] = 6
	expected_result[:, -2] = 6
	expected_result[0, :] = 0
	expected_result[-1, :] = 0
	expected_result[:, 0] = 0
	expected_result[:, -1] = 0
	expected_result[1, 1] = 4
	expected_result[1, -2] = 4
	expected_result[-2, 1] = 4
	expected_result[-2, -2] = 4
	return close_enough(result, expected_result)

def test_gaussian_1d_kernel_1():
	kernel = gaussian_1d_kernel(sigma=1, normalize=False, shape=(5, ))
	expected = np.array([0.05399097, 0.24197072, 0.39894228, 0.24197072, 0.05399097])

	kernel_normalized = gaussian_1d_kernel(sigma=1, normalize=True, shape=(5, ))
	expected_normalized = np.array([0.05448868, 0.24420134, 0.40261995, 0.24420134, 0.05448868])
	return close_enough(kernel, expected) and close_enough(kernel_normalized, expected_normalized)

def test_gaussian_2d_kernel_1():
	kernel = gaussian_2d_kernel(sigma=1, normalize=False, shape=(5, 5))
	expected_result = np.array([
		[0.00291502, 0.01306423, 0.02153928, 0.01306423, 0.00291502],
		[0.01306423, 0.05854983, 0.09653235, 0.05854983, 0.01306423],
		[0.02153928, 0.09653235, 0.15915494, 0.09653235, 0.02153928],
		[0.01306423, 0.05854983, 0.09653235, 0.05854983, 0.01306423],
		[0.00291502, 0.01306423, 0.02153928, 0.01306423, 0.00291502]
	])
	return close_enough(kernel, expected_result)

# Also check the example in examples/gabor_filter
def test_gabor_2d_kernel_1():
	shape = (5, 5)
	sigma = 4.0
	theta = 0
	Lambda = 10
	gamma = 0.75
	psi = 0

	kernel = gabor_2d_kernel(shape=shape, sigma=sigma, theta=theta, Lambda=Lambda, gamma=gamma, psi=psi)
	expected_result = np.array([
	 [0.25419045-0.78231775j, 0.73088595-0.53101972j, 0.93210249+0.j, 0.73088595+0.53101972j, 0.25419045+0.78231775j],
	 [0.26795476-0.82467994j, 0.77046313-0.55977423j, 0.98257547+0.j, 0.77046313+0.55977423j, 0.26795476+0.82467994j],
	 [0.27270654-0.83930443j, 0.78412616-0.569701j, 1.00000000+0.j, 0.78412616+0.569701j, 0.27270654+0.83930443j],
	 [0.26795476-0.82467994j, 0.77046313-0.55977423j, 0.98257547+0.j, 0.77046313+0.55977423j, 0.26795476+0.82467994j],
	 [0.25419045-0.78231775j, 0.73088595-0.53101972j, 0.93210249+0.j, 0.73088595+0.53101972j, 0.25419045+0.78231775j]
	])
	return close_enough(kernel, expected_result)
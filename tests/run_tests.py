import sys, inspect, os
import importlib.util

# Returns a list of strings with all testing modules.
def getTestModules(path):
	test_modules = []
	for filename in os.listdir(path):
		if filename != "run_tests.py" and filename.endswith(".py"):
			test_modules.append(os.path.join(path, filename))
	return test_modules

# Given a test module, run it's tests (all functions that start with test_)
def runTests(module):
	Str = " - " + module
	Failed = ""
	spec = importlib.util.spec_from_file_location("", module)
	test_module = importlib.util.module_from_spec(spec)
	spec.loader.exec_module(test_module)

	countTests, countPass = 0, 0
	for name, obj in inspect.getmembers(test_module):
		if inspect.isfunction(obj) and name.startswith("test_"):
			countTests += 1
			if obj() == True:
				countPass += 1
			else:
				Failed += name + " "

	Str += " (Passed: " + str(countPass) + "/" + str(countTests) + ")"
	if Failed != "":
		Str += "\n    - Failed: " + Failed
	print(Str)
	return countPass, countTests

# Given a module and a name, run that test if it exists in the module.
def runTest(module, testName):
	spec = importlib.util.spec_from_file_location("", module)
	test_module = importlib.util.module_from_spec(spec)
	spec.loader.exec_module(test_module)

	found, passed = 0, 0
	for name, obj in inspect.getmembers(test_module):
		if inspect.isfunction(obj) and name == testName:
			found += 1
			if obj() == True:
				passed += 1
			break
	return passed, found

def recursivelySearch(path):
	files = os.listdir(path)
	allPass, allTests = 0, 0
	countPass, countTests = 0, 0

	# First run the tests
	modules = getTestModules(path)

	if len(modules) > 0 and len(sys.argv) == 1:
		print("-> Directory:", path)

	for module in modules:
		if len(sys.argv) == 1:
			countPass, countTests = runTests(module)
		else:
			countPass, countTests = runTest(module, sys.argv[1])
		allPass += countPass
		allTests += countTests
		# If already found the wanted test, stop searching (unique name is expected)
		if len(sys.argv) == 2 and allTests > 0:
			print("Test found in: {}".format(module))
			return allPass, allTests

	# Then, go recursively to other directories
	for file in files:
		newPath = path + os.sep + file
		if not file in (".", "..", "__pycache__") and os.path.isdir(newPath):
			countPass, countTests = recursivelySearch(newPath)
			allPass += countPass
			allTests += countTests
	return allPass, allTests

def main():
	assert len(sys.argv) in (1, 2)
	if len(sys.argv) == 1:
		print("Running Mihlib Tests")
	else:
		print("Running specific test: {}".format(sys.argv[1]))

	allPass, allTests = recursivelySearch(os.path.dirname(os.path.realpath(__file__)))
	print("Passed {} out of {}".format(allPass, allTests))

if __name__ == "__main__":
	main()

from Mihlib import *
from statistics.data_mining.prefixspan import *

def test_isSubEvent_1():
	e1=["A", "B", "C"]
	e2=["A", "B"]
	e3=["A", "C"]
	e4=["B", "A"]
	e5=["A", "D"]
	return isSubEvent(e1, e2) and isSubEvent(e1, e3) and isSubEvent(e1, e4) and (not isSubEvent(e1, e5))

def test_isSubEvent_2():
	e1_1 = ["_", "A"]
	e1_2 = ["_", "A"]

	e2_1 = ["A", "B", "C"]
	e2_2 = ["_", "B"]

	e3_1 = ["_", "A"]
	e3_2 = ["A"]

	e4_1 = ["_", "B"]
	e4_2 = ["_", "C"]
	return isSubEvent(e1_1, e1_2) and isSubEvent(e2_1, e2_2) and (not isSubEvent(e3_1, e3_2)) \
		and (not isSubEvent(e4_1, e4_2))

# Tests whether the isEventPrefix function is correctly implemented
def test_isEventPrefix_1():
	e1=["A", "B", "C"]
	e2=["A", "B"]
	e3=["A", "C"]
	return isEventPrefix(e1, e2) and (not isEventPrefix(e1, e3))

def test_getDifferenceEvents_1():
	e1 = ["a", "b", "c", "d"]
	e2 = ["c", "d", "e", "f"]
	return getDifferenceEvents(e1, e2) == ["a", "b"] and getDifferenceEvents(e2, e1) == ["e", "f"]

def test_getFollowingEvent_1():
	ev0_1 = ["A", "C"]
	ev0_2 = ["A"]
	ev0_res = ["C"]

	ev1_1 = ["A", "B", "C"]
	ev1_2 = ["B"]
	ev1_res = ["C"]

	ev2_1 = ["A", "B", "C", "D"]
	ev2_2 = ["A", "B"]
	ev2_res = ["C", "D"]

	return getFollowingEvent(ev0_1, ev0_2) == ev0_res and getFollowingEvent(ev1_1, ev1_2) == ev1_res  \
		and getFollowingEvent(ev2_1, ev2_2) == ev2_res

def test_getFollowingEvent_2():
	ev0_1 = ["A", "B", "C"]
	ev0_2 = ["_", "B"]
	ev0_res = ["C"]
	return getFollowingEvent(ev0_1, ev0_2) == ev0_res

def test_isSequencePrefix_1():
	A=[["a"], ["a", "b", "c"], ["a", "c"], ["d"], ["c", "f"]]
	B=[["a"], ["a", "b", "c"], ["a"]]
	C=[["a"], ["a", "b", "c"], ["c"]]
	return isSequencePrefix(A, B) and (not isSequencePrefix(A, C))

# Taken from paper
def test_isSequencePrefix_2():
	S=[["a"], ["a", "b", "c"], ["a", "c"], ["d"], ["c", "f"]]
	S1=[["a"]]
	S2=[["a"], ["a"]]
	S3=[["a"], ["a", "b"]]
	S4=[["a"], ["a", "b", "c"]]
	S5=[["a", "b"]]
	S6=[["a"], ["b", "e"]]

	return isSequencePrefix(S, S1) and isSequencePrefix(S, S2) and isSequencePrefix(S, S3) and isSequencePrefix(S, S4)\
		and (not isSequencePrefix(S, S5)) and (not isSequencePrefix(S, S6))

def test_isSubSequence_1():
	S1=[ ["A", "B", "C"], ["M"], ["B", "D", "Z"]]
	S2=[ ["A"], ["D", "Z"] ] #True
	S3=[ ["C"], ["M"], ["B", "Z"] ] #True
	S4=[ ["C"], ["B", "Z"],  ["M"] ] #False
	S5=[ ["A", "B", "C", "D"], ["M"], ["D", "Z"]] #False
	S6=[ ["A", "B", "C"], ["Z", "B"]] #True
	S7=[ ["A", "B", "C"], ["M"], ["B", "D", "Z"]] #True
	S8=[ ["A", "B", "C"], ["M"], ["B", "D", "Z", "P"]] #False
	S9=[ ["A", "B", "C"], ["M"], ["B", "D", "Z"], ["P"]] #False
	return isSubSequence(S1, S2) and isSubSequence(S1, S3) and (not isSubSequence(S1, S4)) \
		and (not isSubSequence(S1, S5)) and isSubSequence(S1, S6) and isSubSequence(S1, S7) \
		and (not isSubSequence(S1, S8)) and (not isSubSequence(S1, S9))

# Taken from DM course
def test_isSubSequence_2():
	S1=[ ["A", "B"], ["E"], ["A", "B", "C", "D"] ]
	S2=[ ["A"], ["B", "C"] ]
	S3=[ ["A", "B", "C"] ]
	S4=[ ["A", "B"], ["C"] ]
	return isSubSequence(S1, S2) and (not isSubSequence(S3, S4))

def test_isSubSequence_3():
	S1=[ ["A"], ["A", "B", "C"], ["A", "C"], ["D"], ["C", "F"] ]
	S2=[ ["B", "C"], ["A"] ]
	return isSubSequence(S1, S2)

# Taken from DM Course
def test_getSequenceSupport_1():
	dataSet = { \
		1 : [ ["A"], ["B"], ["C"] ], \
		2 : [ ["A", "B"], ["C"], ["A", "D"] ], \
		3 : [ ["A", "B", "C"], ["B", "C", "E"] ], \
		4 : [ ["A", "D"], ["B", "C"], ["A", "E"] ], \
		5 : [ ["B"], ["E"] ]
	}

	# Sequences of length 1
	seq1_1, seq1_2, seq1_3, seq1_4, seq1_5 = [["A"]], [["B"]], [["C"]], [["D"]], [["E"]]
	seq2_1, seq2_2, seq2_3, seq2_4 = [["A"], ["B"]], [["A"], ["C"]], [["B"], ["C"]], [["B"], ["E"]]
	return getSequenceSupport(dataSet, seq1_1) == 4 and getSequenceSupport(dataSet, seq1_2) == 5 \
		and getSequenceSupport(dataSet, seq1_3) == 4 and getSequenceSupport(dataSet, seq1_4) == 2 \
		and getSequenceSupport(dataSet, seq1_5) == 3 and getSequenceSupport(dataSet, seq2_1) == 3 \
		and getSequenceSupport(dataSet, seq2_2) == 4 and getSequenceSupport(dataSet, seq2_3) == 3 \
		and getSequenceSupport(dataSet, seq2_4) == 3

# Taken from paper
def test_getSequenceSupport_2():
	dataSet = { \
		1 : [ ["a", "b", "c"], ["a", "c"], ["d"], ["c", "f"] ], \
		2 : [ ["_", "d"], ["c"], ["b", "c"], ["a", "e"] ], \
		3 : [ ["_", "b"], ["d", "f"], ["c"], ["b"] ], \
		4 : [ ["_", "f"], ["c"], ["b"], ["c"] ] \
	}

	seq1 = [["a"]]
	seq2 = [["b"]]
	seq3 = [["_", "b"]]
	seq4 = [["c"]]
	seq5 = [["d"]]
	seq6 = [["f"]]

	return getSequenceSupport(dataSet, seq1) == 2 and getSequenceSupport(dataSet, seq2) == 4 \
		and getSequenceSupport(dataSet, seq3) == 2 and getSequenceSupport(dataSet, seq4) == 4 \
		and getSequenceSupport(dataSet, seq5) == 2 and getSequenceSupport(dataSet, seq6) == 2

def test_getSequenceProjection_1():
	A=[ ["A"], ["A", "B", "C"], ["A", "C"], ["D"], ["C", "F"] ]
	B=[ ["B", "C"], ["A"] ]
	return getSequenceProjection(A, B) == [ ["B", "C"], ["A", "C"], ["D"], ["C", "F"] ]

def test_getSequencePostfix_1():
	A=[ ["A"], ["A", "B", "C"], ["A", "C"], ["D"], ["C", "F"] ]
	B=[ ["B", "C"], ["A"] ]
	return getSequencePostfix(A, B) == [ ["_", "C"], ["D"], ["C", "F"] ]

def test_getSequencePostfix_2():
	seq=[ ["_", "B"] ]
	A=[ ["A"], ["A", "B", "C"], ["A", "C"], ["D"], ["C", "F"] ]
	A_res=[ ["_", "_", "C"], ["A", "C"], ["D"], ["C", "F"] ]
	B=[ ["_", "B"], ["D", "F"], ["C"], ["B"]]
	B_res = [ ["D", "F"], ["C"], ["B"] ]

	return getSequencePostfix(A, seq) == A_res and getSequencePostfix(B, seq) == B_res

def main():
	pass

if __name__ == "__main__":
	main()
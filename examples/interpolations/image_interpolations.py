import sys
from Mihlib import *

def main():
	assert len(sys.argv) == 3, "Usage: image_interpolation.py image scale"
	scale = float(sys.argv[2])
	assert scale > 0 and scale < 1
	print("Bilinear interpolation of image to scale:", scale)

	image = readImage(sys.argv[1])
	gray_image = grayScaleImage(image)
	plot_image(gray_image, new_figure=True, title="Original image")

	new_image = bilinear_interpolation_image(gray_image, scale)
	plot_image(new_image, new_figure=True, title="Image rescaled to " + sys.argv[2])

	show_plots(block=True)

if __name__ == "__main__":
	main()

#include "Tensor.hpp"

void example1() {
	std::cerr << "-- Example 1 --\n";
	srand(time(nullptr));
	size_t C = rand() % 11;
	Image3D<int> x(3, 5, C + 1);
	std::cerr << x << "\n";
	// This shouldn't work, because you cannot create a manual Tensor (giving array shapes), but not own the memory,
	//  because you will not be able to reclaim it at the end of the context, nor access it for later deletion.
	// Image3D_<int> y(1, 2, 3);
	// std::cerr << y << "\n";
}

void example2() {
	std::cerr << "\n-- Example 2 --\n";
	int npX[] = {1, 2, 3, 4};
	size_t npShape[] = {2, 2};
	struct NumpyArray npImage = {
		(char *)npX,
		npShape,
		2,
		NumpyArrayType::NP_INT32
	};

	Image3D<int> z(&npImage);
	std::cerr << z << "\n";
	std::cerr << z(0, 1, 0) << "\n";
	z(0, 1, 0) = 5;
	std::cerr << z(0, 1, 0) << "\n";

	Image3D_<int> z2(&npImage);
	std::cerr << z2 << "\n";
	std::cerr << z2(0, 1, 0) << "\n";
	z2(0, 1, 0) = 5;
	std::cerr << z2(0, 1, 0) << "\n";
}

void example3() {
	std::cerr << "\n-- Example 3 --\n";
	Image3D<int> x(99, 99, 3);
	std::cerr << x << "\n";
	x.print({std::make_pair(0, 4), std::make_pair(0, 3), std::make_pair(0, 3)});
	x.fill(5);
	x(0, 0, 0) = 99;
	x(0, 0, 1) = 100;
	x(0, 0, 2) = 101;
	// x(0, 0, 3) = 102; // This should fail with out of bounds assert (in assert-enabled mode)
	x(0, 1, 0) = 102;
	x.print({std::make_pair(0, 4), std::make_pair(0, 3), std::make_pair(0, 3)});
}

void example4() {
	std::cerr << "\n-- Example 4 --\n";
	int npX[] = {1, 2, 3, 4};
	size_t npShape[] = {2, 2};
	struct NumpyArray npImage = {
		(char *)npX,
		npShape,
		2,
		NumpyArrayType::NP_INT32
	};

	Image2D<int> z(&npImage);
	std::cerr << z << "\n";

	Image3D<int> z2(&npImage);
	std::cerr << z2 << "\n";

	int npX2[] = {1, 2, 3, 4, 5, 6, 7, 8};
	size_t npShape2[] = {2, 2, 2};
	struct NumpyArray npImage2 = {
		(char *)npX2,
		npShape2,
		3,
		NumpyArrayType::NP_INT32
	};

	Image3D<int> z3(&npImage2);
	std::cerr << z3 << "\n";

	// Should throw exception for 2D container with 3D array
	// Image2D<int> z4(&npImage2);
	// std::cerr << z4 << "\n";
}

void example5() {
	int x[120] = {0};
	std::array<size_t, 5> shape = {1, 2, 3, 4, 5};

	Tensor<int, false, 5> z(x, shape);
	std::cerr << z << "\n";

	// z(1, 0, 0, 0, 1) = 1; // Should fail in assert-enabled mode
	std::cerr << z(0, 0, 0, 0, 1) << "\n";
	z(0, 0, 0, 0, 1) = 1;
	std::cerr << z(0, 0, 0, 0, 1) << "\n";
	z.fill(5);
	std::cerr << z(0, 0, 0, 0, 1) << "\n";
}

void example6() {
	int x[120] = {0};
	std::array<size_t, 5> shape = {1, 2, 3, 4, 5};
	std::array<size_t, 3> newShape = {2, 3, 20};

	Tensor<int, false, 5> z(x, shape);
	std::cerr << "Original tensor: " << z << " Value: " << z(0, 0, 0, 0, 0) << "\n";
	Tensor<int, false, 3> newZ(z.view(newShape));
	newZ(0, 0, 0) = 5;
	std::cerr << "New tensor: " << newZ << " Value: " << z(0, 0, 0, 0, 0) << "\n";
	// auto badZ = newZ.view(99, 99, 99, 99); // should fail due to bad shape

	newZ.fill(10);
	std::cerr << "Final value: " << x[0] << "\n";
}

void example7() {
	int x[120] = {0};
	std::array<size_t, 2> shape1{120, 1};
	Tensor<int, false, 2> t1(x, shape1);
	std::array<size_t, 2> access1{0, 0};

	std::cerr << t1(access1) << "\n";
	t1(access1) = 1;
	std::cerr << t1(access1) << "\n";
}

int main() {
	// example1();
	// example2();
	// example3();
	// example4();
	// example5();
	// example6();
	example7();
	return 0;
}

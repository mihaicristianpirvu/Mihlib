from Mihlib import *

def integral_image1():
	L = np.array([ [5,2], [3,6] ])
	integral = integral_image(L)
	assert(integral[0][0] == 5 and integral[0][1] == 7 and integral[1][0] == 8 and integral[1][1] == 16)
	print("Integral image 1: \n", integral)

	value = get_integral_image_area(integral, None, (L.shape[X]-1, L.shape[Y]-1))
	print("Sum of all pixels:", value, "\n")

def integral_image2():
	L = np.array([ [5,2,5,2], [3,6,3,6], [5,2,5,2], [3,6,3,6] ])
	integral = integral_image(L)
	print("Integral image 2: \n", integral)

	value = get_integral_image_area(integral, None, (L.shape[X]-1, L.shape[Y]-1))
	print("Sum of all pixels:", value)

	value = get_integral_image_area(integral, (1, 1), (3, 3))
	print("Sum of corner right bottom:", value)

	value = get_integral_image_area(integral, (0, 0), (2, 2))
	print("Sum of middle:", value)

	value = get_integral_image_area(integral, (0, 1), (1, 3))
	print("Sum of right side:", value)

def main():
	integral_image1()
	integral_image2()

if __name__ == "__main__":
	main()
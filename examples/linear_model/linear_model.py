from Mihlib import *
from random import randint
import numpy as np
import matplotlib.pyplot as plt
import sys

def generateRandomPoints(N=200, P1=(0,0), P2=(2, 1)):
	L = []
	while True:
		a, b = randint(-N, N), randint(-N, N)
		D = distance_point_line(P1, P2, (a,b))
		if D < 5:
			if randint(0, 100) < 95:
				L.append((a,b))
		elif D < 15:
			if randint(0, 100) < 75:
				L.append((a,b))
		else:
			if randint(0, 100) < 5:
				L.append((a,b))
		if len(L) == 200:
			break
	return np.array(L)

def main():
	data = generateRandomPoints()
	#data = np.array( [(2104, 399.9), (1600, 329.9), (2400, 369)] )
	plot_points(data)
	N = 2 * np.max(data)
	
	# Linear Ransac
	while True:
		model = ransac_linear_regression(data, K = int(np.sqrt(len(data))), cnt_min=np.sqrt(len(data)))
		if model is not None:
			plot_line(getLineParams(model[0], model[1]), (-N, N), label="Ransac")
			break

	# Least squares method
	model = least_squares_linear_regression(data)
	plot_line(model, (-N, N), label="Least squares")
	
	# Total least squares method
	model = total_least_squares_linear_regression(data)
	plot_line(model, (-N, N), label="Total Least squares")

	# Hough transform
	result = hough_transform_line(data, threshold=20, num_bins=1000)
	if len(result) > 0:
		plot_line( result[0], (-N, N), label="Hough transform")

	# Stohastic Gradient descent
	result = gradient_descent_linear_regression(data, num_iterations=1000, learning_rate=0.00005)
	plot_line(result, (-N, N), label="Stohastic Gradient descent")

	plt.legend()
	plt.show(block=True)

if __name__ == "__main__":
	main()
import sys, os
sys.path.append(os.path.dirname(__file__)) # Windows
import numpy as np
from datetime import datetime
from argparse import ArgumentParser
from sklearn.externals import joblib
from functools import partial

from Mihlib import *

def parseArguments():
	parser = ArgumentParser()
	parser.add_argument("type", type=str, default="train")
	parser.add_argument("dataset", type=str, default="mnist", help="Which dataset to test/train on")
	parser.add_argument("--neuron_count", type=int, default=100)
	parser.add_argument("--trasnfer_function", type=str, default="logistic")
	parser.add_argument("--dataset_path", type=str, default=".")
	parser.add_argument("--num_epochs", type=int, default=2)
	parser.add_argument("--batch_size", type=int, default=1)
	parser.add_argument("--learning_rate", type=float, default=0.01)
	parser.add_argument("--file", type=str, default="nn.pkl", help="File for import a neural network model")
	parser.add_argument("--yy", type=int, default=0, help="Don't ask if to delete old directory, if set to 1.")
	parser.add_argument("--num_items", type=int, default=0, help="If >0, work with just a slice of total input")
	args = parser.parse_args()

	assert args.dataset in ("mnist", "cifar10")
	assert args.type in ("train", "test")
	assert args.trasnfer_function in ("relu", "logistic", "tanh")
	return args

def changeDirectory(directoryName, force):
	import os
	import sys
	import shutil

	print("Changing to working directory:", directoryName)
	if os.path.exists(directoryName):
		if force == False:
			print("Directory", directoryName ,"already exists, files may be overwritten. Confirm [y/n].")
			choice = input().lower().strip()
			if not choice == "y":
				sys.exit(0)
		shutil.rmtree(directoryName)

	os.makedirs(directoryName)
	os.chdir(directoryName)

# Called after each epoch so update the accuracy, nll and confusion matrix of each epoch.
def saveResults(model, epoch, train_set, validation_set, accuracyResults, NLLResults, confusionMatrix):
	train_accuracy, train_error = model.test(train_set[0], train_set[1])
	validation_accuracy, validation_error = model.test(validation_set[0], validation_set[1])
	results = model.fit(validation_set[0])

	print("Epoch %d results. Train Accuracy: %2.6f, Loss: %2.3f. Validation Accuracy: %2.6f, Loss: %2.3f." % (epoch,
		train_accuracy, train_error, validation_accuracy, validation_error))

	accuracyResults[0][epoch] = train_accuracy
	accuracyResults[1][epoch] = validation_accuracy
	NLLResults[0][epoch] = train_error
	NLLResults[1][epoch] = validation_error

	numClasses = confusionMatrix.shape[1]
	validation_labels = np.where(validation_set[1] == 1)[1]
	# Numpy tricks to see how many labels on each result class and label class match, so i don't do a NxN, but CxC
	for i in range(numClasses):
		for j in range(numClasses):
			A = np.logical_and(validation_labels == i, True) * 1
			B = np.logical_and(results == j, True) * 1
			C = np.where((A * B) == 1)[0]
			confusionMatrix[epoch, i, j] = len(C)
	fileName = "model_epoch%d_acc%2.6f.pkl" % (epoch, validation_accuracy)
	model.save(fileName)

def iterationResults(model, epoch, iteration, validation_set):
	accuracy, error = model.test(validation_set[0], validation_set[1])
	print("Iteration %d on epoch %i. Validation Accuracy: %2.6f, Loss: %2.3f." % (iteration, epoch, accuracy, error))

def plotTrainValidationAccuracyLoss(accuracyResults, lossResults, epochsCount):
	trainAccuracy, validationAccuracy = accuracyResults
	ylim=(np.min(validationAccuracy) / 2, 1)
	plot_data(trainAccuracy, label="train", ylim=ylim, xticks=range(epochsCount), \
		ylabel="accuracy", xlabel="epoch", title="accuracy", new_figure=True)
	plot_data(validationAccuracy, label="validation", new_figure=False)
	save_figure("accuracy.png")

	trainLoss, validationLoss = lossResults
	ylim = (np.min(validationLoss) / 2, np.max(validationLoss) + 1)
	plot_data(trainLoss, label="train", ylim=ylim, xticks=range(epochsCount), \
		ylabel="NLL", xlabel="epoch", title="loss", new_figure=True)
	plot_data(validationLoss, label="validation", new_figure=False)
	save_figure("loss.png")

def main():
	np.seterr(all="raise")
	args = parseArguments()
	datasetReader = MNISTReader(args.dataset_path, desiredShape=(1, 32, 32,)) if args.dataset == "mnist" \
		else Cifar10Reader(args.dataset_path, desiredShape=(3, 32, 32))
	depth = 1 if args.dataset == "mnist" else 3

	layers = [
		ReshapeLayer((depth, 32, 32), (32 * 32 * depth,)),
		FullyConnectedLayer((32 * 32 * depth,), (args.neuron_count,), Logistic),
		FullyConnectedLayer((args.neuron_count,), (10,), Identity),
		SoftMaxLayer((10,))
	]

	# layers = [
	# 	ConvolutionalLayer((depth, 32, 32), kernelSize=5, stride=1, numKernels=10, transferFunction=Logistic),
	# 	MaxPoolLayer((10, 28, 28), stride=2),
	# 	ReshapeLayer((10, 14, 14), (14 * 14 * 10,)),
	# 	FullyConnectedLayer((14 * 14 * 10,), (100,), Logistic),
	# 	FullyConnectedLayer((100,), (10,), Identity),
	# 	SoftMaxLayer((10,))
	# ]

	if args.type == "train":
		changeDirectory(args.dataset + "_" + str(args.neuron_count) + "_" + str(args.trasnfer_function), force=args.yy)
		optimizer=GradientDescent(args.learning_rate)
		feed_forward = FeedForward(layers, errorFunction=LogisticError, optimizer=optimizer)
		print(feed_forward)

		numClasses = datasetReader.getNumberOfClasses()
		accuracyResults = np.zeros((2, args.num_epochs))
		NLLResults = np.zeros((2, args.num_epochs))
		confusionMatrix = np.zeros((args.num_epochs, numClasses, numClasses), dtype=int)

		images, labels = datasetReader.getData("train")
		labels = toCategorical(labels, numClasses)
		if args.num_items > 0:
			images = images[0 : args.num_items]
			labels = labels[0 : args.num_items]
		validate_images, validate_labels = datasetReader.getData("validate")
		validate_labels = toCategorical(validate_labels, numClasses)

		saveResultsCallback = partial(saveResults, \
			train_set=(images[0 : len(validate_images)], labels[0 : len(validate_images)]), \
			validation_set=(validate_images, validate_labels), accuracyResults=accuracyResults, \
			NLLResults=NLLResults, confusionMatrix=confusionMatrix)
		iterationResultsCallback = partial(iterationResults, validation_set=(validate_images, validate_labels))
		# iterationResultsCallback2 = partial(iterationResults, validation_set=(images[0:1000], labels[0:1000]))

		now = datetime.now()
		feed_forward.train(images, labels, args.num_epochs, args.batch_size, epoch_callbacks=[saveResultsCallback],
			iteration_callbacks=[iterationResultsCallback], iteration_callbacks_count=None)
		print("Training took:", (datetime.now() - now))

		with open("confusion.txt", "w") as f:
			for i in range(args.num_epochs):
				f.write(str(i) + "\n" + str(confusionMatrix[i]))

		plotTrainValidationAccuracyLoss(accuracyResults, NLLResults, args.num_epochs)

	else:
		feed_forward = joblib.load(args.file)
		print(feed_forward)
	
		now = datetime.now()
		images, labels = datasetReader.getData("test")
		if args.num_items > 0:
			images = images[0 : args.num_items]
			labels = labels[0 : args.num_items]
		labels = toCategorical(labels, datasetReader.getNumberOfClasses())

		accuracy, error = feed_forward.test(images, labels)
		print("Testing took:", (datetime.now() - now))
		print("Accuracy: %2.6f" % accuracy)

if __name__ == "__main__":
	main()

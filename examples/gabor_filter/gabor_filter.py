# gabor_filter.py - Tests done identically to those of:
# https://cvtuts.wordpress.com/2014/04/27/gabor-filters-a-practical-overview/
from Mihlib import *
import sys

def test_gamma(image):
	shape = (25, 25)
	sigma = 4.0
	theta = 0
	Lambda = 10
	gammas = [0.25, 0.5, 0.75, 1.00, 1.25, 1.5]
	psi = 0

	plot_image(image, new_figure=True, axis=(3, 4, 1))
	for i in range(len(gammas)):
		gamma = gammas[i]
		kernel = gabor_2d_kernel(theta=theta, Lambda=Lambda, sigma=sigma, gamma=gamma, psi=psi, shape=shape)
		filtered_image = convolve_2d_cv(image, kernel.real)
		plot_image(filtered_image, show_axis=False, axis=(3, 4, i + 2), title="gamma: " + str(gammas[i]), \
			new_figure=False)

def test_lambda(image):
	shape = (25, 25)
	sigma = 4.0
	theta = 0
	Lambdas = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
	gamma = 0.5
	psi = 0

	plot_image(image, new_figure=True, axis=(3, 4, 1))
	for i in range(len(Lambdas)):
		Lambda = Lambdas[i]
		kernel = gabor_2d_kernel(theta=theta, Lambda=Lambda, sigma=sigma, gamma=gamma, psi=psi, shape=shape)
		filtered_image = convolve_2d_cv(image, kernel.real)
		plot_image(filtered_image, show_axis=False, axis=(3, 4, i + 2), title="lambda: " + str(Lambdas[i]), \
			new_figure=False)

def test_theta(image):
	shape = (25, 25)
	sigma = 4.0
	thetas = [0, 20, 40, 60, 80, 100, 120, 140, 160]
	Lambda = 10.0
	gamma = 0.5
	psi = 0

	plot_image(image, new_figure=True, axis=(3, 4, 1))
	for i in range(len(thetas)):
		theta = thetas[i] / 180 * np.pi
		kernel = gabor_2d_kernel(theta=theta, Lambda=Lambda, sigma=sigma, gamma=gamma, psi=psi, shape=shape)
		filtered_image = convolve_2d_cv(image, kernel.real)
		plot_image(filtered_image, show_axis=False, axis=(3, 4, i + 2), title="theta: " + str(thetas[i]), \
			new_figure=False)

def test_sigma(image):
	shape = (25, 25)
	sigmas = [2.0, 3.0, 4.0, 5.0, 6.0]
	theta = 0
	Lambda = 10.0
	gamma = 0.5
	psi = 0

	plot_image(image, new_figure=True, axis=(2, 3, 1))
	for i in range(len(sigmas)):
		sigma = sigmas[i]
		kernel = gabor_2d_kernel(theta=theta, Lambda=Lambda, sigma=sigma, gamma=gamma, psi=psi, shape=shape)
		filtered_image = convolve_2d_cv(image, kernel.real)
		plot_image(filtered_image, show_axis=False, axis=(2, 3, i + 2), title="sigma: " + str(sigma), new_figure=False)

# different sizes of the kernel will yield (almost) the same result
def test_ksize(image):
	shapes = [(23, 23), (25, 25), (51, 51), (151, 151), (531, 531)]
	sigma = 4.0
	theta = 0
	Lambda = 10.0
	gamma = 0.5
	psi = 0

	plot_image(image, new_figure=True, axis=(2, 3, 1))
	for i in range(len(shapes)):
		shape = shapes[i]
		kernel = gabor_2d_kernel(theta=theta, Lambda=Lambda, sigma=sigma, gamma=gamma, psi=psi, shape=shape)
		filtered_image = convolve_2d_cv(image, kernel.real)
		plot_image(filtered_image, show_axis=False, axis=(2, 3, i + 2), title="k_size: " + str(shape), \
			new_figure=False)

def main():
	image = readImage(sys.argv[1])
	image_gray = image if len(image.shape) == 2 else grayScaleImage(image)

	# reversed order so ksize is first

	test_gamma(image_gray)
	test_lambda(image_gray)
	test_theta(image_gray)
	test_sigma(image_gray)
	test_ksize(image_gray)

	show_plots(block=True)


if __name__ == "__main__":
	main()
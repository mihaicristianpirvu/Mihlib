import numpy as np
import h5py
import joblib
from Mihlib import HogFeature, plot_image, show_plots, normalizeData, getRandomCrops, \
	readImage, getAllCrops, grayScaleImage, addSquares
from sklearn import svm
from argparse import ArgumentParser

def getArguments():
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("dataset_path")
	parser.add_argument("--test_type", default="test_pos")
	parser.add_argument("--file", default="svm.pkl")
	args = parser.parse_args()

	assert args.type in ("train", "test", "test_one_image")
	assert args.test_type in ("test_neg_large_1", "test_neg_large_2", "test_neg_large_3", "test_neg_small", \
		"test_pos", "test_all")
	return args

def randomCropsFromData(data, labels):
	numSamples = 10
	windowSize = (128, 64)
	# N x H x W x D => numSamples * N x H x W x D
	finalData = np.zeros((numSamples * data.shape[0], 128, 64, 3))
	finalLabels = np.zeros((numSamples * data.shape[0], ))
	for i in range(data.shape[0]):
		finalData[i * numSamples : (i + 1) * numSamples] = getRandomCrops(data[i], windowSize, numSamples)
		finalLabels[i * numSamples : (i + 1) * numSamples] = labels[i]
	return finalData, finalLabels

def getDataset(args):
	file = h5py.File(args.dataset_path, "r")
	if args.type == "train":
		finalData, finalLabels = file["data"]["train"][:], file["labels"]["train"][:]
	else:
		if args.test_type == "test_pos":
			finalData, finalLabels = file["data"]["test_pos"][:], file["labels"]["test_pos"][:]
		elif args.test_type == "test_all":
			finalData, finalLabels = file["data"]["test_pos"][:], file["labels"]["test_pos"][:]
			for testType in ["test_neg_large_1", "test_neg_large_2", "test_neg_large_3", "test_neg_small"]:
				partialData, partialLabels = file["data"][testType][:], file["labels"][testType][:]
				partialData, partialLabels = randomCropsFromData(partialData, partialLabels)
				finalData = np.concatenate([finalData, partialData])
				finalLabels = np.concatenate([finalLabels, partialLabels])
		else:
			data, labels = file["data"][args.test_type][:], file["labels"][args.test_type][:]
			finalData, finalLabels = randomCropsFromData(data, labels)
	
	grayData = finalData[:, :, :, 0] * 0.2989 + finalData[:, :, :, 1] * 0.5870 + finalData[:, :, :, 2] * 0.1140
	return grayData, finalLabels

def getHogFeatures(data):
	results = np.zeros((data.shape[0], 3780))
	for i in range(data.shape[0]):
		hog = HogFeature(data[i], numOrientations=9, pixelsPerCell=(8, 8), cellsPerBlock=(2, 2), normalization="L2")
		results[i] = hog
	results = normalizeData(results, mean=np.mean(results), std=np.std(results))
	return results

def getFeatures(args):
	data, labels = getDataset(args)
	print("Read data(%s): %s %s" % (args.type, data.shape, labels.shape))
	print("#pos: %d; #neg: %d" % ( len(np.where(labels == 1)[0]), len(np.where(labels == 0)[0]) ))
	
	features = getHogFeatures(data)
	print("Computed features", features.shape)
	return features, labels

if __name__ == "__main__":
	args = getArguments()
	if args.type == "train":
		features, labels = getFeatures(args)
		classifier = svm.SVC(kernel="rbf", gamma="auto", C=1, cache_size=1000)
		classifier.fit(features, labels)
		print("Exporting SVM to:", args.file)
		joblib.dump(classifier, args.file)
	elif args.type == "test":
		features, labels = getFeatures(args)
		print("Loading SVM from:", args.file)
		classifier = joblib.load(args.file)
		result = classifier.predict(features)

		tp = len(np.where(np.logical_and(result == 1, labels == 1) == True)[0])
		fn = len(np.where(np.logical_and(result == 0, labels == 0) == True)[0])
		fp = len(np.where(np.logical_and(result == 1, labels == 0) == True)[0])
		tn = len(np.where(np.logical_and(result == 0, labels == 1) == True)[0])
		accuracy = len(np.where(result == labels)[0]) / len(features) * 100
		print("Accuracy: %2.2f, TP: %2.2f, FP: %2.2f, FN: %2.2f, TN: %2.2f" % (accuracy, tp, fp, fn, tn))
	elif args.type == "test_one_image":
		image = readImage(args.dataset_path)[:, :, 0 : 3]
		grayImage = grayScaleImage(image)
		print("Loading SVM from:", args.file)
		classifier = joblib.load(args.file)
		crops, indexes = getAllCrops(grayImage, windowSize=(128, 64), stride=6)
		print("Extracted all crops:", crops.shape)
		features = getHogFeatures(crops)
		result = classifier.predict(features)

		trueIndexes = np.where(result == 1)[0]
		print("Found %d boxes" % (len(trueIndexes)))
		image = addSquares(image, indexes[trueIndexes])
		plot_image(image)
		show_plots()
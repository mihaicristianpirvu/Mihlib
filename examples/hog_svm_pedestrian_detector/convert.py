# convert.py Module that convers the INRIA dataset to h5py format. First argument must be path to the root INRIA dir.
import h5py
import sys
import os
import numpy as np
from Mihlib import readImage, plot_image, show_plots, npGetInfo, getRandomCrops, cropCenterImage
from random import randint

def getPaths(cwd, trainPosList, trainNegList, testPosList, testNegList):
	trainPaths = {"pos" : [], "neg" : []}
	testPaths = {"pos" : [], "neg" : []}

	f = open(trainPosList, "r")
	for line in f.readlines():
		trainPaths["pos"].append(cwd + os.sep + line.strip())
	f = open(trainNegList, "r")
	for line in f.readlines():
		trainPaths["neg"].append(cwd + os.sep + line.strip())
	f = open(testPosList, "r")
	for line in f.readlines():
		testPaths["pos"].append(cwd + os.sep + line.strip())
	f = open(testNegList, "r")
	for line in f.readlines():
		testPaths["neg"].append(cwd + os.sep + line.strip())
	return trainPaths, testPaths

def getDataset(train, test):
	trainWindowSize = (128, 64)
	numSamples = 10
	numTrain, numTest = (len(train["pos"]) + numSamples * len(train["neg"])), (len(test["pos"]) + len(test["neg"]))
	print("num train:", numTrain, "numTest", numTest)

	dataset = {
		"train" : np.zeros((numTrain, 128, 64, 3), dtype=np.uint8),
		"test_pos" : np.zeros((len(test["pos"]), 128, 64, 3), dtype=np.uint8),
		"test_neg_small" : np.zeros((195, 240, 320, 3), dtype=np.uint8),
		"test_neg_large_1" : np.zeros((89, 480, 640, 3), dtype=np.uint8),
		"test_neg_large_2" : np.zeros((58, 486, 648, 3), dtype=np.uint8),
		"test_neg_large_3" : np.zeros((53, 432, 648, 3), dtype=np.uint8)
	}

	labels = {
		"train" : np.zeros((numTrain, ), dtype=np.uint8),
		"test_pos" : np.zeros((len(test["pos"]), ), dtype=np.uint8),
		"test_neg_small" : np.zeros((195, ), dtype=np.uint8),
		"test_neg_large_1" : np.zeros((89, ), dtype=np.uint8),
		"test_neg_large_2" : np.zeros((58, ), dtype=np.uint8),
		"test_neg_large_3" : np.zeros((53, ), dtype=np.uint8)
	}

	perm = np.random.permutation(numTrain)
	for i in range(len(train["pos"])):
		if i % 100 == 0:
			print("Done: %i/%i" % (i, len(train["pos"])))
		image = readImage(train["pos"][i])[:, :, 0 : 3].astype(np.uint8)
		dataset["train"][i] = cropCenterImage(image, trainWindowSize)
		labels["train"][i] = 1

	startIndex = len(train["pos"])
	for i in range(len(train["neg"])):
		if i % 100 == 0:
			print("Done: %i/%i" % (i, len(train["neg"])))
		image = readImage(train["neg"][i])[:, :, 0 : 3].astype(np.uint8)
		crops = getRandomCrops(image, trainWindowSize, numSamples=numSamples)
		for j in range(numSamples):
			dataset["train"][startIndex + (i * numSamples) + j] = crops[j]
			labels["train"][startIndex + (i * numSamples) + j] = 0
	
	dataset["train"] = dataset["train"][perm]
	labels["train"] = labels["train"][perm]
	
	for i in range(len(test["pos"])):
		if i % 100 == 0:
			print("Done: %i/%i" % (i, len(test["pos"])))
		image = readImage(test["pos"][i])[:, :, 0 : 3].astype(np.uint8)
		dataset["test_pos"][i] = cropCenterImage(image, trainWindowSize)
		labels["test_pos"][i] = 1

	shapesCount = {(240, 320, 3) : 0, (480, 640, 3) : 0, (486, 648, 3) : 0, (432, 648, 3) : 0}
	shapesKeys = {(240, 320, 3) : "test_neg_small", (480, 640, 3) : "test_neg_large_1", \
		(486, 648, 3) : "test_neg_large_2", (432, 648, 3) : "test_neg_large_3"}
	# For some reason negative testing labels have variable shape, so only support top 4 most common
	for i in range(len(test["neg"])):
		if i % 100 == 0:
			print("Done: %i/%i" % (i, len(test["neg"])))

		image = readImage(test["neg"][i])[:, :, 0 : 3].astype(np.uint8)
		if not image.shape in shapesKeys:
			continue

		index = shapesCount[image.shape]
		dataset[shapesKeys[image.shape]][index] = image
		labels[shapesKeys[image.shape]][index] = 0
		shapesCount[image.shape] += 1
	return dataset, labels

if __name__ == "__main__":
	assert len(sys.argv) == 2
	cwd = sys.argv[1] + os.sep
	train, test = getPaths(cwd, cwd + "train_64x128_H96/pos.lst", cwd + "train_64x128_H96/neg.lst", \
		cwd + "test_64x128_H96/pos.lst", cwd + "test_64x128_H96/neg.lst")
	print("Train:", len(train["pos"]), len(train["neg"]), "; Test:", len(test["pos"]), len(test["neg"]))

	dataset, labels = getDataset(train, test)
	file = h5py.File("inria_person.h5")
	data_group = file.create_group("data")
	for k, v in dataset.items():
		data_group.create_dataset(k, data=v)

	labels_group = file.create_group("labels")
	for k, v in labels.items():
		labels_group.create_dataset(k, data=v)

from Mihlib import *
import sys

def simple_subplots():
	img = np.zeros((5, 5, 3), dtype=np.uint8)

	# Make a grid of 4 x 4 with a step of 15
	for i in range(4 * 4):
		r, g, b = 15 * (i + 1), 15 * i if i > 0 else 0, 15 * (i - 1) if i > 1 else 0

		img[:, :] = (r, g, b)
		plot_image(img, axis=(4, 4, i + 1), new_figure=False, show_axis=False)

def main():
	simple_subplots()
	show_plots()

if __name__ == "__main__":
	main()

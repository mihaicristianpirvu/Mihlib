from Mihlib import ID3
from argparse import ArgumentParser
import numpy as np

from dataset import getDataset, printDataset

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("dataset")
	parser.add_argument("dataset_path")

	args = parser.parse_args()
	assert args.dataset in ("credit_fraud", "test_dataset")
	return args

def getScores(results, labels):
	def compute(results, labels):
		predTrue = results == True
		predFalse = results == False
		labelTrue = labels == True
		labelFalse = labels == False

		TP = np.sum(np.logical_and(predTrue, labelTrue))
		TN = np.sum(np.logical_and(predFalse, labelFalse))
		FP = np.sum(np.logical_and(predTrue, labelFalse))
		FN = np.sum(np.logical_and(predFalse, labelTrue))

		accuracy = 100 * (TP + TN) / len(results)
		precision = TP / (TP + FP)
		recall = TP / (TP + FN)

		return TP, TN, FP, FN, accuracy, precision, recall

	res = np.array([compute((results == i), (labels == i)) for i in range(2)])
	res = np.mean(res, axis=0)
	return res

def main():
	args = getArgs()
	(trainSet, trainLabels), (testSet, testLabels), metadata = getDataset(args)

	tree = ID3()
	for i in range(1, 17):
		tree.train(trainSet[0 : i * 50], trainLabels[0 : i * 50], metadata)
		results = tree.test(testSet)
		scores = getScores(results, testLabels)

		TP, TN, FP, FN, accuracy, precision, recall = getScores(results, testLabels)
		print("%d items. TP: %d. TN: %d. FP: %d. FN: %d. Accuracy: %2.2f. Precision: %2.2f. Recall: %2.2f" % \
			(i * 50, TP, TN, FP, FN, accuracy, precision, recall))

if __name__ == "__main__":
	main()
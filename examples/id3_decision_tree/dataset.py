import numpy as np

# Simple dataset inspired from https://www.youtube.com/watch?v=wL9aogTuZw8
def getTestDataset():
	# In metadata, the last column is assumed ethe one to be classified
	metadata = {
		"features" : ["Gender", "Car Ownership", "Travel Cost", "Income Level", "Transportation"],
		"values" : [
			["Male", "Female"],
			[0, 1, 2],
			["Cheap", "Standard", "Expensive"],
			["Low", "Medium", "High"],
			["Bus", "Train", "Car"]
		]
	}

	dataset = {
		"train" : [
			[0, 0, 0, 0],
			[0, 1, 0, 1],
			[1, 1, 0, 1],
			[1, 0, 0, 0],
			[0, 1, 0, 1],
			[0, 0, 1, 1],
			[1, 1, 1, 1],
			[1, 1, 2, 2],
			[0, 2, 2, 1],
			[1, 2, 2, 2]
		],
		"test" : [
			[0, 1, 1, 2],
			[0, 0, 0, 1],
			[1, 1, 0, 2],
			[0, 0, 2, 2]
		]
	}

	labels = np.uint8([0, 0, 1, 0, 0, 1, 1, 2, 2, 2])
	train = np.uint8(dataset["train"])
	test = np.uint8(dataset["test"])
	return (train, labels), (test, None), metadata

def getCreditFraudDataset(args):
	import arff
	data = arff.load(open(args.dataset_path, "r"))

	metadata = { "features" : [], "values" : [] }
	for attr in data["attributes"]:
		feature, values = attr
		metadata["features"].append(feature)
		metadata["values"].append(values)

	items = data["data"]
	dataset, labels = [], []
	for i in range(len(items)):
		item = items[i]
		values = item[0 : -1]
		label = item[-1]

		newItem = []
		for i in range(len(values)):
			if metadata["values"][i] == "REAL":
				newItem.append(values[i])
			else:
				newItem.append(metadata["values"][i].index(values[i]))

		newLabel = metadata["values"][-1].index(label)
		dataset.append(newItem)
		labels.append(newLabel)

	# Now, update the REAL values with mean (or median)
	dataset = np.array(dataset)
	labels = np.array(labels)

	for i in range(len(metadata["values"])):
		metaValues = metadata["values"][i]
		if metaValues != "REAL":
			continue

		columnData = dataset[:, i]
		mean = np.median(columnData)
		metadata["values"][i] = ["<%2.2f" % (mean), ">=%2.2f" % (mean)]

		newValues = np.float32(columnData >= mean)
		dataset[:, i] = newValues

	dataset = np.uint8(dataset)
	labels = np.uint8(labels)

	trainData, testData = dataset[0 : 800], dataset[800 :]
	trainLabels, testLabels = labels[0 : 800], labels[800 :]
	return (trainData, trainLabels), (testData, testLabels), metadata

def checkDataset(dataset):
	(train, trainLabels), (test, testLabels), metadata = dataset
	for item in train:
		assert len(item) == len(metadata["features"]) - 1
		for i in range(len(item)):
			assert item[i] <= len(metadata["values"][i])

	for i in range(len(trainLabels)):
		assert trainLabels[i] <= len(metadata["values"][-1])

	for item in test:
		assert len(item) == len(metadata["features"]) - 1
		for i in range(len(item)):
			assert item[i] <= len(metadata["values"][i])

	if not testLabels is None:
		for i in range(len(testLabels)):
			assert testLabels[i] <= len(metadata["values"][-1])

	return True

def getDataset(args):
	if args.dataset == "test_dataset":
		dataset = getTestDataset()
	elif args.dataset == "credit_fraud":
		dataset = getCreditFraudDataset(args)
	else:
		assert False

	assert checkDataset(dataset)
	return dataset

def printDataset(data, labels, metadata):
	# features, values = metadata
	for feature in metadata["features"]:
		print("%-15s" % (feature), end="")
	print("")
	for i in range(len(data)):
		item = data[i]
		for j in range(len(item)):
			strValue = str(metadata["values"][j][item[j]])
			print("%-15s" % strValue, end="")

		if not labels is None:
			print("%-15s" % (metadata["values"][-1][labels[i]]))
		else:
			print("%-15s" % ("-"))
	print("")

import cv2
from Mihlib import *
from argparse import ArgumentParser

def parseArguments():
	parser = ArgumentParser()
	parser.add_argument("image1", help="First image for the panorama.")
	parser.add_argument("image2", help="Second image for the panorama.")
	parser.add_argument("--ransac_steps", default=100, type=int, help="Number of steps for the RANSAC algorithm.")
	parser.add_argument("--ransac_max_distance", default=0.5, type=float, \
		help="Maximum distance for an inlier in RANSAC algorithm.")
	parser.add_argument("--ransac_min_count_percent", default=5, type=int, \
		help="Minimum percent of inliers for a good RANSAC model.",)
	args = parser.parse_args()
	return args

def findMatches(image1, image2):
	ratio = 0.75
	surf = SurfFeature(hessianThreshold=400, numOctaves=5, numOctaveLayers=5)
	keyPoints1, descriptor1 =  SURF_getKeypointsAndDescriptors(surf, image1.astype(np.uint8))
	keyPoints2, descriptor2 =  SURF_getKeypointsAndDescriptors(surf, image2.astype(np.uint8))

	matcher = cv2.BFMatcher(cv2.NORM_L2)
	matchPairs = SURF_matchKeypoints(matcher, keyPoints1, descriptor1, keyPoints2, descriptor2, ratio)
	return matchPairs

def main():
	args = parseArguments()
	image1, image2 = readImage(args.image1)[:, :, 0 : 3], readImage(args.image2)[:, :, 0 : 3]

	matches = findMatches(image2, image1)
	print("OpenCV-SURF matches:", len(matches))

	homography = ransacHomography(matches, args.ransac_steps, args.ransac_max_distance, args.ransac_min_count_percent)

	image = merge_images(image1, image2, homography)
	plot_image(image)
	show_plots()

if __name__ == "__main__":
	main()
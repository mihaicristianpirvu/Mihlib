# TODO: https://ourarchive.otago.ac.nz/bitstream/handle/10523/7534/OUCS-2002-12.pdf?sequence=1&isAllowed=y
import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from Mihlib import PrincipalComponentAnalysis

def getData(N):
    f1 = np.random.randn(N, 2)
    # f2 = np.random.randn(N, 1)
    f2 = f1[:, 0 : 1] + np.random.randn(N, 1) * 1 + 2 
    return np.concatenate([f1, f2], axis=1)

# https://stats.stackexchange.com/questions/22569/pca-and-proportion-of-variance-explained
def getCovPercentOfTopK(data, K, dataTotalDiagonalCovariance):
    cov = np.cov(data.T)
    topK = np.sort(np.diag(cov))[::-1][0 : K].sum()
    return topK / dataTotalDiagonalCovariance * 100

def main():
    np.set_printoptions(precision=3, suppress=True)
    np.random.seed(42)
    K = 2
    data = getData(N=100)
    dataTotalDiagonalCovariance = np.diag(np.cov(data.T)).sum()

    covPercent = getCovPercentOfTopK(data, K, dataTotalDiagonalCovariance)
    print("Data shape: %s. Cov (K=2): %2.3f%%" % (data.shape, covPercent))

    dataPCA = PCA(n_components=2).fit_transform(data)
    covPCAPercent = getCovPercentOfTopK(dataPCA, K, dataTotalDiagonalCovariance)
    print("Data PCA shape: %s. Cov (K=2): %2.3f%%" % (dataPCA.shape, covPCAPercent))

    dataPCA2 = PrincipalComponentAnalysis(data, M=2)
    covPCAPercent2 = getCovPercentOfTopK(dataPCA2, K, dataTotalDiagonalCovariance)
    print("Data PCA shape: %s. Cov (K=2): %2.3f%%" % (dataPCA2.shape, covPCAPercent2))
    assert np.abs(covPCAPercent - covPCAPercent2) < 1e-5

if __name__ == "__main__":
    main()
import sys
from Mihlib import *

def main():
	c0 = Camera() # default camera
	pose = HTranslation(1, 2, 3) * HRotationX(30)
	c1 = Camera(focalLength = 0.015, pose=pose.matrix)

	# Generate at least 6 points (or more if given by argv)
	N = 6 if len(sys.argv) != 2 else int(sys.argv[1])
	worldPoints = np.random.normal(size=(N, 3))

	# Plot world points in 3D
	plot_points(worldPoints, projection="3d", axis=(2, 2, 1), new_figure=False)
	# Plot perspective projections in 2D
	c1.plotPoints(worldPoints, axis=(2, 2, 2), new_figure=False, title="C1")
	c0.plotPoints(worldPoints, axis=(2, 2, 3), new_figure=False, title="C0")
	show_plots()

if __name__ == "__main__":
	main()
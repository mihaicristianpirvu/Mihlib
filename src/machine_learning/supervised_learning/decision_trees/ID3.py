import numpy as np

class ID3:
	def __init__(self, missingSubtreeMethod="default", missingSubtreeDefault=0):
		assert missingSubtreeMethod in ("compute_frequency", "default")
		self.missingSubtreeMethod = missingSubtreeMethod
		if missingSubtreeMethod == "default":
			self.lambdaMissing = lambda subTree : missingSubtreeDefault
		elif missingSubtreeMethod == "compute_frequency":
			assert False, "TODO"

		self.root = None
		self.trained = False

	# Given a set of labels, compute the current entropy: -sum(pi * log2(pi)), where
	# pi = np.where(labels == i) / len(lebels)
	def entropy(labels):
		uniques, counts = np.unique(labels, return_counts=True)
		total = len(labels)
		counts = counts / total
		return -np.sum(counts * np.log2(counts))

	# For each column in the data, compute the information gain if it's chose as the root of the current subtree
	def informationGain(data, labels):
		uniques, counts = np.unique(data, return_counts=True)
		total = len(data)
		counts = counts / total

		Sum = 0
		for i in range(len(uniques)):
			entropy = ID3.entropy(labels[np.where(data == uniques[i])])
			Sum += counts[i] * entropy
		return Sum

	def buildTree(self, data, labels, metadata, depth):
		assert len(labels) > 0
		entropy = ID3.entropy(labels)
		# This sub-tree (or the entire dataset) has the same label => tree is unable to be built
		if entropy == 0 or len(data[0]) == 0:
			return int(labels[0])

		numColumns = len(data[0])
		columnGains = np.zeros(numColumns)
		for c in range(numColumns):
			columnEntropy = ID3.informationGain(data[:, c], labels)
			gain = entropy - columnEntropy
			columnGains[c] = gain

		bestColumn = np.argmax(columnGains)
		subTrees = []
		root = (bestColumn, subTrees)
		numValues = metadata[bestColumn]

		for value in range(numValues):
			newDataIndices = np.where(data[:, bestColumn] == value)
			newSubData = np.delete(data[newDataIndices], bestColumn, axis=1)
			newLabels = labels[newDataIndices]
			newMetadata = np.delete(metadata, bestColumn)

			if len(newLabels) == 0:
				subTree = -1
			else:
				subTree = self.buildTree(newSubData, newLabels, newMetadata, depth + 1)
			subTrees.append(subTree)

		# If some sub-trees are not completed, complete them using statistics of neighbours or default value,
		#  based on the constructor parameter.
		if not -1 in subTrees:
			return root

		defaultValue = self.lambdaMissing(subTrees)
		for i in range(len(subTrees)):
			if subTrees[i] == -1:
				subTrees[i] = defaultValue

		return root

	def train(self, data, labels, metadata):
		assert len(labels) > 0
		actualMetadata = np.array([len(metadata["values"][i]) for i in range(len(metadata["values"]))])
		root = self.buildTree(data, labels, actualMetadata, depth = 0)
		self.root = root
		self.trained = True

	def test(self, data):
		assert self.trained == True
		data = np.copy(data)
		results = []

		for i in range(len(data)):
			tree = self.root
			item = data[i]
			while True:
				if type(tree) == np.int:
					results.append(tree)
					break

				bestColumn, subTree = tree
				value = item[bestColumn]
				item = np.delete(item, bestColumn)
				tree = subTree[value]

		return np.array(results)
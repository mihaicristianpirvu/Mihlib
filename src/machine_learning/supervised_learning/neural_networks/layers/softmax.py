import numpy as np
from .layer import Layer

class SoftMaxLayer(Layer):
	def __init__(self, inputSize):
 		super().__init__(inputSize, inputSize)

	def forward(self, layerInput):
		new_inputs = layerInput - np.max(layerInput, axis=1, keepdims=True)
		new_inputs = np.clip(new_inputs, -200, np.inf)
		exp_inputs = np.exp(new_inputs)
		self.layerOutput = exp_inputs / np.sum(exp_inputs, axis=1, keepdims=True)
		return self.layerOutput

	def backward(self, layerInput, error):
		Z = np.sum(self.layerOutput * error, axis=1, keepdims=True)
		return self.layerOutput * (error - Z)

	def hasWeightAndBiasTogether(self):
		return True

	def __str__(self):
		return "SoftMax " + super().__str__()
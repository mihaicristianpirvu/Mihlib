# layer.py - Interface for a neural network layer and possible implementations of such layers.
import numpy as np
from ..transfer_function import Identity

# Layer interface used by a Neural Network
class Layer:
	def __init__(self, inputSize, outputSize, transferFunction=Identity):
		self.inputSize = inputSize
		self.outputSize = outputSize
		self.transferFunction = transferFunction()

		# Output of this layer since the last step.
		self.layerOutput = np.zeros(self.outputSize)

	# Compute the output of this layer based on the inputs
	def forward(self, layerInput):
		raise NotImplementedError("Should have implemented this")

	# Compute the derivative of the error w.r.t to internal parameters of the layer
	# Gradients are cummulated instead of modified on the spot
	def backward(self, layerInput, error):
		raise NotImplementedError("Should have implemented this")

	# Optimizer will only be called for the trainable layers
	def hasParameters(self):
		return False

	# Layers with parameters should update this accordingly.
	def getWeights(self):
		return 0

	def getBias(self):
		return 0

	def getGradientWeights(self):
		return 0

	def getGradientBias(self):
		return 0

	def setWeights(self, value):
		raise NotImplementedError("Should have implemented this")

	def setBias(self, value):
		raise NotImplementedError("Should have implemented this")

	def setGradientWeights(self, value):
		raise NotImplementedError("Should have implemented this")

	def setGradientBias(self, value):
		raise NotImplementedError("Should have implemented this")

	def __str__(self):
		return str(self.inputSize) + " -> " + str(self.outputSize)
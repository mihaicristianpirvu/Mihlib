import numpy as np
from .layer import Layer
from ..transfer_function import Identity
from mathematics.linear_algebra.matrix import aMatrixMultiply, bMatrixMultiply, abMatrixMultiply

# Fully Connected Layer, batch version.
class FullyConnectedLayer(Layer):
	# @param[in] inputSize The input shape of an element in the batch, which must be Flatten (N, 1)
	# @param[in] outputSize The output shape of an element in the batch, which must be Flatten (M, 1) 
	# @param[in] transferFunction A transfer function for this layer
	def __init__(self, inputSize, outputSize, transferFunction=Identity):
		assert len(inputSize) == 2 and inputSize[1] == 1 and len(outputSize) == 2 and outputSize[1] == 1
		super().__init__(inputSize, outputSize, transferFunction)

		# Layer's parameters, initialized using Xavier's initialization
		self.weights = np.random.normal(0, np.sqrt(2 / (inputSize[0] + outputSize[0])), \
			size=(inputSize[0] + 1, outputSize[0]))

		# Computed value
		self.layerOutput = None

		# Gradients
		self.gradientWeights = np.zeros((inputSize[0] + 1, outputSize[0]))

	# layerOutput is the Z(L) parameter in the functions and this is saved for backpropagation step
	# The forward step will return f(Z(L)) to the next layers.
	# Compute the forward pass of a batched input based on the current weights.
	# @param[in] layerInput A MxNx1 input
	# @return The transformed output based on the weights, of shape MxOx1
	def forward(self, layerInput):
		# For one item: zj = sum(i=input) w_ij * xi, for all the outputs j.
		# For a batch of M (first dimension), result is: OxN @ Mx1xN => MxOx1
		self.layerOutput = bMatrixMultiply(self.getWeights().T, layerInput) + self.getBias()
		# yj = f(zj), which should apply element wise for a batch.
		return self.transferFunction.apply(self.layerOutput)

	def backward(self, layerInput, propagatedError):
		assert propagatedError.shape == self.layerOutput.shape
		newPropagatedError = np.zeros(layerInput.shape)
		gradientWeights = self.getGradientWeights()
		gradientBias = self.getGradientBias()

		# deltaZ = dE/dZ = dE/dY * dY/dZ, where dE/dY is the propagated error, and dy/dz is the derivative of the
		# transfer function, which we can compute for this output. Shape: MxOx1
		deltaZ = propagatedError * self.transferFunction.applyDerivative(self.layerOutput)

		# dE/dWjk = Xj * (dE/dZk), and has a shape of ((N+1)xO), where last line is biases
		# Chaning this to the other implementation gets a 33% time penalty (14s -> 22s 1 epoch on MNIST w/ batch 100)
		for i in range(layerInput.shape[0]):
			gradientWeights += np.dot(layerInput[i], deltaZ[i].T) / layerInput.shape[0]
			newPropagatedError[i] = np.dot(self.getWeights(), deltaZ[i])
		# Xn+1 = 1 (not represented anymore, so we don't copy data), so this is just 1 * (dE/dZk)
		gradientBias += np.mean(deltaZ, axis=0)

		# This is moved in the for above for a small speedup.
		# newPropagatedError = bMatrixMultiply(self.getWeights(), deltaZ)
		assert newPropagatedError.shape == layerInput.shape
		return newPropagatedError

	# Used to access the weights and bias values (such as in the process of backpropagation)
	def getWeights(self):
		return self.weights[0 : -1]

	def getBias(self):
		return np.expand_dims(self.weights[-1], axis=-1)

	def getGradientWeights(self):
		return self.gradientWeights[0 : -1]

	def getGradientBias(self):
		return np.expand_dims(self.gradientWeights[-1], axis=-1)

	def setWeights(self, value):
		self.weights[0 : -1] = value.reshape(self.weights[0 : -1].shape)

	def setBias(self, value):
		self.weights[-1] = value.reshape(self.weights[-1].shape)

	def setGradientWeights(self, value):
		self.gradientWeights[0 : -1] = value.reshape(self.gradientWeights[0 : -1].shape)

	def setGradientBias(self, value):
		self.gradientWeights[-1] = value.reshape(self.gradientWeights[-1].shape)

	def hasParameters(self):
		return True

	def __str__(self):
		return "FullyConnected " + super().__str__() + " (" + self.transferFunction.__str__() + ")"
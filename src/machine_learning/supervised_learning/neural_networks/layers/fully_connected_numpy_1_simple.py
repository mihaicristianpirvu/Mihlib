import numpy as np
from .layer import Layer
from ..transfer_function import Identity

# Fully Connected Layer. TODO: write comments
class FullyConnectedLayer(Layer):
	# @param[in] inputSize The input shape, which must be Flatten (N, 1)
	# @param[in] outputSize The output shape, which must be Flatten (M, 1) 
	# @param[in] transferFunction A transfer function for this layer
	def __init__(self, inputSize, outputSize, transferFunction=Identity):
		assert len(inputSize) == 2 and inputSize[1] == 1 and len(outputSize) == 2 and outputSize[1] == 1
		super().__init__(inputSize, outputSize, transferFunction)

		# Layer's parameters, initialized using Xavier's initialization
		self.weights = np.random.normal(0, np.sqrt(2 / (inputSize[0] + outputSize[0])), \
			size=(inputSize[0] + 1, outputSize[0]))

		# Computed value
		self.layerOutput = None

		# Gradients
		self.gradientWeights = np.zeros((inputSize[0] + 1, outputSize[0]))

	def forward(self, layerInput):
		self.layerOutput = np.zeros((layerInput.shape[0], *self.outputSize))
		for i in range(layerInput.shape[0]):
			self.layerOutput[i] = self.forward_simple(layerInput[i])
		return self.transferFunction.apply(self.layerOutput)

	# layerOutput is the Z(L) parameter in the functions and this is saved for backpropagation step
	# The forward step will return f(Z(L)) to the next layers.
	def forward_simple(self, layerInput):
		# Add one 1 at end for the bias term
		layerOutput = np.dot(self.getWeights().T, layerInput) + self.getBias()
		return layerOutput

	def backward(self, layerInput, propagatedError):
		result = np.zeros(layerInput.shape)
		for i in range(layerInput.shape[0]):
			result[i] = self.backward_simple(layerInput[i], propagatedError[i], self.layerOutput[i])
		return result

	def backward_simple(self, layerInput, propagatedError, layerOutput):
		assert propagatedError.shape == self.outputSize

		# error is weights(L+1) * delta_y(L+1) => delta_Y(L) = error * f'(Z(L))
		delta_y = propagatedError * self.transferFunction.applyDerivative(layerOutput)

		# Compute the gradients w.r.t. the weights (self.g_weights)
		gradientWeights = self.getGradientWeights()
		gradientBias = self.getGradientBias()
		gradientWeights += np.dot(layerInput, delta_y.T)
		gradientBias += delta_y

		# Compute and return the gradients w.r.t the inputs of this layer
		result = np.dot(self.getWeights(), delta_y)

		assert result.shape == layerInput.shape
		return result

	# Used to access the weights and bias values (such as in the process of backpropagation)
	def getWeights(self):
		return self.weights[0 : -1]

	def getBias(self):
		return np.expand_dims(self.weights[-1], axis=-1)

	def getGradientWeights(self):
		return self.gradientWeights[0 : -1]

	def getGradientBias(self):
		return np.expand_dims(self.gradientWeights[-1], axis=-1)

	def setWeights(self, value):
		self.weights[0 : -1] = value.reshape(self.weights[0 : -1].shape)

	def setBias(self, value):
		self.weights[-1] = value.reshape(self.weights[-1].shape)

	def setGradientWeights(self, value):
		self.gradientWeights[0 : -1] = value.reshape(self.gradientWeights[0 : -1].shape)

	def setGradientBias(self, value):
		self.gradientWeights[-1] = value.reshape(self.gradientWeights[-1].shape)

	def hasParameters(self):
		return True

	def __str__(self):
		return "FullyConnected " + super().__str__() + " (" + self.transferFunction.__str__() + ")"
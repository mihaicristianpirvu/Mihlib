import numpy as np
from .layer import Layer
from ..transfer_function import Identity
from mathematics.linear_algebra.matrix import aMatrixMultiply, bMatrixMultiply, abMatrixMultiply

# Fully Connected Layer, batch version.
class FullyConnectedLayer(Layer):
	# @param[in] inputSize The input shape of an element in the batch, which must be Flatten (N,)
	# @param[in] outputSize The output shape of an element in the batch, which must be Flatten (M,) 
	# @param[in] transferFunction A transfer function for this layer
	def __init__(self, inputSize, outputSize, transferFunction=Identity):
		assert len(inputSize) == 1 and len(outputSize) == 1
		super().__init__(inputSize, outputSize, transferFunction)

		# Layer's parameters, initialized using Xavier's initialization
		self.weights = np.random.normal(0, np.sqrt(2 / (inputSize[0] + outputSize[0])), \
			size=(inputSize[0] + 1, outputSize[0]))

		# Computed value
		self.layerOutput = None

		# Gradients
		self.gradientWeights = np.zeros((inputSize[0] + 1, outputSize[0]))

	# layerOutput is the Z(L) parameter in the functions and this is saved for backpropagation step
	# The forward step will return f(Z(L)) to the next layers.
	# Compute the forward pass of a batched input based on the current weights.
	# @param[in] layerInput A MxNx1 input
	# @return The transformed output based on the weights, of shape MxOx1
	def forward(self, layerInput):
		assert len(layerInput.shape) == 2, "Input must be of shape (mbSize, inputShape). For (mbSize, inputShape, 1),\
		use numpy_1 implementation."
		# For one item: zj = sum(i=input) xi * w_ij, for all the outputs j.
		# For a batch of M (first dimension), result is: MxI x IxO => MxO
		self.layerOutput = np.dot(layerInput, self.getWeights()) + self.getBias()
		# yj = f(zj), which should apply element wise for a batch.
		return self.transferFunction.apply(self.layerOutput)

	def backward(self, layerInput, propagatedError):
		assert propagatedError.shape == self.layerOutput.shape
		newPropagatedError = np.zeros(layerInput.shape)
		gradientWeights = self.getGradientWeights()
		gradientBias = self.getGradientBias()

		# deltaZ = dE/dZ = dE/dY * dY/dZ, where dE/dY is the propagated error, and dy/dz is the derivative of the
		# transfer function, which we can compute for this output. Shape: MxO
		deltaZ = propagatedError * self.transferFunction.applyDerivative(self.layerOutput)

		# dE/dWjk = (dE/dZk) * (dZk/dWjk). Since Zj = Xi * Wij => (dZk/dWjk) = Xj.
		# So, dE/dWjk = Xj * (dE/dZk), and has a shape of ((N+1)xO), where last line is biases
		# Concrete example here: input has shape (M x 100), output and deltaZ has (M x 10)
		# Weights have (100 x 10), so we can dot input.T x deltaZ = (100 x M) x (M x 10) => (100 x 10).
		# We also divide by number of items in batch (M), so the gradients are updated with the mean result.
		gradientWeights += np.dot(layerInput.T, deltaZ) / layerInput.shape[0]
		gradientBias += np.mean(deltaZ, axis=0)

		# This is moved in the for above for a small speedup.
		# dE/dX = dE/dZ * dZ/dX. Since Zj=Xi * Wij => dZ/dX = Wij. Thus dE/dX = deltaZ * Weights and must have a
		#  shape of (MxI) (same as input). W = (IxO) and deltaZ(MxO) => deltaZ * W.T => (MxI) (same as input shape)
		newPropagatedError = np.dot(deltaZ, self.getWeights().T)
		assert newPropagatedError.shape == layerInput.shape
		return newPropagatedError

	# Used to access the weights and bias values (such as in the process of backpropagation)
	def getWeights(self):
		return self.weights[0 : -1]

	def getBias(self):
		return self.weights[-1]

	def getGradientWeights(self):
		return self.gradientWeights[0 : -1]

	def getGradientBias(self):
		return self.gradientWeights[-1]

	def setWeights(self, value):
		self.weights[0 : -1] = value.reshape(self.weights[0 : -1].shape)

	def setBias(self, value):
		self.weights[-1] = value.reshape(self.weights[-1].shape)

	def setGradientWeights(self, value):
		self.gradientWeights[0 : -1] = value.reshape(self.gradientWeights[0 : -1].shape)

	def setGradientBias(self, value):
		self.gradientWeights[-1] = value.reshape(self.gradientWeights[-1].shape)

	def hasParameters(self):
		return True

	def __str__(self):
		return "FullyConnected " + super().__str__() + " (" + self.transferFunction.__str__() + ")"
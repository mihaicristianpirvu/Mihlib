import numpy as np
from .layer import Layer
from ..transfer_function import Identity

class ConvolutionalLayer(Layer):
	def __init__(self, inputSize, kernelSize, stride, numKernels, transferFunction=Identity):
		self.depth, self.width, self.height = inputSize
		assert (self.height - kernelSize) % stride == 0 and (self.width - kernelSize) % stride == 0
		self.kernelSize = kernelSize
		self.stride = stride
		self.numKernels = numKernels

		self.heightOutput = int((self.height - kernelSize) / stride + 1)
		self.widthOutput = int((self.width - kernelSize) / stride + 1)
		super().__init__(inputSize, (self.numKernels, self.widthOutput, self.heightOutput), transferFunction)

		# Layer's parameters
		self.weights = np.random.normal(0, np.sqrt(2 / (self.depth * kernelSize * kernelSize)), \
			(numKernels, self.depth, kernelSize, kernelSize))
		self.biases = np.random.normal(0, np.sqrt(2 / (self.depth * kernelSize * kernelSize)), (numKernels, 1))

		# Computed value
		self.layerOutput = None

		# Gradients
		self.gradientWeights = np.zeros((self.numKernels, self.depth, self.kernelSize, self.kernelSize))
		self.gradientBiases = np.zeros((self.numKernels, 1))
	
	def forward(self, layerInput):
		self.layerOutput = np.zeros((layerInput.shape[0], *self.outputSize))
		for i in range(layerInput.shape[0]):
			self.layerOutput[i] = self.forward_simple(layerInput[i])
		return self.transferFunction.apply(self.layerOutput)

	def forward_simple(self, layerInput):
		layerOutput = np.zeros(self.outputSize)

		for n in range(self.numKernels):
			for m in range(self.depth):
				for y in range(0, self.height - self.kernelSize + 1, self.stride):
					k_y = int(y / self.stride)
					for x in range(0, self.width - self.kernelSize + 1, self.stride):
						k_x = int(x / self.stride)
						for p in range(self.kernelSize):
							for q in range(self.kernelSize):
								a = layerInput[m, x+p, y+q] * self.weights[n, m, p, q]
								layerOutput[n, k_x, k_y] += a
			for k_y in range(self.heightOutput):
				for k_x in range(self.widthOutput):
					layerOutput[n, k_x, k_y] += self.biases[n]

		return layerOutput

	def backward(self, layerInput, propagatedError):
		result = np.zeros(layerInput.shape)
		for i in range(layerInput.shape[0]):
			result[i] = self.backward_simple(layerInput[i], propagatedError[i], self.layerOutput[i])
		return result

	def backward_simple(self, layerInput, propagatedError, layerOutput):
		delta_y = propagatedError * self.transferFunction.applyDerivative(layerOutput)

		# Gradient w.r.t weights
		for n in range(self.numKernels):
			for m in range(self.depth):
				for i in range(self.heightOutput):
					for j in range(self.widthOutput):
						for p in range(self.kernelSize):
							for q in range(self.kernelSize):
								a = layerInput[m, i*self.stride+p, j*self.stride+q] * delta_y[n, i, j]
								self.gradientWeights[n, m, p, q] += a

		# Gradient w.r.t inputs
		result = np.zeros(layerInput.shape)
		for n in range(self.numKernels):
			for m in range(self.depth):
				for i in range(self.height):
					for j in range(self.width):
						for p in range(self.kernelSize):
							for q in range(self.kernelSize):
								out_i = int((i - p) / self.stride)
								out_j = int((j - q) / self.stride)
								if ((i - p) % self.stride == 0) and ((j - q) % self.stride == 0) and (i - p >= 0) and \
									(j - q >= 0) and out_i in range(0, self.heightOutput) and \
									out_j in range(0, self.widthOutput):
									a = self.weights[n, m, p, q] * delta_y[n, out_i, out_j]
									result[m, i, j] += a

		# Alternative way of writing this without caring about the fraction
		'''
		# Gradient w.r.t inputs
		result = np.zeros(layerInput.shape)
		for n in range(self.numKernels):
			for m in range(self.depth):
				for out_i in range(self.heightOutput):
					for out_j in range(self.widthOutput):
						for p in range(self.kernelSize):
							for q in range(self.kernelSize):
								in_i = out_i * self.stride + p
								in_j = out_j * self.stride + q
								a = self.weights[n, m, p, q] * delta_y[n, out_i, out_j]
								result[m, in_i, in_j] += a
		'''


		for n in range(self.numKernels):
			self.gradientBiases[n] += np.sum(delta_y[n])

		return result

	# Used to access the weights and bias values (such as in the process of backpropagation)
	def getWeights(self):
		return self.weights

	def getBias(self):
		return self.biases

	def getGradientWeights(self):
		return self.gradientWeights

	def getGradientBias(self):
		return self.gradientBiases

	def setWeights(self, value):
		self.weights = value

	def setBias(self, value):
		self.biases = value

	def setGradientWeights(self, value):
		self.gradientWeights = value

	def setGradientBias(self, value):
		self.gradientBiases = value

	def hasParameters(self):
		return True

	def __str__(self):
		return "Convolution (6-for version) " + super().__str__() + " (" + self.transferFunction.__str__() + ")"
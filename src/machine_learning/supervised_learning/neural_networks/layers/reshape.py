from .layer import Layer

# Both layerInput and error should have an additional dimension with the mini batch size.
class ReshapeLayer(Layer):
	def __init__(self, inputSize, outputSize):
		super().__init__(inputSize, outputSize)

	def forward(self, layerInput):
		self.layerOutput = layerInput.reshape(layerInput.shape[0], *self.outputSize)
		return self.layerOutput

	def backward(self, layerInput, propagatedError):
		return propagatedError.reshape(layerInput.shape[0], *self.inputSize)

	def __str__(self):
		return "Reshape " + super().__str__()
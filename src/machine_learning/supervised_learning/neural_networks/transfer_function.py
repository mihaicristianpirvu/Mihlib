# transfer_function.py - Module used to implement different transfer functions used by a network layer (identitiy,
# tanh, sigmoid, relu, identity, etc.)
import numpy as np

class TransferFunction:
	def apply(self, input):
		raise NotImplementedError("Should have implemented this")

	def applyDerivative(self, input):
		raise NotImplementedError("Should have implemented this")

	def __str__(self):
		return "Transfer Function"

class Identity(TransferFunction):
	def apply(self, input):
		return input

	def applyDerivative(self, input):
		return np.ones(input.shape)

	def __str__(self):
		return "Identity"

class ReLU(TransferFunction):
	def apply(self, input):
		return (input > 0) * input

	def applyDerivative(self, input):
		return (input > 0)

	def __str__(self):
		return "ReLU"

class Logistic(TransferFunction):
	def apply(self, input):
		a = np.clip(input, -500, 500)
		f = 1 / (1 + np.exp(-a))
		return f
		# self.output = 1 / (1 + np.exp(-a))
		# return self.output

	def applyDerivative(self, input):
		a = np.clip(input, -500, 500)
		f = 1 / (1 + np.exp(-a))
		# return self.output * (1 - self.output)
		return f * (1 - f)

	def __str__(self):
		return "Logistic"

class Tanh(TransferFunction):
	def apply(self, input):
		a = np.exp(-2 * np.clip(input, -250, 250))
		f = (1 - a) / (1 + a)
		return f

	def applyDerivative(self, input):
		a = np.exp(-2 * np.clip(input, -250, 250))
		f = (1 - a) / (1 + a)
		return 1 - f**2

	def __str__(self):
		return "Tanh"

import numpy as np
from copy import copy

# optimizer.py - Different optimizer implementations (gradient descent only, for the moment)
class Optimizer:
	def __init__(self, learningRate):
		self.learningRate = learningRate

	# Some layers have no optimizers becasue they have no parameters.
	def optimize(self, layer):
		raise NotImplementedError("Should have implemented this")

	def __str__(self):
		raise NotImplementedError("Should have implemented this")

class GradientDescent(Optimizer):
	def optimize(self, layer):
		assert layer.hasParameters()
		weights = layer.getWeights()
		gradientWeights = layer.getGradientWeights()

		weights -= self.learningRate * gradientWeights
		gradientWeights *= 0

	def __str__(self):
		return "Gradient Descent. Learning rate: " + str(self.learningRate)

class Momentum(Optimizer):
	def __init__(self, learningRate, momentum=0.9):
		self.learningRate = learningRate
		self.momentumRate = momentum
		# Keep a dictionary with all the layers to update the weights accordingly from step to step
		self.layerMomentum = {}

	def optimize(self, layer):
		assert layer.hasParameters()
		weights = layer.getWeights()
		gradientWeights = layer.getGradientWeights()

		if not layer in self.layerMomentum:
			self.layerMomentum[layer] = np.zeros(gradientWeights.shape)

		self.layerMomentum[layer] = self.momentumRate * self.layerMomentum[layer] + self.learningRate * gradientWeights

		weights -= self.layerMomentum[layer]
		gradientWeights *= 0

	def __str__(self):
		return "Gradient Descent with Momentum. Learning rate: " + str(self.learningRate) + ", Momentum: " + \
			str(self.momentumRate)

class Nesterov(Optimizer):
	def __init__(self, learningRate, momentum=0.9):
		self.learningRate = learningRate
		self.momentumRate = momentum
		# Keep a dictionary with all the layers to update the weights accordingly from step to step
		self.layerMomentum = {}

	# Implementation based on http://cs231n.github.io/neural-networks-3/ to avoid computing new gradients.
	# http://cs231n.stanford.edu/slides/2016/winter1516_lecture6.pdf
	def optimize(self, layer):
		assert layer.hasParameters()
		weights = layer.getWeights()
		gradientWeights = layer.getGradientWeights()

		if not layer in self.layerMomentum:
			self.layerMomentum[layer] = np.zeros(gradientWeights.shape)

		tmpLayerMomentum = copy(self.layerMomentum[layer])
		self.layerMomentum[layer] = self.momentumRate * self.layerMomentum[layer] - self.learningRate * gradientWeights
		weights -= self.momentumRate * tmpLayerMomentum - (1 + self.momentumRate) * self.layerMomentum[layer]
		gradientWeights *= 0

	def __str__(self):
		return "Nesterov Momentum. Learning rate: " + str(self.learningRate) + ", Momentum: " + \
			str(self.momentumRate)

class AdaGrad(Optimizer):
	def __init__(self, learningRate):
		super().__init__(learningRate)
		# This is the equivalent of the G matrix in http://ruder.io/optimizing-gradient-descent/index.html#adagrad,
		#  as implemented on te PDF from nesterov.
		self.gradCache = {}

	def optimize(self, layer):
		assert layer.hasParameters()
		weights = layer.getWeights()
		gradientWeights = layer.getGradientWeights()

		if not layer in self.gradCache:
			self.gradCache[layer] = 0
		self.gradCache[layer] += gradientWeights**2

		weights -= self.learningRate * gradientWeights / (np.sqrt(self.gradCache[layer]) + 1e-7)
		gradientWeights *= 0

	def __str__(self):
		return "Adagrad. Learning rate: " + str(self.learningRate)

class RMSProp(Optimizer):
	def __init__(self, learningRate, decayRate=0.9):
		super().__init__(learningRate)
		self.gradCache = {}
		self.decayRate = decayRate

	def optimize(self, layer):
		assert layer.hasParameters()
		weights = layer.getWeights()
		gradientWeights = layer.getGradientWeights()

		if not layer in self.gradCache:
			self.gradCache[layer] = 0
		self.gradCache[layer] = self.decayRate * self.gradCache[layer] + (1 - self.decayRate) * gradientWeights**2

		weights -= self.learningRate * gradientWeights / (np.sqrt(self.gradCache[layer]) + 1e-7)
		gradientWeights *= 0

	def __str__(self):
		return "RMSProp. Learning rate: " + str(self.learningRate) + " Decay rate: " + str(self.decayRate)

class Adam(Optimizer):
	def __init__(self, learningRate, momentum=0.9, decayRate=0.9):
		super().__init__(learningRate)
		self.momentumRate = momentum
		self.decayRate = decayRate
		self.layerMomentum = {}
		self.gradCache = {}
		self.time = {}

	def optimize(self, layer):
		assert layer.hasParameters()
		weights = layer.getWeights()
		gradientWeights = layer.getGradientWeights()

		if not layer in self.layerMomentum:
			self.layerMomentum[layer] = np.zeros(gradientWeights.shape)
		if not layer in self.gradCache:
			self.gradCache[layer] = 0
		if not layer in self.time:
			self.time[layer] = 0

		self.time[layer] += 1
		self.layerMomentum[layer] = self.momentumRate * self.layerMomentum[layer] + \
			(1 - self.momentumRate) * gradientWeights
		self.gradCache[layer] = self.decayRate * self.gradCache[layer] + \
			(1 - self.decayRate) * gradientWeights**2

		correctionMomentum = self.layerMomentum[layer] / (1 - self.momentumRate**self.time[layer])
		correctionGrad = self.gradCache[layer] / (1 - self.decayRate**self.time[layer])

		weights -= self.learningRate * correctionMomentum / (np.sqrt(correctionGrad) + 1e-7)
		gradientWeights *= 0

	def __str__(self):
		return "Adam. Learning rate: " + str(self.learningRate) + " Momentum rate: " + str(self.momentumRate) + \
			" Decay rate: " + str(self.decayRate)

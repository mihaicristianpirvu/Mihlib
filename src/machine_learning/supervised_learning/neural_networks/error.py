import numpy as np

# error.py - Generic function for all differentiable errors used in a neural network
class ErrorFunction:
	def computeErrorMinimum(self, labelVector, networkOutput):
		raise NotImplementedError("Should have implemented this")

	def computeCurrentError(self, correctLabels, networkOutputs):
		raise NotImplementedError("Should have implemented this")

class LogisticError(ErrorFunction):
	def computeErrorMinimum(self, labelVector, networkOutput):
		return -labelVector / networkOutput

	def computeCurrentError(self, correctLabels, networkOutputs):
		assert correctLabels.shape == networkOutputs.shape
		error = 0
		# Formula from homework PDF
		N = correctLabels.shape[0]
		K = correctLabels.shape[1]
		for i in range(N):
			assert (np.sum(networkOutputs[i]) - 1) < 0.01
			for j in range(K):
				error += correctLabels[i, j] * np.log(networkOutputs[i, j])
		return -error / N

	def __str__(self):
		return "Logistic Error"

class SumSquaredError(ErrorFunction):
	def computeErrorMinimum(self, labelVector, networkOutput):
		return networkOutput - labelVector

	def computeCurrentError(self, correctLabels, networkOutputs):
		assert correctLabels.shape == networkOutputs.shape
		error = 0
		N = correctLabels.shape[0]
		K = correctLabels.shape[1]
		for i in range(N):
			error += np.sum((networkOutputs[i] - correctLabels[i])**2)
		return error / N

	def __str__(self):
		return "Sum Squared Error"

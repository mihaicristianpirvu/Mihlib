import numpy as np
from utilities.constants import *
from mathematics.linear_algebra.distances import distance_point_line
from mathematics.linear_algebra.lines import getIntersectionPoint_mb
from utilities.others import getStepIndex

# linear_regression.py Module that implements different techniques to perform linear regression.
# In future, if there will be general methods to perform regression, a linear implementation should exist as well
#  for the example.

# Given N numbers, return 2 numbers that best define a line (?)
# - K: count of checks
# - D: maximum distance for a point to be in the model
# - C: mininum points needed for a model to be valid
# - value: The value in the image considered for points
# Returns a list of points that fit the model, with the first 2 being the chosen ones.
def ransac_linear_regression(L, K=10, D=2, cnt_min=10):
	best, best_count_fit = None, None
	for i in range(K):
		p1 = L[randomInteger(0, len(L)-1)]
		p2 = p1
		while p1[X] == p2[X] or p1[Y] == p2[Y]:
			p2 = L[randomInteger(0, len(L)-1)]
		current_fit = [p1, p2]

		# Now, try for all the points and see if more than d points agree to the model
		for p in L:
			if p is p1 or p is p2:
				continue
			# Now, for this given line, check how many external points fit to it
			dist = distance_point_line(p1, p2, p)
			if dist < D:
				current_fit.append(p)
		
		if len(current_fit) >= cnt_min and (best_count_fit == None or len(current_fit) > best_count_fit):
			best, best_count_fit = np.array(current_fit), len(current_fit)
	return best

# Given N points, return the best line equation in the least squares sense
def least_squares_linear_regression(L):
	# We're trying to minimze E = (y - mx - b) ^ 2. See pages 52-53 notebook for mathematical solve
	Sx, Sy, Sx_2, Sy_2, Sxy = 0, 0, 0, 0, 0
	N = len(L)
	for p in L:
		Sx += p[X]
		Sy += p[Y]
		Sxy += p[X] * p[Y]
		Sx_2 += pow(p[X], 2)
		Sy_2 += pow(p[Y], 2)

	A = np.array([[2 * Sx, 2 * N], [2 * Sx_2, 2 * Sx]])
	B = np.array([2 * Sy, 2 * Sxy])
	return np.linalg.solve(A, B)

# Given N points, return the best line equation that minimizes E = ax + by - c
def total_least_squares_linear_regression(L):
	Sx, Sy = 0, 0
	N = len(L)
	for p in L:
		Sx += p[X]
		Sy += p[Y]

	x_dash = Sx / N
	y_dash = Sy / N

	A = []
	for p in L:
		l = [p[X] - x_dash, p[Y] - y_dash]
		A.append(l)

	A = np.array(A)
	sym_matrix = np.matmul(np.transpose(A), A)
	
	lambdas, v = np.linalg.eig(sym_matrix)
	v = np.transpose(v)
	# v = (a, b); a(x-x_d) + b(x-y_d) = 0 => ax + bx + (-a*x_d - b*y_d) = 0
	argmin = np.argmin(lambdas)
	a = v[argmin][X]
	b = v[argmin][Y]
	d = -a * x_dash - b * y_dash
	return (a, b, d)

	a_min = point[X] - num_bins * bin_interval
	a_max = point[X] + num_bins * bin_interval
	b_min = point[Y] - num_bins * bin_interval
	b_max = point[Y] + num_bins * bin_interval	

	a = a_min
	while a < a_max:
		b = -point[X] * a + point[Y]
		if (a, b) not in D:
			D[(a, b)] = 1
		else:
			D[(a, b)] += 1
		a += bin_interval
		L.append((a,b))
	return D

def hough_transform_line(points, threshold, num_bins):
	assert(num_bins > 1)
	good_lines = []
	D = {}

	intersectionPoints = []
	for i in range(len(points) - 1):
		line1 = (-points[i][X], points[i][Y])
		for j in range(i + 1, len(points)):
			line2 = (-points[j][X], points[j][Y])
			intersectionPoint = getIntersectionPoint_mb(line1, line2)
			if intersectionPoint != None:
				intersectionPoints.append(intersectionPoint)

	intersectionPoints = np.array(intersectionPoints)
	a_min = np.min(intersectionPoints[:,X])
	a_max = np.max(intersectionPoints[:,X])
	b_min = np.min(intersectionPoints[:,Y])
	b_max = np.max(intersectionPoints[:,Y])
	a_interval = (a_max-a_min) / (num_bins - 1)
	b_interval = (b_max-b_min) / (num_bins - 1)
	results = np.zeros((num_bins, num_bins))

	for intersectionPoint in intersectionPoints:
		# Calculate the interval indexes in results matrix
		a_index = int(getStepIndex(intersectionPoint[X], a_min, a_interval))
		b_index = int(getStepIndex(intersectionPoint[Y], b_min, b_interval))
		if a_index >= num_bins:
			a_index = num_bins - 1
		if b_index >= num_bins:
			b_index = num_bins - 1
		# Add one vote to this interval
		results[a_index][b_index] += 1
		# Also save all points that voted for this
		if (a_index, b_index) not in D:
			D[(a_index, b_index)] = []
		D[(a_index, b_index)].append(intersectionPoint)
		# threshold-1 maybe here (?)
		if results[a_index][b_index] == threshold:
			good_lines.append((a_index, b_index))

	# Now, let's average all good intersection points
	final_lines = []
	for good_line in good_lines:
		assert len(D[good_line]) >= threshold
		Sum = [0, 0]
		for item in D[good_line]:
			Sum[X] += item[X]
			Sum[Y] += item[Y]
		final_lines.append( (Sum[X]/len(D[good_line]), Sum[Y]/len(D[good_line])) )

	return np.array(final_lines)

# Given a (m, b) line, compute the error against the data points, using the SSD
# SSD([x,y]) = ( sum[1:N] ( y - (mx + b) ) ^ 2 ) / N
def SSD_linear_regression(m, b, data):
	Sum = 0
	for i in range(len(data)):
		x, y = data[i]
		value = y - m * x - b
		Sum += pow(value, 2)
	return Sum / len(data)

def step_graident_descent_linear_regression(m, b, data, learning_rate):
	N = len(data)
	new_m, new_b = 0, 0

	# E = (y - mx - b) ^ 2 =>
	#  dE/dm = -2 * x * (y - mx -b) = 2 * x * (mx + b - y)
	#  dE/db = -2 * (y - mx - b) = 2 * (mx + b - y)
	for i in range(N):
		x, y = data[i]
		new_m += 2 * x * (m*x + b - y)
		new_b += 2 * (m*x + b - y)
	new_m /= N
	new_b /= N

	m = m - learning_rate * new_m
	b = b - learning_rate * new_b
	return (m, b)

# Iteratively try to minimize the function E = (y-mx-b) ^ 2 by using the partial derivatives -dE/dm and -dE/db
# which are the fastest direction descent of the function where E is the error function. A simpler solution
# for this case would be to just solve it (and get a least squares solution), but this is iteratively and works
# faster in general case (more complicated function, not linear regression). When n->oo, the solution should be
# the same as least squares.
# data: dataset containing pairs of x, y points
# num_iterations: number of iterations
# learning_rate: the amount of emphasis on each gradient descent at every step
# Output: m, b values for the desired line
def gradient_descent_linear_regression(data, num_iterations, learning_rate):
	m, b = 0, 0

	error = SSD_linear_regression(m, b, data)
	prev_error = None
	prev_10k_error = error
	for i in range(num_iterations):
		#print("Step=", i, "m=", m, "b=", b, "Error=", error)
		if not prev_error is None and abs(error - prev_error) < 0.000001:
			break

		# Stop if the current error is also bigger than it was 10k steps ago
		if i % 10000 == 0 and i != 0:
			if error > prev_10k_error:
				break
			prev_10k_error = error

		m, b = step_graident_descent_linear_regression(m, b, data, learning_rate)
		prev_error = error
		error = SSD_linear_regression(m, b, data)

	#print("Step=", i, "m=", m, "b=", b, "Error=", error)
	return (m, b)

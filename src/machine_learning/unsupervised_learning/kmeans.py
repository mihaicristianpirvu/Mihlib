from utilities.random import randomPick
from utilities.others import squash_array
import numpy as np

# Computes the expectation step of kMeans (moving points to the closest centroids)
def expectation_step(data, centroids, distanceFunction):
	results = np.zeros((data.shape[0], len(centroids)))

	for k in range(len(centroids)):
		results[:, k] = distanceFunction(data, centroids[k])
	newClusters = np.argmin(results, axis=-1)

	'''
	# Equivalent of above but more readable.
	for i in range(image.shape[I]):
		for j in range(image.shape[J]):
			best, bestDisstance = 0, INF
			for k in range(len(centroids)):
				result = distanceFunction(image[i, j], centroids[k])
				if result < bestDisstance:
					best, bestDisstance = k, result
			newClusters[i, j] = best
	'''
	return newClusters

# Computes the new centroids by finding the average point of all clusters in the image.
def maximization_step(data, clusters, clustersCount, initializationFunction):
	centroids = []
	for i in range(clustersCount):
		# Get the positions where the clusters are in the ith centroid
		positions = np.where(clusters == i)

		# Use numpy smart indexing to get the values (colors) of those indexes in the images
		values = data[positions]

		# If no item is in this cluster, add a random value based on the initialization Function
		if values.shape[0] == 0:
			centroid_value = initializationFunction(data, 1)
		# Otherwise, compute the mean of all the colors
		else:
			centroid_value = np.mean(values, axis=0)
		centroids.append(centroid_value)
	return centroids

# Algorithm converges if only less than percentChange of the points changed their cluster.
def check_convergence(oldClusters, newClusters, percentChange):
	chnagedCount = np.sum(np.equal(oldClusters, newClusters) ^ 1)
	expectedCount = 0 if percentChange == 0 else oldClusters.shape[0] / (100 / percentChange)
	return chnagedCount < expectedCount

# Public interface function
# param data The data on which the algorithm is run. Must have a shape of (N, D).
# param clustersCount The number of clusters (K)
# param distanceFunction Callback that computes the distance between two points
# param initialization The type of initialization
def kMeans(data, clustersCount, distanceFunction, initializationFunction, maxSteps, percentChange=1, **kwargs):
	assert len(data.shape) == 2
	assert clustersCount < np.sqrt(data.shape[0]) and clustersCount > 0
	assert maxSteps > 0

	centroids = initializationFunction(data, clustersCount)
	assert len(centroids) == clustersCount
	clusters = np.zeros((data.shape[0], ))
	newClusters = None

	step = 0
	while True:
		step += 1

		# Expectation step (move points to nearest clusters)
		newClusters = expectation_step(data, centroids, distanceFunction)
		centroids = maximization_step(data, newClusters, clustersCount, initializationFunction)

		if check_convergence(clusters, newClusters, percentChange) or step == maxSteps:
			print("Finished in {} steps. Num clusters: {}".format(step, clustersCount))
			if step <= 5:
				return kMeans(data, clustersCount, distanceFunction, initializationFunction, maxSteps)
			#print("Finished in {} steps. Num clusters: {}".format(step, clustersCount))
			else:
				#log.info("Finished in {} steps. Num clusters: {}".format(step, clustersCount))
				break

		clusters = newClusters
	return newClusters, centroids

# Public interface for a given number of points.
def kMeans_points(data, clustersCount, distanceFunction=lambda a, b : np.linalg.norm(a - b, axis=-1), \
	initializationFunction=lambda a, b : randomPick(a, b), maxSteps=15, percentChange=1):
	return kMeans(data, clustersCount, distanceFunction, initializationFunction, maxSteps, percentChange)

# Public interface function
# param image The image on which the algoritmh is run. Expeced RGB image.
# param clustersCount The number of clusters (K)
# param distanceFunction Callback that computes the distance between two points
# param initialization The type of initialization
def kMeans_image(image, clustersCount, distanceFunction=lambda a, b : np.linalg.norm(a - b, axis=-1), \
	initializationFunction=lambda a, b : randomPick(a, b), maxSteps=15, **kwargs):
	# kMeans returns a tuple: clustered_data, centroids
	clustered_data, centroids = kMeans(squash_array(image, [2, 1]), clustersCount, distanceFunction, \
		initializationFunction, maxSteps, **kwargs)
	clustered_image = clustered_data.reshape(image.shape[0], image.shape[1])
	# TODO: centroids should probably be translated in 2D represenation.
	return clustered_image, centroids
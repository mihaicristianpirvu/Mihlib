import numpy as np

def PrincipalComponentAnalysis(data, M):
    assert len(data.shape) == 2 and data.shape[-1] >= M
    # Compute the covariance matrix of the data
    cov = np.cov(data.T)
    # Get the eigenvalues/eigenvectors of the covariance matrix
    eVal, eVec = np.linalg.eig(cov)
    # Get top M eigenvalues positions
    argSort = np.argsort(eVal)[-M : ]
    # Keep top M eigenvectors w.r.t eigenvalues positions
    eVec = eVec[:, argSort]
    # Transform the data 
    newData = data @ eVec
    return newData
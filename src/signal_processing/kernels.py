import numpy as np
from utilities.others import isNumber
from utilities.constants import *

# This only checks for correct shape for now. Not all kernels need it (gaussian needs specific things)
def kernel_base(kwargs):
	# Default shape is 3x3
	if not "shape" in kwargs:
		kwargs["shape"] = (3, 3)
	elif isNumber(kwargs["shape"]):
		kwargs["shape"] = (kwargs["shape"], )

	kwargs["normalize"] = False if "normalize" not in kwargs else kwargs["normalize"]

	# Only odd kernels allowed
	for i in range(len(kwargs["shape"])):
		assert(kwargs["shape"][i] % 2 == 1)

	kwargs["type"] = float if "type" not in kwargs else kwargs["type"]

# Creates the identity kernel with 1 the middle position and 0 everywhere else.
# @param kwargs["shape"] Should be a 2D shaped tuple (x, x). Default it is set up to (3, 3).
# @return Returns the (shape x shape) square identity kernel.
def identity_kernel(**kwargs):
	kwargs["shape"] = (3, 3) if not "shape" in kwargs else kwargs["shape"]
	kernel_base(kwargs)
	assert len(kwargs["shape"]) == 2 and kwargs["shape"][0] == kwargs["shape"][1]

	Kernel = np.zeros(kwargs["shape"])
	middle = (kwargs["shape"][0] // 2, kwargs["shape"][1] // 2)
	Kernel[middle] = 1

	return Kernel

# Creates a box kernel which has the same value on all the elements. It can be used for bluring of an image.
# @param kwargs["value"] The value with which the box is filled.
# @param kwargs["shape"] Should be a 1D or 2D shaped tuple (x, x). Default it is set up to (3, 3).
# @param kwargs["normalize"] Whether the filter is normalized (sums up to 1) or not.
def box_kernel(**kwargs):
	kwargs["value"] = 1 if "value" not in kwargs else kwargs["value"]
	kernel_base(kwargs)

	Kernel = np.ones(kwargs["shape"]) * kwargs["value"]
	Kernel = Kernel / np.sum(Kernel) if kwargs["normalize"] else Kernel
	return Kernel

# 1D gaussian kernel created using the Gaussian distribution formula
# @param sigma The standard deviation of the Gaussian distribution
# @param normalize If True, the kernel is normalized so all elements will add up to 1
# @param kwargs["shape"] Should be integer or 1D shaped tuple (x, 1). Default it is set to (3, 1)
def gaussian_1d_kernel(sigma = 1, **kwargs):
	kwargs["shape"] = (3, 1) if not "shape" in kwargs else kwargs["shape"]
	kernel_base(kwargs)

	assert len(kwargs["shape"]) == 1
	middle = kwargs["shape"][0] // 2

	grid = np.mgrid[-middle : middle + 1].astype(kwargs["type"])
	Kernel = np.exp(-grid ** 2 / (2 * sigma**2)) / (np.sqrt(2 * np.pi) * sigma**2)

	Kernel = Kernel / np.sum(Kernel) if kwargs["normalize"] else Kernel
	return Kernel

# 2D gaussian is made by 2 1D gaussians of same shape multiplied. Therefore can only be rectangle.
def gaussian_2d_kernel(sigma = 1, **kwargs):
	# Pass the 1D shape to the 1D gaussian kernel function
	kwargs["shape"] = 1 if not "shape" in kwargs else kwargs["shape"][I]
	gauss_1d = gaussian_1d_kernel(sigma, **kwargs)
	gauss_1d_t = gauss_1d.reshape(gauss_1d.shape[I], 1)
	return gauss_1d * gauss_1d_t

# Gabor kernel according to "General Road detection from single image" paper.
# @param theta The angle of the sinusoid
# @param Lambda The wavelength of the sinusoid
# @param sigma Standard deviation along x and y for Gaussian part
# @param gamma The shape of the Gaussian
# @param psi The phase of the sinusoidal
# @return A complex typed kernel.
def gabor_2d_kernel(theta, Lambda, sigma=1, gamma=1, psi=0, **kwargs):
	assert(sigma != 0)
	kwargs["type"] = np.complex
	Kernel = kernel_base(kwargs)

	middle = kwargs["shape"][0] // 2
	x, y = np.mgrid[-middle : middle + 1, -middle : middle + 1]

	ct1 = -0.5 / (sigma**2)
	ct2 = np.pi * 2 / Lambda

	a =  x * np.cos(theta) + y * np.sin(theta)
	b = -x * np.sin(theta) + y * np.cos(theta)
	Kernel = np.exp(ct1 *(a**2 + gamma**2 * b**2)) * np.exp(ct2 * a * 1j + psi)

	# To be compliant with open cv, apparently I need to transpose it.
	return Kernel.T
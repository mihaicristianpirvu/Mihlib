import numpy as np
from utilities.cpp_utilities import *
from utilities.constants import *

lib_convolve_classic = ffi.dlopen(cpp_root_path + os.sep + getLibraryName("convolve_classic"))
ffi.cdef('''
	int call_convolve_classic(struct NumpyArray *, struct NumpyArray *, struct NumpyArray *);
''')

# Simple implementation of classic convolution operation for 3D inputs (or 2D inputs with depth=1)
def convolve_3d_classic(input, filter):
	original_shape = input.shape
	assert len(original_shape) in (2, 3)

	# for 2D make it (h, w, 1) and reshape at end back to (h, w)
	if len(original_shape) == 2:
		input = input.reshape((*original_shape, 1))

	input = input.astype("float64")
	filter = filter.astype("float64")
	output = np.zeros(input.shape, dtype="float64")

	param_in = prepareInterfaceArray(input)
	param_filter = prepareInterfaceArray(filter)
	param_out = prepareInterfaceArray(output)
	returnCode = lib_convolve_classic.call_convolve_classic(param_in, param_filter, param_out)
	assert returnCode == 0

	return output.reshape(original_shape)

# OpenCV version of convolve_2d
def convolve_2d_cv(input, filter, result=None):
	from cv2 import filter2D, CV_32F
	if result is None:
		result = input
	result = filter2D(input, CV_32F, filter, result)
	return result

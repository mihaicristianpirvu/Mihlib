# others.py -- Module that implements different data mining algorithms
import numpy as np
# from machine_learning.unsupervised_learning.kmeans import kMeans
import machine_learning.public_api
from utilities.public_api import randomPick

def kMeans_points(data, clustersCount, distanceFunction=lambda a, b : np.linalg.norm(a - b, axis=-1), \
	initializationFunction=lambda a, b : randomPick(a, b), maxSteps=15, percentChange=1):
	return machine_learning.public_api.kMeans(data, clustersCount, distanceFunction, initializationFunction, maxSteps, percentChange)

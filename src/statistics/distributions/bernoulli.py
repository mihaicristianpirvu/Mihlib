from .binomial import Binomial

# Bernoulli is just a Binomial distribution with n=1.
# When you think "Bernoulli", just think "(possibly unfair) coin toss."
class Bernoulli(Binomial):
	def __init__(self, p):
		super().__init__(p, 1)
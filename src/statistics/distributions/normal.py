from .distribution import ContinuousDistribution
import numpy as np

# Normal (Gaussian) distribution is a continuous distribution, parametrized by its mean and standard deviation.
class Normal(ContinuousDistribution):
	def __init__(self, mean, std):
		self.mean = mean
		self.std = std

	# Uses the Box-Muller transformation to map 2 uniform variables in [0, 1] to 2 normal variables.
	# Idea: https://goo.gl/tLDnns
	def sample(self):
		u1, u2 = np.random.uniform(0, 1, size=(2, ))
		z = np.sqrt(-2 * np.log(u1)) * np.cos(2 * np.pi * u2)
		return z * self.std + self.mean
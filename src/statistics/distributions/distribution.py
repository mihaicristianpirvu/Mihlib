import numpy as np

# What does a generic distribution need? PDF, parameters, sampling, PDF plot, entropy, mean, variance, KL??
# Just 1D for now!
class Distribution:
	# Samples values from a given distribution one time.
	# @return An array of shape (distributionValues, ), where distributionValues is distribution specific (for example
	#   Binomial has a sequence parameter (n), so the result is shaped (n, )).
	def sample(self):
		raise NotImplementedError("Not yet implemented...")

	# Samples n times from the distribution
	# @param[in] n The number of times to independently sample
	# @return An array of shape (n, distributionValues)
	def sample_n(self, n):
		assert n >= 1
		first_result = self.sample()
		if n == 1:
			return first_result
		else:
			# results are either a number or an array (if one sample is itself an array)
			shape = (n, len(first_result)) if isinstance(first_result, np.ndarray) else (n, )
			results = np.zeros(shape=shape)
			results[0] = first_result
			for i in range(1, n):
				results[i] = self.sample()
			return results

	def pmf(self, value):
		raise NotImplementedError("Not yet implemented...")

	def pdf(self, value):
		raise NotImplementedError("Not yet implemented...")		

class ContinuousDistribution(Distribution):
	pass

class DiscreteDistribution(Distribution):
	pass
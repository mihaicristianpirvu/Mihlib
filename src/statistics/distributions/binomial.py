from .distribution import DiscreteDistribution
import numpy as np

# Binomial distribution. Discrete distribution that measures the number of successes in a sequence of n independent 
#  experiments asking yes/no question.
class Binomial(DiscreteDistribution):
	def __init__(self, p, n):
		assert p >= 0 and p <= 1 and n >= 1
		self.p = p
		self.n = n

	def sample(self):
		return (np.random.uniform(0, 1, size=(self.n, )) < self.p).astype(np.int32)
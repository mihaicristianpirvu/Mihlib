from .bernoulli import Bernoulli
from .binomial import Binomial
from .normal import Normal
from .uniform import DiscreteUniform
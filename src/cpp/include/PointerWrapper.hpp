/* PointerWrapper.hpp Basic pointer wrapper, that has 2 behaviours based on whether a pointer must be allocated,
 *  so the data from a given pointer is just copied, or we wrap on top of it, ignore any allocations and never free
 *  any memory, as it is expected that the memory management is done by other component (i.e. Python interpeter) */

template <typename T, bool OwnsMemory>
class PointerWrapper {
public:
	PointerWrapper(size_t);
	PointerWrapper(T *, size_t);
	~PointerWrapper();

	T *basePointer;
};

template <typename T, bool OwnsMemory>
PointerWrapper<T, OwnsMemory>::PointerWrapper(size_t N) {
	if(OwnsMemory == false)
		return;

	/* Zero initialize with () */
	this->basePointer = new T[N]();
	if (this->basePointer == nullptr)
		throw std::runtime_error("Memory allocation failed.");
}

template <typename T, bool OwnsMemory>
PointerWrapper<T, OwnsMemory>::PointerWrapper(T *pointer, size_t N) : PointerWrapper(N) {
	if(OwnsMemory) {
		std::memcpy(this->basePointer, pointer, N);
	}
	else {
		this->basePointer = pointer;
	}
}

template <typename T, bool OwnsMemory>
PointerWrapper<T, OwnsMemory>::~PointerWrapper() {
	if(OwnsMemory)
		delete this->basePointer;
}
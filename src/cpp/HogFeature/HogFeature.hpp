#ifndef HOG_FEATURE_HPP
#define HOG_FEATURE_HPP

#include "Tensor.hpp"
#include <array>
#include <cassert>
#include <cmath>
#include <functional>
#include <iostream>
#include <string>
#include <tuple>

class HogFeature {
public:
	enum HogNormalization {
		L1 = 0,
		L1_SQRT,
		L2,
		L2_HYS,
		NONE
	};

	HogFeature(const Image2D_<double> &, const Image2D_<double> &, size_t, std::pair<size_t, size_t>,
		std::pair<size_t, size_t>, HogNormalization);
	friend std::ostream &operator<<(std::ostream &, const HogFeature &);

private:
	void computeGradientsMagnitudeOrientation();
	uint8_t computeOrientationBin(std::pair<size_t, size_t>);
	void computeCellsHistogram();
	void computeDescriptor();

public:
	/* Memory is not owned. */
	const Image2D_<double> image;
	const size_t orientations;
	const std::pair<size_t, size_t> pixelsPerCell, cellsPerBlock;
	const HogNormalization normalization;
	const std::pair<size_t, size_t> cellsCount, numBlocks;
	/* cellsPerBlock x cellsPerBlock x numOrientations x numBlocks x numBlocks */
	std::tuple<size_t, size_t, size_t, size_t, size_t> descriptorShape;
	Image2D<double> gradYImage, gradXImage;
	Tensor<double, true, 2> magnitudeImage;
	Tensor<uint8_t, true, 2> orientationImage;
	/* Data for cells */
	Tensor<double, true, 3> cellHistogram;
	/* Final descriptor as a concatenation of blocks computed from cells normalized by their sums */
	Tensor<double, false, 5> descriptor;
};

extern "C" {
	int call_HogFeature(struct NumpyArray *, struct NumpyArray *, size_t, size_t, size_t, size_t, size_t, size_t);
}

#endif /* HOG_FEATURE_HPP */
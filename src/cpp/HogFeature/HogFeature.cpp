#include "HogFeature.hpp"

static std::function<double(double)> normalizationCallback1(HogFeature::HogNormalization);
static std::function<double(double)> normalizationCallback2(HogFeature::HogNormalization);
static std::function<double(double)> normalizationCallback3(HogFeature::HogNormalization);

int call_HogFeature(struct NumpyArray *npImage, struct NumpyArray *npOutImage, size_t orientations,
	size_t pixels_per_cell_i, size_t pixels_per_cell_j, size_t cells_per_block_i, size_t cells_per_block_j,
	size_t normalization) {

	HogFeature hog(npImage, npOutImage, orientations, {pixels_per_cell_i, pixels_per_cell_j},
		{cells_per_block_i, cells_per_block_j}, (HogFeature::HogNormalization)normalization);
	// std::cerr << hog << "\n";
	return 0;
}

std::ostream &operator<<(std::ostream &os, const HogFeature &hog)
{
	os << "HoG. Image shape: " << hog.image.shape() << " ; Orientations: " << hog.orientations
		<< " ; Pixels per cell: " << hog.pixelsPerCell << " ; Cells per block: " << hog.cellsPerBlock
		<< " ; Cells count: " << hog.cellsCount << " ; Num blocks: " << hog.numBlocks
		<< " ; Descriptor shape: " << hog.descriptor.shape() << " ; Normalization Type: " << (int)hog.normalization;
	return os;
}

HogFeature::HogFeature(const Image2D_<double> &image, const Image2D_<double> &outImage, size_t orientations,
	std::pair<size_t, size_t> pixelsPerCell, std::pair<size_t, size_t> cellsPerBlock, HogNormalization normalization)
	: image(image), orientations(orientations),
	pixelsPerCell(pixelsPerCell), cellsPerBlock(cellsPerBlock), normalization(normalization),
	cellsCount({image.shape(0) / pixelsPerCell.first, image.shape(1) / pixelsPerCell.second}),
	numBlocks({cellsCount.first - cellsPerBlock.first + 1, cellsCount.second - cellsPerBlock.second + 1}),
	gradYImage(image.shape()), gradXImage(image.shape()),
	magnitudeImage(image.shape()), orientationImage(image.shape()),
	cellHistogram(cellsCount.first, cellsCount.second, orientations),
	/* descriptor shape: (2 * 2 independent blocks) * (9 orientations) * (15 * 7 total blocks) */
	descriptor(outImage.view(numBlocks.first, numBlocks.second, cellsPerBlock.first,
		cellsPerBlock.second, orientations)) {

	/* First step is to populate the magnitude and orientation images with values for each pixel */
	this->computeGradientsMagnitudeOrientation();

	/* Then, after the magnitude and orientation images are computed, we can create the cell histograms, where the
	 * values at each histogram represents the magnitude of the orientation of that bin (i.e. if 0-20 degrees, add
	 * magnitude only to that bin) */
	this->computeCellsHistogram();

	/* Final step is to compute the descriptor from the computed blocks */
	this->computeDescriptor();
}

void HogFeature::computeGradientsMagnitudeOrientation() {
	size_t height = this->image.shape(0), width = this->image.shape(1);

	this->gradYImage(0, 0) = this->image(1, 0) - this->image(0, 0);
	this->gradXImage(0, 0) = this->image(0, 1) - this->image(0, 0);
	this->magnitudeImage(0, 0) = std::hypot(this->gradYImage(0, 0), this->gradXImage(0, 0));
	this->orientationImage(0, 0) = this->computeOrientationBin({0, 0});

	this->gradYImage(0, width - 1) = this->image(1, width - 1) - this->image(0, width - 1);
	this->gradXImage(0, width - 1) = this->image(0, width - 1) - this->image(0, width - 2);
	this->magnitudeImage(0, width - 1) = std::hypot(this->gradYImage(0, width - 1), this->gradXImage(0, width - 1));
	this->orientationImage(0, width - 1) = this->computeOrientationBin({0, width - 1});

	this->gradYImage(height - 1, 0) = this->image(height - 1, 0) - this->image(height - 2, 0);
	this->gradXImage(height - 1, 0) = this->image(height - 1, 1) - this->image(height - 1, 0);
	this->magnitudeImage(height - 1, 0) = std::hypot(this->gradYImage(height - 1, 0), this->gradXImage(height - 1, 0));
	this->orientationImage(height - 1, 0) = this->computeOrientationBin({height - 1, 0});

	this->gradYImage(height - 1, width - 1) = this->image(height - 1 , width - 1)
		- this->image(height - 2, width - 1);
	this->gradXImage(height - 1, width - 1) = this->image(height - 1, width - 1)
		- this->image(height - 1, width - 2);
	this->magnitudeImage(height - 1, width - 1) =
		std::hypot(this->gradYImage(height - 1, width - 1), this->gradXImage(height - 1, width - 1));
	this->orientationImage(height - 1, width - 1) = this->computeOrientationBin({height - 1, width - 1});

	/* Start with first and last columns */
	for(size_t i = 1; i < height - 1; i++) {
		this->gradYImage(i, 0) = (this->image(i + 1, 0) - this->image(i - 1, 0)) / 2;
		this->gradXImage(i, 0) = this->image(i, 1) - this->image(i, 0);
		this->magnitudeImage(i, 0) = std::hypot(this->gradYImage(i, 0), this->gradXImage(i, 0));
		this->orientationImage(i, 0) = this->computeOrientationBin({i, 0});

		this->gradYImage(i, width - 1) = (this->image(i + 1, width - 1) - this->image(i - 1, width - 1)) / 2;
		this->gradXImage(i, width - 1) = this->image(i, width - 1) - this->image(i, width - 2);
		this->magnitudeImage(i, width - 1) = std::hypot(this->gradYImage(i, width - 1),
			this->gradXImage(i, width - 1));
		this->orientationImage(i, width - 1) = this->computeOrientationBin({i, width - 1});
	}

	/* Continue with first and last lines */
	for(std::size_t j = 1; j < width - 1; j++) {
		this->gradYImage(0, j) = this->image(1, j) - this->image(0, j);
		this->gradXImage(0, j) = (this->image(0, j + 1) - this->image(0, j - 1)) / 2;
		this->magnitudeImage(0, j) = std::hypot(this->gradYImage(0, j), this->gradXImage(0, j));
		this->orientationImage(0, j) = this->computeOrientationBin({0, j});

		this->gradYImage(height - 1, j) = this->image(height - 1, j) - this->image(height - 2, j);
		this->gradXImage(height - 1, j) = (this->image(height - 1, j + 1) - this->image(height - 1, j - 1)) / 2;
		this->magnitudeImage(height - 1, j) = std::hypot(this->gradYImage(height - 1, j),
			this->gradXImage(height - 1, j));
		this->orientationImage(height - 1, j) = this->computeOrientationBin({height - 1, j});
	}

	/* The rest of the matrix is generated applying [-1, 0, 1] kernel. */
	for(size_t i = 1; i < height - 1; i++) {
		for(size_t j = 1; j < width - 1; j++) {
			/* Gy: [i][j] = ([i + 1][j] - [i - 1][j]) / 2 */
			/* Gx: [i][j] = ([i][j + 1] - [i][j - 1]) / 2 */
			this->gradYImage(i, j) = (this->image(i + 1, j) - this->image(i - 1, j)) / 2;
			this->gradXImage(i, j) = (this->image(i, j + 1) - this->image(i, j - 1)) / 2;
			this->magnitudeImage(i, j) = std::hypot(this->gradYImage(i, j), this->gradXImage(i, j));
			this->orientationImage(i, j) = this->computeOrientationBin({i, j});
		}
	}
}

uint8_t HogFeature::computeOrientationBin(std::pair<size_t, size_t> position) {
	/* Atan2 = [-pi, pi] => Atan2 + pi = [0, 2 * pi] */
	double orientation = (std::atan2(this->gradYImage(position.first, position.second),
		this->gradXImage(position.first, position.second)) + M_PI) * 180 / M_PI;
	/* This is python's equivalent to orientation%180 for doubles (do %180 for int and add the value after decimal) */
	// if(HogFeature::histogram_degrees == 180)
	orientation = (int)orientation % 180 + (orientation - (int)orientation);

	/* 36 / (180/9) = 36 / 15 = 2 => bin 2 */
	uint8_t bin = (uint8_t)(orientation / (180 / this->orientations));
	assert(bin < this->orientations);

	return bin;
}

void HogFeature::computeCellsHistogram() {
	/* Each block has 2 components, the histogram (9 numbers for all orientations) and 1 sum which is used for block
	 * normalization at the next step. */
	size_t cellShape = this->pixelsPerCell.first * this->pixelsPerCell.second;
	for(size_t cell_i = 0; cell_i < this->cellsCount.first; cell_i++) {
		for(size_t cell_j = 0; cell_j < this->cellsCount.second; cell_j++) {
			/* Each cell has a rectangular shape, which can be computed using the cell indexes
			* example: 8x8 blocks, (5, 3)th block: (40:48, 24:32) */
			const std::pair<size_t, size_t>
				startIndex_i = {cell_i * this->pixelsPerCell.first, (cell_i + 1) * this->pixelsPerCell.first},
				startIndex_j = {cell_j * this->pixelsPerCell.second, (cell_j + 1) * this->pixelsPerCell.second};

			for(size_t i = startIndex_i.first; i < startIndex_i.second; i++) {
				for(size_t j = startIndex_j.first; j < startIndex_j.second; j++) {
					uint8_t bin = this->orientationImage(i, j);
					double value = this->magnitudeImage(i, j) / cellShape;
					this->cellHistogram(cell_i, cell_j, bin) += value;
				}
			}
		}
	}
}

void HogFeature::computeDescriptor() {
	auto callback1 = normalizationCallback1(this->normalization);
	auto callback2 = normalizationCallback2(this->normalization);
	auto callback3 = normalizationCallback3(this->normalization);

	for(size_t block_i = 0; block_i < this->numBlocks.first; block_i++) {
		for(size_t block_j = 0; block_j < this->numBlocks.second; block_j++) {
			/* For each block (block_i, block_j), compute the normalization sum of the cells */
			double normalizationValue = 0;
			/* For block (i, j) we have cells (i : i + numCells, j : j + numCells) */
			for(size_t cell_i = block_i; cell_i < block_i + this->cellsPerBlock.first; cell_i++) {
				for(size_t cell_j = block_j; cell_j < block_j + this->cellsPerBlock.second; cell_j++) {
					for(size_t bin = 0; bin < this->orientations; bin++) {
						normalizationValue += callback1(this->cellHistogram(cell_i, cell_j, bin));
					}
				}
			}
			normalizationValue = callback2(normalizationValue);

			for(size_t cell_i = block_i; cell_i < block_i + this->cellsPerBlock.first; cell_i++) {
				for(size_t cell_j = block_j; cell_j < block_j + this->cellsPerBlock.second; cell_j++) {
					for(size_t orientation = 0; orientation < this->orientations; orientation++) {
						double value = this->cellHistogram(cell_i, cell_j, orientation) / normalizationValue;
						value = callback3(value);
						this->descriptor(block_i, block_j, cell_i - block_i, cell_j - block_j, orientation) = value;
					}
				}
			}
		}
	}
}

std::function<double(double)> normalizationCallback1(HogFeature::HogNormalization normalization) {
	if(normalization == HogFeature::HogNormalization::L1 || normalization == HogFeature::HogNormalization::L1_SQRT)
		return [](double value) { return std::abs(value); };
	else if(normalization == HogFeature::HogNormalization::L2) {
		return [](double value) { return value * value; };
	}
	else if(normalization == HogFeature::HogNormalization::NONE) {
		return [](double) { return 0; };
	}
	return nullptr;
}

std::function<double(double)> normalizationCallback2(HogFeature::HogNormalization normalization) {
	if(normalization == HogFeature::HogNormalization::L1 || normalization == HogFeature::HogNormalization::L1_SQRT)
		return [](double value) { return value + 1e-5; };
	else if(normalization == HogFeature::HogNormalization::L2) {
		return [](double value) { return std::sqrt(value + 1e-10); };
	}
	else if(normalization == HogFeature::HogNormalization::NONE) {
		return [](double) { return 1; };
	}
	return nullptr;
}

std::function<double(double)> normalizationCallback3(HogFeature::HogNormalization normalization) {
	if(normalization == HogFeature::HogNormalization::L1_SQRT) {
		return [](double value) { return std::sqrt(value); };
	}
	else if (normalization == HogFeature::HogNormalization::L1 || normalization == HogFeature::HogNormalization::L2
		|| normalization == HogFeature::HogNormalization::NONE) {
		return [](double value) { return value; };
	}
	return nullptr;
}
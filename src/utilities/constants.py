# constants.py File used to hold different global constant variables used throughout the library.
import os

# Global base path
global_lib_path = os.path.dirname(os.path.realpath(__file__))[: -len("utilities/")]
cpp_root_path = global_lib_path + os.sep + "cpp"

# Global constants
INF = 99999
EPS = 1e-4

# Indexes used for common convention (points, lines, etc.)
X = 0
Y = 1
Z = 2

# Indexes used for numpy image convention. Note that index[I] should be index[-Y] (because I represents the -Y axis)
I = 0
J = 1

# Indexes for RGB
R = 0
G = 1
B = 2
import numpy as np
import os
from cffi import FFI

# Used for functions implemented in C/CPP to communicate with numpy arrays.
ffi = FFI()
ffi.cdef('''
	enum NumpyArrayType {
		NP_UINT8 = 0,
		NP_INT64,
		NP_FLOAT32,
		NP_FLOAT64
	};

	struct NumpyArray {
		char *data;
		size_t *shape;
		size_t shapeSize;
		enum NumpyArrayType type;
	};
''')

def prepareInterfaceArray(npArray):
	param = ffi.new("struct NumpyArray *")
	param.data = ffi.cast("char *", npArray.ctypes.data)
	param.shape = ffi.cast("size_t *", np.array(npArray.shape).ctypes.data)
	param.shapeSize = len(npArray.shape)

	if npArray.dtype == np.uint8:
		param.type = 0
	elif npArray.dtype == np.int32:
		param.type = 1
	elif npArray.dtype == np.int64:
		param.type = 2
	elif npArray.dtype == np.float32:
		param.type = 3
	elif npArray.dtype == np.float64:
		param.type = 4
	else:
		sys.stderr.write("Only uint8, int32, int64, float32  and float64 are supported.\n")
		sys.exit(-1)

	return param

# If convolve_classic => convolve_classic/convolve_classic.so (or .dll)
def getLibraryName(baseName):
	return baseName + os.sep + baseName + ".dll" if os.name == "nt" else baseName + os.sep + baseName + ".so"
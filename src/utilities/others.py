# others.py -- Module that implements stuff I don't know where else to put
import numpy as np
import os
import random
from scipy.linalg import qr
from utilities.constants import *

# Collapse a 3D array into a 2D array using the values on the 3rd dimension.
# Usefulness: (i,j) = (255, 13, 12) => (i, j) = 255 if collapse is 0
def collapse_array(data, collapse=0):
	assert(len(data.shape) >= 3)
	new_data = np.copy(data[:, :, collapse])
	return new_data

# Given an array, reshape it based on the dimensions parameter which represents how many consecutive dimensions to be
#  squashed together.
# Example: (100, 100, 3), dimensions = (2, 1) => (10000, 3)
def squash_array(data, dimensions):
	newDimensions = []
	assert np.sum(np.array(dimensions)) == len(data.shape)
	index = 0
	for i in range(len(dimensions)):
		value = 1
		for j in range(dimensions[i]):
			value *= data.shape[index]
			index += 1
		newDimensions.append(value)
	return data.reshape(newDimensions)

def printf(format, *args):
	sys.stdout.write(format % args)

def minMax(x, y):
	return (x, y) if x < y else (y, x)

# Returns the key of the biggest value in the dictionary
def dict_argmax(d):
	 v = list(d.values())
	 k = list(d.keys())
	 return k[v.index(max(v))]

def getNeighbourIndexes(i, j):
	return [(i-1, j-1), (i-1, j), (i-1, j+1), (i, j-1), (i, j+1), (i+1, j-1), (i+1, j), (i+1, j+1)]

# Given an array_shape (i.e image shape), a position and a range, return a list of all 2D positions that are inside the
# array and correspond to the required range. Example: position=(5,5) and range=(3,3) =>
# [ (4,4), (4,5), (4,6), (5,4), (5,6), (6,4), (6,5), (6,6) ]
# TODO: generic function for N dimensions and unit tests for multiple shapes.
def getNeighbouringIndex_2D(array_shape, position, Range):
	assert(len(array_shape) == 2)
	assert(len(Range) == 2)
	assert(Range[I] == Range[J]) # Temporary, only allow squared shapes until we add unit tests.

	result = []
	for i in range(position[I] - Range[I] // 2, position[I] + (Range[I] // 2 + 1)):
		if i < 0 or i >= array_shape[I]:
			continue
		for j in range(position[J] - Range[J] // 2, position[J]+ (Range[J] // 2 + 1)):
			if j < 0 or j >= array_shape[J]:
				continue
			result.append((i,j))
	return result

# Similar to python's range, but can have a float steps
def flrange(left, right, N, method="count"):
	assert method in ("step", "count")
	L = []
	left, right = minMax(left, right)
	value = left

	if method == "step":
		step = N
	elif method == "count":
		# 1 less points, so the last one is the end of the interval itself.
		step = (right - left) / (N - 1)

	# Use this intead of value != right for obvious float errors.
	while abs(value - right) > EPS:
		L.append(value)
		value += step
	L.append(right)
	return np.array(L)

# Give me the indexes between base and x, stepping by step
def getStepIndex(x, base, step):
	return (x - base) // step

# Given an object, return a list with all it's callable functions
def getCallable(obj):
	return [method for method in dir(obj) if callable(getattr(obj, method))]

def isNumber(x):
	return isinstance(x, int) or isinstance(x, float) or isinstance(x, np.int) or isinstance(x, np.float) \
		or isinstance(x, np.int64)

# Used to compare arrays
def close_enough(a, b, eps=5 * EPS):
	return np.prod(np.abs(a - b) < eps)

# (0, 4, 256) => [0, 64, 128, 192, 256]
def getIntervalArray(start, count, end):
	step = (end - start) // count
	result = []
	# Last step should capture the division error too, for example: (0, 3, 10) => (0, 3, 6, 10) (not (0, 3, 6, 9))
	for i in range(count):
		result.append(i * step)
	result.append(end)
	return result

# data An 1D array having values (intensities) at each index which are put into bins
# bins A list of sorted values representing each bin: [1, 2, 5, 7, 9] means 4 bins: [1, 2), [2, 5), [5, 7), [7, 9)
# Returns a list of intensities of the same len as the number of bins
def histogram(data, bins):
	assert len(data.shape) == 1
	result = np.zeros(len(bins) - 1)

	for i in range(len(bins) - 1):
		bin = (bins[i], bins[i + 1])
		assert bin[0] < bin[1]

		count = np.where(np.logical_and(data >= bin[0], data < bin[1]))
		result[i] = len(count[0])
	return result

def npGetInfo(data):
	return "Shape: %s. Min: %s. Max: %s. Mean: %s. Std: %s. Dtype: %s" % \
		(data.shape, np.min(data), np.max(data), np.mean(data), np.std(data), data.dtype)

# Converts the result from the histogram function above to an array that works with matplotlib (plot_hist)
# [0, 2, 1, 2, 0] => [1, 1, 2, 3, 3]
def getPlottableHistogramArray(data):
	result = []
	for i in range(len(data)):
		value = data[i]
		result.extend( int(value) * [i] )
	return np.array(result)

# iota(5) = [0, 1, 2, 3, 4]
def iota(n, start=0):
	result = []
	for i in range(n):
		result.append(start + i)
	return result

# Generate N points in the given range, that must satisfy the checkFunction property
# The dimensions of the points will be based on the shape of the range (default 2D)
# The checkFunction will be provided by the user to assert the validity of the generated points (if None all are valid)
def generateRandomPoints(N=100, _range=((0, 99), (0, 99)), checkFunction=None):
	L = []

	for i in range(N):
		P = []
		for j in range(len(_range)):
			dim_range = _range[j]
			assert(len(dim_range) == 2)
			P.append(randomInteger(dim_range[0], dim_range[1]))
		L.append(P)
	return np.array(L)

def skewSymmetricMatrix(v):
	assert len(v) == 3
	return np.array([[0, -v[Z], v[Y]], [v[Z], 0, -v[X]], [-v[Y], v[X], 0]])

def toHomogeneous(x):
	return np.array([*list(x), 1])

def fromHomogeneous(x):
	return x[0 : -1] / x[-1]

def rq(A):
	Q,R = qr(np.flipud(A).T)
	R = np.flipud(R.T)
	Q = Q.T
	return R[:,::-1], Q[::-1,:]

# This could get an utilities/os.py directory on itself. TODO.
def changeDirectory(directoryName, mode="shred", force=False):
	import os
	import sys
	import shutil

	assert mode in ("shred", "normal")

	print("Changing to working directory:", directoryName)
	if os.path.exists(directoryName):
		if mode == "shred":
			if force == False:
				print("Directory", directoryName ,"already exists, files may be overwritten. Confirm [y/n].")
				choice = input().lower().strip()
				if not choice == "y":
					sys.exit(0)
			shutil.rmtree(directoryName)
			os.makedirs(directoryName)
		else:
			print("Directory", directoryName, "already exist, just going there. Writing may overwrite other files.")
	else:
		print("Directory", directoryName, "does not exist. Creating.")
		os.makedirs(directoryName)
	os.chdir(directoryName)

def testReader(reader, type, miniBatchSize):
	generator = reader.iterate(type, miniBatchSize=miniBatchSize)
	numIterations = reader.getNumIterations(type, miniBatchSize=miniBatchSize)

	print("Num iterations: %d" % (numIterations))
	for i in range(numIterations):
		items = next(generator)
		print("%d/%d" % (i + 1, numIterations))
		for item in items:
			print(npGetInfo(item))

def random_pick(items, N):
	assert N <= len(items)
	perm = np.random.permutation(len(items))
	return items[perm[0 : N]]
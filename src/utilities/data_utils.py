# data_utils.py Module that does different transformations on a set of data, which is usually an array that is the
#  first parameter and has a shape of (N, ...), where N is the number of items in the data.
import numpy as np
from .others import toHomogeneous

# Creates a categorical (1 vs K) array from an 1D array.
# data array of shape (N, ).
# numClasses The number of classes in the returned array. If None, max(data) is taken as numClasses.
# Returns a categorical array (1 vs K) based on the input.
def toCategorical(data, numClasses = None):
	data = np.array(data)
	# Expecting a (N, ) array
	assert(len(data.shape) == 1)
	
	if numClasses is None:
		numClasses = np.max(data) + 1
	N = data.shape[0]

	categoricalData = np.zeros((N, numClasses))
	categoricalData[np.arange(N), data] = 1

	return categoricalData

# Center the data around the mean and normalize it by the standard deviation.
def normalizeData(data, mean, std):
	data -= mean
	data /= std
	return data

# Converts the data in [-1, 1] by doing min max normalization
def minMaxNormalize(data):
	diff = (np.max(data) - np.min(data)) / 2
	data /= diff
	return np.clip(data, -1, 1)

# Borders the data with zeroes. The data and desiredShape must be in 3D
# 1000x28x28x3 => 1000x32x32x3 or 1000x1x28x28 => 1000x3x35x32 is okay, but not 1000x28x28x28x3 => 1000x28x30x30x5 (4D)
# initialShape and desiredShape are for one item (not entire dataset): (32, 32, 3) is ok, (10000, 32, 32, 3) not.
# TODO: add border type: center (as it's now) OR top left ; make it generic (N dimensions)
def borderData(data, desiredShape):
	initialShape = data.shape[1:]
	assert len(initialShape) == len(desiredShape)
	assert len(desiredShape) == 3 # Only 3D bordering supported for now
	for i in range(len(desiredShape)):
		assert desiredShape[i] >= initialShape[i]

	if initialShape == desiredShape:
		return data

	numData = data.shape[0]
	dataShape = (numData, *desiredShape)
	halfs = [0, 0, 0]
	for i in range(3):
		halfs[i] = (desiredShape[i] - initialShape[i]) // 2

	# Problem with 28->35 => values between 3:32, which has shape of 29, so we need 3:31 in this case
	inner_positions = np.array([
		[halfs[0], desiredShape[0] - halfs[0] - ((desiredShape[0] - halfs[0]) % 2 == 1)],
		[halfs[1], desiredShape[1] - halfs[1] - ((desiredShape[1] - halfs[1]) % 2 == 1)],
		[halfs[2], desiredShape[2] - halfs[2] - ((desiredShape[2] - halfs[2]) % 2 == 1)]
	])

	borderedData = np.zeros(dataShape)
	borderedData[:, inner_positions[0, 0] : inner_positions[0, 1], inner_positions[1, 0] : inner_positions[1, 1], \
		inner_positions[2, 0] : inner_positions[2, 1]]
	return borderedData

# N x *OriginalShape => N x *shape
def reshapeData(data, shape):
	return data.reshape(data.shape[0], *shape)

# Computes the isotropic normalization of the data and returns the new data and the tranformation
def isotropicNormalizationData(data):
	assert len(data.shape) == 2 and data.shape[1] in (2, 3)
	newData = np.ones(data.shape)
	dataMean = np.mean(data, axis=0)
	newData = data - dataMean

	scale = np.sqrt(2) / np.mean(np.sqrt(np.sum(newData**2, axis=1)))
	newData *= scale

	T = np.diag(toHomogeneous([scale, scale]))
	T[0 : 2, 2] = -scale * dataMean
	return newData, T

def toHomogeneousData(data):
	newData = np.ones((*data.shape[0 : -1], data.shape[-1] + 1))
	originalShape = newData.shape
	newData.T[0 : -1] = data.T
	return newData

def fromHomogeneousData(data):
	assert data.shape[-1] > 1
	newData = (data.T[0 : -1] / data.T[-1]).T
	return newData

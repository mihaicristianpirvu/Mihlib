import numpy as np

# (N, A, B) x (B, C) => (N, A, C).
# Thanks numpy for optimizing np.matmul (NOT).
def aMatrixMultiply(a, b):
	assert a.shape[2] == b.shape[0]
	result = np.zeros((a.shape[0], a.shape[1], b.shape[1]))
	for i in range(a.shape[0]):
		result[i] = np.dot(a[i], b)
	return result

# (A, B) x (N, B, C) => (N, A, C)
def bMatrixMultiply(a, b):
	assert a.shape[1] == b.shape[1]
	result = np.zeros((b.shape[0], a.shape[0], b.shape[2]))
	for i in range(b.shape[0]):
		result[i] = np.dot(a, b[i])
	return result

# (N, A, B) (N, B, C) => (N, A, C)
def abMatrixMultiply(a, b):
	assert a.shape[0] == b.shape[0] and a.shape[2] == b.shape[1]
	result = np.zeros((a.shape[0], a.shape[1], b.shape[2]))
	for i in range(a.shape[0]):
		result[i] = np.dot(a[i], b[i])
	return result

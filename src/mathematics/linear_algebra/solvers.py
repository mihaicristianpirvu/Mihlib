#solvers.py Module that implements methods for solving different algebraic equations
import numpy as np

# a*x^2 + b*x + c = 0
# Using classical method, return x1, x2 that solves this
def solveQuadraticEquation(paramters=(0, 0, 0)):
	(a, b, c) = paramters
	delta = pow(b, 2) - 4 * a * c
	assert(delta >= 0)
	x1 = (-b - np.sqrt(delta)) / 2 * a
	x2 = (-b + np.sqrt(delta)) / 2 * a
	return (x1, x2)

# a*x + b = 0
def solveLinearEquation(parameters=(0, 0)):
	(a, b) = parameters
	return -b / a

# Returns a least square solution to a given matrix, using the Direct Linear Transform algorithm, using the eigenvector
#  of the smallest eigenvalue. Another solution is using the SVD algorithm, which I might do some other time.
def solveLeastSquares(matrix):
	eigenValues, eigenVectors = np.linalg.eig(matrix.T @ matrix)
	argmin = np.argmin(eigenValues)
	return eigenVectors[:, argmin]

# same as above, but uses the SVD decomposition and last column of D
def solveLeastSquaresSVD(matrix):
	u, v, d = np.linalg.svd(matrix)
	return d[-1]
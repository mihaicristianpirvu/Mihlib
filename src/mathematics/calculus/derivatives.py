def derivative(f, x, h=0.0000001):
	return (f(x + h) - f(x)) / h

def derivative_bidirectional(f, x, h=0.0000001):
	return (f(x + h) - f(x - h)) / (2 * h)

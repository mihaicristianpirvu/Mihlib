import numpy as np
from mathematics.calculus.derivatives import derivative

# Lucas Kanade algorithm for 1D estimation, given a function f and a displacement function g = f(x + h) and finds h.
# Assumes function is differentiable, and h is not very large.
# @param[in] f Input function
# @param[in] g Target function g = f (x + h)
# @param[in] a The lower bound of the interval over which the algorithm is ran.
# @param[in] b The upper bound of the interval over which the algorithm is ran.
# @param[in] step The step over which the interval is binned.
# @return An estimate of h
def lucas_kanade_basic(f, g, a=0, b=1, step=0.01, maxSteps=100):
	x = np.arange((b-a)/step) * step

	h_prev = 0
	i = 0
	while True:
		i += 1
		# compute f, f', g, g' for [a, b] interval with a given step
		f_x = f(x + h_prev)
		f_deriv_x = derivative(f, x + h_prev)
		g_x = g(x)
		g_deriv_x = derivative(g, x)

		# Compute the weight of each derivative w = abs(g'(x) - f'(x))
		w = np.abs(g_deriv_x - f_deriv_x)
		h = h_prev + (np.sum((w * (g_x - f_x)) / f_deriv_x) / np.sum(w))

		if np.abs(h - h_prev) < 1e-5 or step == maxSteps:
			break
		h_prev = h
	return h

# Lucas Kanade algorithm for any number of dimensions. Given a function f and a displacement function g = f(X + h),
#  finds the appropiate finds h.
# Assumes function is differentiable, and h is not very large.
# @param[in] f Input function
# @param[in] g Target function g = f (X + h)
# @param[in] a The lower bound of the interval over which the algorithm is ran.
# @param[in] b The upper bound of the interval over which the algorithm is ran.
# @param[in] step The step over which the interval is binned.
# @return An estimate of h
def lucas_kanade(f, g, a=0, b=1, step=0.01, maxSteps=100):
	x = np.arange((b-a)/step) * step

	h_prev = 0
	i = 0
	while True:
		i += 1
		# compute f, f', g, g' for [a, b] interval with a given step
		f_x = f(x + h_prev)
		f_deriv_x = derivative(f, x + h_prev)
		g_x = g(x)
		g_deriv_x = derivative(g, x)

		# Compute the weight of each derivative w = abs(g'(x) - f'(x))
		w = derivative(f, x)**2
		h = h_prev + np.sum(w * f_deriv_x * (g_x - f_x)) / np.sum(w * f_deriv_x**2)

		if np.abs(h - h_prev) < 1e-5 or step == maxSteps:
			break
		h_prev = h
	return h
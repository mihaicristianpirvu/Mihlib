import importlib

if importlib.util.find_spec("keras"):
	from .keras.neural_network import NeuralNetworkKeras

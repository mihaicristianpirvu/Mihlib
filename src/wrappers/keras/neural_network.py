import joblib
from copy import copy

class Dummy: pass

# Wrapper on top of the Keras model. Added methods for saving and loading a state. To completly implement a Keras
#  model, one must impement the setup method (define layers) and call setOptimizer, setCriterion, setIO and compile
class NeuralNetworkKeras(Dummy):
	def __init__(self):
		# Hacky hack here. Change the base class of this class once an object was created to keras.models.Model. We do
		#  this lazily (and not from beginning), because importing Keras requires to import its backend as well
		#  which will slow ALL the other projects that don't use this neural network module at all.
		import keras
		NeuralNetworkKeras.__bases__ = (keras.models.Model, )

		self.compilable = False
		self.optimizer = None
		self.criterion = None
		# The hyperparameters are saved here (and should only be sent to constructor if they are to be included in the
		#  state of the model. Anything that happens in setup method, is about the learnable parameters s.a. weights, 
		#  layers, etc)
		self.hyperParametersState = copy(vars(self))
		self.setup()

	def setOptimizer(self, optimizer):
		self.optimizer = optimizer

	def setCriterion(self, criterion):
		self.criterion = criterion

	def setIO(self, inputs, outputs):
		import keras
		super(keras.models.Model, self).__init__(inputs=inputs, outputs=outputs)
		self.compilable = True

	def compile(self, **kwargs):
		assert self.compilable, "In order to be compilable, you must call setIO for inputs/outputs of the model"
		super().compile(optimizer=self.optimizer, loss=self.criterion, **kwargs)

	# Handles all the initilization stuff of a specific keras model object.
	def setup(self):
		raise NotImplementedError("Should have implemented this")

	# Method used to save the current state of the model (any hyperparameters that are not trainable, but fixed at
	#  train time and needed at test time).
	def save_state(self, path):
		print("Saving the hyperparamters state of the model to", path)
		joblib.dump(self.hyperParametersState, path)

	# Used to load the state of this object from a previous saved file.
	def load_state(self, path):
		print("Loading the state of the model from", path)
		state = joblib.load(path)
		for key in state:
			setattr(self, key, state[key])
			self.hyperParametersState[key] = state[key]
		self.setup()
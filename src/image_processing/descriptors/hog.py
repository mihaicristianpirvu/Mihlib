import numpy as np
from utilities.cpp_utilities import *
from utilities.constants import *

lib_hog_feature = ffi.dlopen(cpp_root_path + os.sep + getLibraryName("HogFeature"))
ffi.cdef('''
	int call_HogFeature(struct NumpyArray *, struct NumpyArray *, size_t, size_t, size_t, size_t, size_t, size_t);
''')

def HogFeature(input, numOrientations, pixelsPerCell, cellsPerBlock, normalization):
	# Only grayscale
	assert len(input.shape) == 2
	assert input.shape[0] % pixelsPerCell[0] == 0 and input.shape[1] % pixelsPerCell[1] == 0
	numCells = (input.shape[0] // pixelsPerCell[0], input.shape[1] // pixelsPerCell[1])
	assert numCells[0] % cellsPerBlock[0] == 0 and numCells[1] % cellsPerBlock[1] == 0
	numBlocks = (numCells[0] - cellsPerBlock[0] + 1), (numCells[1] - cellsPerBlock[1] + 1)
	output = np.zeros((numBlocks[0] * numBlocks[1] * numOrientations * cellsPerBlock[0] * cellsPerBlock[1]), \
		dtype=np.float64)
	assert np.prod(output.shape) > 0

	assert normalization in ("L1", "L1-sqrt", "L2", "L2-Hys", "none")
	if normalization == "L1":
		numNormalization = 0
	elif normalization == "L1-sqrt":
		numNormalization = 1
	elif normalization == "L2":
		numNormalization = 2
	elif normalization == "L2-Hys":
		assert Falase, "L2-Hys not supported yet"
		numNormalization = 3
	elif normalization == "none":
		numNormalization = 4

	param_in = prepareInterfaceArray(input)
	param_out = prepareInterfaceArray(output)

	returnCode = lib_hog_feature.call_HogFeature(param_in, param_out, numOrientations, \
		pixelsPerCell[0], pixelsPerCell[1], cellsPerBlock[0], cellsPerBlock[1], numNormalization)
	assert returnCode == 0

	return output
#surf.py SURF descriptor and matching functions. Currently requires OpenCV, but plan is to implement myself.
import cv2

# Returns an instance of SURF object which can be used further to detect and compute keypoints and desecriptors
def SurfFeature(hessianThreshold=400, numOctaves=4, numOctaveLayers=3):
	return cv2.xfeatures2d.SURF_create(hessianThreshold, numOctaves, numOctaveLayers)

# Detectes keypoints and descriptors for those keypoints in an image given a SURF instance
def SURF_getKeypointsAndDescriptors(surfDetector, image):
	keyPoints, descriptor = surfDetector.detectAndCompute(image, None)
	return keyPoints, descriptor

# Matches two descriptors, given a matcher instance using KNN
# Returns a list of pairs of coordinates in the images [(p_img1, p_img2)]
def SURF_matchKeypoints(matcher, keyPoints1, descriptor1, keyPoints2, descriptor2, ratio=0.75, k=2):
	raw_matches = matcher.knnMatch(descriptor1, trainDescriptors = descriptor2, k = k)
	mkp1, mkp2 = [], []
	for m in raw_matches:
		if len(m) == 2 and m[0].distance < m[1].distance * ratio:
			m = m[0]
			mkp1.append( keyPoints1[m.queryIdx].pt[::-1] )
			mkp2.append( keyPoints2[m.trainIdx].pt[::-1] )
	kp_pairs = list(zip(mkp1, mkp2))
	return kp_pairs
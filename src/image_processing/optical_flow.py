import numpy as np
from .others import computeIntegralImage, grayScaleImage

# Computes the values of each box using the property of the integral image to fast access a sum of an area
def partialIntegral(integral, windowSize):
	A = integral[2 * windowSize[0] + 1:, 2 * windowSize[1] + 1:]
	B = integral[2 * windowSize[0] + 1:, :-1 - 2 * windowSize[1]]
	C = integral[:-1 - 2 * windowSize[0], 2 * windowSize[1] + 1 : ]
	D = integral[:-1 - 2 * windowSize[0], :-1 - 2 * windowSize[1]]
	return A - B - C + D

# The standard np.grad gives weirder results
def getGradients(image1, image2):
	Ix = np.zeros(image1.shape)
	Iy = np.zeros(image1.shape)
	It = np.zeros(image1.shape)
	Ix[1:-1, 1:-1] = (image1[1:-1, 2:] - image1[1:-1, :-2]) / 2
	Iy[1:-1, 1:-1] = (image1[2:, 1:-1] - image1[:-2, 1:-1]) / 2
	It[1:-1, 1:-1] = image1[1:-1, 1:-1] - image2[1:-1, 1:-1]
	return Iy, Ix, It

# Lucas Kanade algorithm for 2 given images, assumed to be dependent (video frames). Returns the displacement optical
#  flow image for each pixel.
# @param[in] image1 Image at time t
# @param[in] image2 Image at time t + 1
# @param[in] windowSize How large the looked window is (and how small the resulting image is)
# @return An estimate of the optical flow image
def lucas_kanade_image(image1, image2, windowSize=5):
	if len(image1.shape) == 3:
		image1 = grayScaleImage(image1)
	if len(image2.shape) == 3:
		image2 = grayScaleImage(image2)
	if type(windowSize) == int:
		windowSize = (windowSize, windowSize)

	# Compute the gradients, in time and space
	# Iy, Ix, It = *np.gradient(image1), image2 - image1
	Iy, Ix, It = getGradients(image1, image2)

	Ix_2_integral = computeIntegralImage(Ix**2)
	Iy_2_integral = computeIntegralImage(Iy**2)
	Ixy_integral = computeIntegralImage(Ix * Iy)
	Ixt_integral = computeIntegralImage(Ix * It)
	Iyt_integral = computeIntegralImage(Iy * It)

	# Compute partial sums from the original values using the integral image property of area intensity computation.
	Ix_2_partial = partialIntegral(Ix_2_integral, windowSize)
	Iy_2_partial = partialIntegral(Iy_2_integral, windowSize)
	Ixy_partial = partialIntegral(Ixy_integral, windowSize)
	Ixt_partial = partialIntegral(Ixt_integral, windowSize)
	Iyt_partial = partialIntegral(Iyt_integral, windowSize)

	det = Ix_2_partial * Iy_2_partial - Ixy_partial**2

	whereNotSingular = np.where(det != 0)
	sol_y = Ix_2_partial * Iyt_partial - Ixy_partial * Ixt_partial
	sol_x = Iy_2_partial * Ixt_partial - Ixy_partial * Iyt_partial
	flow_y = np.where(det != 0, sol_y / det, 0)
	flow_x = np.where(det != 0, sol_x / det, 0)

	padDiff = image1.shape[0] - flow_y.shape[0], image1.shape[1] - flow_y.shape[1]
	flow_y = np.pad(flow_y, [(0, padDiff[0]), (0, padDiff[1])], mode="constant")
	flow_x = np.pad(flow_x, [(0, padDiff[0]), (0, padDiff[1])], mode="constant")
	return flow_y, flow_x
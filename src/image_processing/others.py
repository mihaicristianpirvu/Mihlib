# others.py -- Module that implements different algorithms regarding image processing or computer vision.
import numpy as np
from PIL import Image
from scipy import misc
from random import randint

from utilities.constants import *
from utilities.others import getNeighbouringIndex_2D, flrange, squash_array, fromHomogeneous, minMax
from utilities.random import randomPick
from mathematics.linear_algebra.interpolations import bilinear_interpolation

# Given a path to an image, and a maxResolution, read that image and return it as a numpy array.
def readImage(path, maxResolution=None):
	im = Image.open(path)
	image = np.array(im, dtype=np.float64)

	if maxResolution != None and (image.shape[I] > maxResolution[0] or image.shape[J] > maxResolution[1]):
		scale = min(maxResolution[0] / image.shape[I], maxResolution[1] / image.shape[J])
		image = misc.imresize(image, scale) # TODO: Maybe use my bilinear interpolation method (?) -- might be unsafe.
	return image

def saveImage(image, path, format="PNG"):
    misc.toimage(image).save(path, format)

# Given an image and 2 (x,y) coordinates, representing the top left corner and bottom right corner, creates a sub image
# that contains both coordinates from the original image
def subImage(image, pos1, pos2):
	assert(pos1[X] < pos2[X] and pos1[Y] < pos2[Y])
	assert(pos1[X] <= image.shape[I] and pos1[Y] <= image.shape[J])
	assert(len(image.shape) in (2, 3))

	return image[pos1[I]:pos2[I], pos1[J]:pos2[J]]

# Given a list of RGB pixels and a tuple image size, greyscale the image
def grayScaleImage(image):
	grayScale = np.zeros((image.shape[0], image.shape[1]), dtype=image.dtype)
	grayScale[:, :] = image[:, :, R] * 0.2989 + image[:, :, G] * 0.5870 + image[:, :, B] * 0.1140
	return grayScale

# Given a 0-255 grayscale image, convert it to a RGB intensity map where pixel of values:
# 0-85 will use only the blue channel
# 86-170 will use the green channel
# 171-255 will use the red channel
# Used to show intensity in a grayscale image (TODO: Algorithm could use work to use combination of colors)
def grayScaleToIntensityImage(image):
	new_image = np.zeros((image.shape[I], image.shape[J], 3))
	for i in range(image.shape[I]):
		for j in range(image.shape[J]):
			new_value = image[i][j]
			if new_value > 255:
				new_value = 255

			if image[i][j] <= 85:
				new_image[i][j] = (0, new_value, 0)
			elif image[i][j] <= 170:
				new_image[i][j] = (0, 0, new_value)
			else:
				new_image[i][j] = (new_value, 0, 0)
	return new_image

# Given an image and a set of labels for each pixels, create a new image that marks every pixel on the same label
# with the same color. The color can be chosen as the average of colors
def segment_image(image, labels, num_clusters):
	color_clusters = [np.zeros(3, dtype=float) for i in range(num_clusters)]
	count_clusters = [0] * num_clusters
	new_image = np.array(image)

	# Add all colors to the mix
	for i in range(image.shape[I]):
		for j in range(image.shape[J]):
			label = labels[i][j]
			color_clusters[label] += image[i][j]
			count_clusters[label] += 1

	# Get unique color
	for i in range(num_clusters):
		color_clusters[i] /= count_clusters[i]

	# Update the image with the new colors
	for i in range(image.shape[I]):
		for j in range(image.shape[J]):
			label = labels[i][j]
			color = color_clusters[label]
			new_image[i][j] = color

	return new_image

# Given an image with pixels starting from 1 to N, assign each type of such pixel an unique colorization
def getUniqueColoredImage(image, countConnectedComponents):
	assert(len(image.shape) == 2)
	maxValue = pow(256, 3)
	division = int(maxValue/countConnectedComponents)
	division = randint(int(division/2), division)
	#print("Division = ", division, "countConnectedComponents=", countConnectedComponents)
	new_image = np.zeros((image.shape[I], image.shape[J], 3), dtype=int)

	double = pow(255, 2)
	for i in range(image.shape[I]):
		for j in range(image.shape[J]):
			pixel = image[i][j]
			value = pixel * division
			new_image[i][j] = (int(value/double), int(value/255) % 255, value % 255)
	return new_image


# Given an image and a threshold, will set all pixels to 1 if predicate returns true, 0 otherwise
def thresholdImage(image, threshold, predicate):
	new_image = np.zeros((image.shape[I], image.shape[J]))
	for i in range(image.shape[I]):
		for j in range(image.shape[J]):
			if predicate(image[i][j], threshold):
				new_image[i][j] = 1
			else:
				new_image[i][j] = 0
	return new_image

# Distance transform of 1D or 2D array (for 2D it's an actualy image)
def distance_transform(l):
	if len(l.shape) == 1:
		return distance_transform_1d(l)
	elif len(l.shape) == 2:
		return distance_transform_2d(l)
	else:
		assert False

# [INF, 0, INF, 0, INF, INF, INF, 0, INF]
# [INF, 0, 1, 0, 1, 2, 3, 0, 1]
# [1, 0, 1, 0, 1, 2, 1, 0 , 1]
def distance_transform_1d(image):
	new_image = INF + np.zeros(image.shape, dtype=int)
	
	for i in range(1, image.shape[I]):
		new_image[i] = min(new_image[i-1]+1, image[i])

	for i in range(image.shape[I]-2, -1, -1):
		new_image[i] = min(new_image[i+1]+1, new_image[i])

	return new_image

def distance_transform_2d(image):
	new_image = INF + np.zeros(image.shape, dtype=int)

	for i in range(image.shape[I]):
		for j in range(image.shape[J]):
			if i >= 1:
				new_image[i][j] = min(new_image[i-1][j] + 1, image[i][j])
			if i < image.shape[I] - 1:
				new_image[i][j] = min(new_image[i][j], new_image[i+1][j] + 1, image[i][j])
			if j >= 1:
				new_image[i][j] = min(new_image[i][j], new_image[i][j-1] + 1, image[i][j])
			if j < image.shape[J] - 1:
				new_image[i][j] = min(new_image[i][j], new_image[i][j+1] + 1, image[i][j])
	for i in range(image.shape[I] - 1, -1, -1):
		for j in range(image.shape[J] - 1, -1, -1):
			if i >= 1:
				new_image[i][j] = min(new_image[i-1][j]+1, new_image[i][j])
			if i < image.shape[I] - 1:
				new_image[i][j] = min(new_image[i+1][j]+1, new_image[i][j])
			if j >= 1:
				new_image[i][j] = min(new_image[i][j-1]+1, new_image[i][j])
			if j < image.shape[J] - 1:
				new_image[i][j] = min(new_image[i][j+1]+1, new_image[i][j])
	return new_image

# Returns the connected components of an image
# param image The processed image
# param background The value of the background pixel, None if none of them are background
# return An array of the same shape as the image (I x J), with the values representing the index of the component
def getConnectedComponents(image, background=None):
	assert(len(image.shape) == 2)

	componentsMatrix = np.zeros(image.shape, dtype=int) - 1
	# 0 will be reserved for background if it exists, otherwise it will be the first component
	currentLabel = 0 if background is None else 1

	l = []
	for i in range(image.shape[I]):
		for j in range(image.shape[J]):
			if image[i, j] == background:
				componentsMatrix[i, j] = 0
				continue

			# If already labeled, step over it
			if componentsMatrix[i, j] != -1:
				continue
			# Otherwise, add to current connected component and add its neighbours to the list and find all of them in
			# a depth search.
			l.append((i, j))
			componentsMatrix[i, j] = currentLabel

			while len(l) != 0:
				current_position = l.pop()
				neighbours = getNeighbouringIndex_2D(image.shape, current_position, (3, 3))
				for neighbour in neighbours:
					# If already labeled or different color, step over
					if componentsMatrix[neighbour] != -1 or image[neighbour] != image[current_position]:
						continue

					l.append(neighbour)
					componentsMatrix[neighbour] = currentLabel

			currentLabel += 1
	return componentsMatrix

def removeWeakConnectedComponents(connectedMatrix, threshold = "argmax"):
	newMatrix = np.zeros(connectedMatrix.shape)

	if threshold == "argmax":
		thresholdIndexes = [np.bincount(connectedMatrix.flatten()).argmax()]
	elif isinstance(threshold, str) and threshold.startswith("top"):
		value = int(threshold[3:])
		assert value > 0
		binCount = np.bincount(connectedMatrix.flatten())
		value = min(value, len(binCount))
		# https://stackoverflow.com/questions/6910641/how-to-get-indices-of-n-maximum-values-in-a-numpy-array
		thresholdIndexes = np.argpartition(binCount, -value)[-value : ]
	else:
		threshold = int(threshold)
		binCounts = np.bincount(connectedMatrix.flatten()) > threshold
		# Find those indexes and iterate through them
		thresholdIndexes = np.where(binCounts == True)[0]

	for i in range(len(thresholdIndexes)):
		# 2D Indexes in the original matrix where this connected component is formed
		componentIndexes = np.where(connectedMatrix == thresholdIndexes[i])
		# Assign the new value for the connected component
		newMatrix[componentIndexes] = (i + 1)

	return newMatrix

def findGravityCenter(image, value):
	(average, N) = ([0, 0], 0)
	for i in range(image.shape[I]):
		for j in range(image.shape[J]):
			if image[i][j] != value:
				continue
			average[0] += i
			average[1] += j
			N += 1
	return (int(average[0]/N), int(average[1]/N))

def findExtremities(image, value):
	(left, right, top, bottom) = (image.shape[J]-1, 0, 0, image.shape[I]-1)
	for i in range(image.shape[I]):
		for j in range(image.shape[J]):
			if image[i][j] != value:
				continue
			left = min(left, j)
			right = max(right, j)
			top = max(top, i)
			bottom = min(bottom, i)
	return (left, right, top, bottom)

# Input:
# - image: NxM
# - K: count of checks
# - D: maximum distance for a point to be in the model
# - C: mininum points needed for a model to be valid
# - value: The value in the image considered for points
def ransac_image(image, K=10, D=2, C=10, value=1):
	best, best_count_fit = None, None
	points = []

	# Get all the points with the good value
	for i in range(image.shape[I]):
		for j in range(image.shape[J]):
			if image[i][j] == value:
				points.append((j,i))
	return ransac_linear_regression(points, K, D, C)

# TODO :hough_transform for greyscale. Define relevant pixels first (0-20, 235-255 maybe not good ?)
def hough_transform(image, bin_interval, num_bins):
	pass

# Scale an image, given a parameter between 0 and 1
# TODO: Add a resize_image(image, newResolution, interpolationType="bilinear") method which supports both
#  downscaling and upscaling in the future. Some algorithms could be implemented in C++/OpenMP for speed.
def bilinear_interpolation_image(image, scale):
	new_image = np.zeros((int(image.shape[I] * scale), int(image.shape[J] * scale)))
	assert scale > 0 and scale < 1

	step = 1 / scale
	new_i = 0
	for i in flrange(0, image.shape[J]-step, step, method="step"):
		new_j = 0
		for j in flrange(0, image.shape[I]-step, step, method="step"):
			# Use the neighbour pixels (this seems okay ?)
			_i = int(i)
			_j = int(j)

			A = (_j, _i+1, image[_j][_i+1])
			B = (_j+1, _i+1, image[_j+1][_i+1])
			C = (_j+1, _i, image[_j+1][_i])
			D = (_j, _i, image[_j][_i])
			new_image[new_j][new_i] = bilinear_interpolation((A,B,C,D))

			new_j += 1
		new_i += 1
	return new_image

# Given an image, compute it's integral image. (Summed area table)
def computeIntegralImage(image):
	S = image
	for i in range(image.ndim):
		S = S.cumsum(axis=i)
	return S

def getIntegralImageValue(image, p1, p2, debug=False):
	a = p1[0] - 1, p1[1] - 1
	b = p1[0] - 1, p2[1]
	c = p2[0], p1[1] - 1
	d = p2

	A = image[a]
	B = image[b]
	C = image[c]
	D = image[d]

	if debug:
		print(a, b, c, d, "=>", A, B, C, D, "=>", A + D - C - B)
	return A + D - C - B

# Given an image, a (i, j) position, a range and a gradient type, get the lowest position in the range that has the
# lowest gradient with regard to the gradient type computation.
# gradient_type: "single_derivative_i" = (-1, 0, 1), which is the difference of right_pixel - left_pixel.
def get_lowest_gradient_position(image, position, Range=(3,3), gradient_type="single_derivative_i"):
	(min_position, min_gradient) = (None, INF)
	assert(Range[I] % 2 == 1 and Range[J] % 2 == 1)

	indexes = getNeighbouringIndex_2D((image.shape[I], image.shape[J]), position, Range)
	for index in indexes:
		i = index[I]
		j = index[J]
		if gradient_type == "single_derivative_i":
			if j - 1 < 0 or j + 1 >= image.shape[J]:
				continue
			gradient = image[i][j+1] - image[i][j-1]

			# RGB
			if len(image.shape) == 3:
				gradient = int(np.mean(gradient))

			if gradient < min_gradient:
				(min_position, min_gradient) = ((i, j), gradient)
	return min_position

# Returns a circle with a given center, radius and shape of the array
def getCircle(shape=(200, 200), center=(100, 100), radius=50):
	x, y = np.mgrid[0 : shape[X], 0 : shape[Y]]
	circle = (x - center[X]) ** 2 + (y - center[Y]) ** 2
	circle_mask = np.logical_and( (x - center[X]) ** 2 + (y - center[Y]) ** 2 < radius ** 2, x < 100 )
	return circle * circle_mask

# Returns the 2d vector with v[i, j] being the angle between center and (i, j)
# max_angle is used to return only an arc of this image
def angleVector(shape, center, max_angle = 2 * np.pi):
	x, y = np.mgrid[0:shape[X], 0:shape[Y]]
	x, y = x - center[X], y - center[Y]
	rotation = np.pi / 2 # By default atan2 transposes the result by pi/2, so we need to rotate it back.
	x, y = x*np.cos(rotation) - y*np.sin(rotation), x*np.sin(rotation) + y*np.cos(rotation)
	result = np.arctan2(y, x) + np.pi # By default it puts values in [-pi, pi], move it to [0, 2pi]
	result = (result <= max_angle) * result
	return result

# TODO : make these generic in MihDataMining and use .flatten() to call that ones.
# Given 2 binary image, a label and a hypothesis, compute the intersection over union of the 2 images
def intersectionOverUnion(label, image):
	assert label.shape == image.shape

	intersectionPoints = np.where(np.logical_and((label == 1), (image == 1)))
	unionPoints = np.where(np.logical_or((label == 1), (image == 1)))
	return len(intersectionPoints[0]) / len(unionPoints[0])

def precision(label, image):
	TP = len(true_positives(label, image)[0])
	FP = len(false_positives(label, image)[0])
	return TP / (TP + FP)

def recall(label, image):
	TP = len(true_positives(label, image)[0])
	TN = len(true_negatives(label, image)[0])
	return TP / (TP + TN)

def F1Score(label, image):
	Precision = precision(label, image)
	Recall = recall(label, image)
	return (2 * Precision * Recall) / (Precision + Recall)

def true_positives(label, image):
	return np.where(np.logical_and((label == 1), (image == 1)))

def false_positives(label, image):
	return np.where(np.logical_and((label == 0), (image == 1)))

def true_negatives(label, image):
	return np.where(np.logical_and((label == 1), (image == 0)))

def false_negative(label, image):
	return np.where(np.logical_and((label == 0), (image == 0)))

# Merges together 2 images into a new one based on a homography transform from image1 to image2.
def merge_images(image1, image2, homography):
	# Find coordinates of corners
	topLeft, topRight, bottomLeft, bottomRight = [0, 0, 1], [0, image2.shape[J], 1],\
		[image2.shape[I], 0, 1], [image2.shape[I], image2.shape[J], 1]

	newTopLeft = fromHomogeneous(homography @ topLeft)
	newTopRight = fromHomogeneous(homography @ topRight)
	newBottomLeft = fromHomogeneous(homography @ bottomLeft)
	newBottomRight = fromHomogeneous(homography @ bottomRight)
	rightExtremity = max(newTopRight[J], newBottomRight[J])
	topExtremity = min(0, newTopLeft[I], newTopRight[I])
	bottomExtremity = max(image1.shape[I], newBottomLeft[I], newBottomRight[I])
	difference = bottomExtremity - topExtremity

	newImage = np.zeros((int(difference), int(rightExtremity), 3))
	newImage[-int(topExtremity) : image1.shape[I] - int(topExtremity), 0 : image1.shape[J]] = image1

	for i in range(image2.shape[I]):
		for j in range(image2.shape[J]):
			newCoords = (fromHomogeneous(homography @ (i, j, 1)) - [topExtremity, 0]).astype(int)
			newImage[newCoords[I], newCoords[J]] = image2[i, j]
	return newImage

# Very big attention to indices. Points are in (X, Y) coordinates. Images are in (I, J) (aka (Y, X)).
def isPointInImage(point, imageShape):
	return point[X] >= 0 and point[X] < imageShape[J] and point[Y] >= 0 and point[Y] < imageShape[I]

# Given an image input and a window size (expected to be bigger than the image), return the middle crop in the image.
def cropCenterImage(image, windowSize):
	diff = image.shape[0] - windowSize[0], image.shape[1] - windowSize[1]
	row_crop, column_crop = diff[0] // 2, diff[1] // 2
	crop_image = image[row_crop : -row_crop - (diff[0] % 2 == 1), column_crop : -column_crop - (diff[1] % 2 == 1)]
	assert tuple(crop_image.shape[0 : 2]) == windowSize, "image: %s window: %s crop: %s" \
		% (image.shape, windowSize, crop_image.shape)
	return crop_image	

# Given an image input and a window size, return numSamples samples of the window size from the image picked at random.
def getRandomCrops(image, windowSize, numSamples):
	assert image.shape[0] > windowSize[0] and image.shape[1] > windowSize[1]
	pickedIndexes = []
	newImages = np.zeros((numSamples, windowSize[0], windowSize[1], 3), dtype=image.dtype)
	maxIndexI, maxIndexJ = image.shape[0] - windowSize[0], image.shape[1] - windowSize[1]
	while len(pickedIndexes) < numSamples:
		startIndexI, startIndexJ = randint(0, maxIndexI - 1), randint(0, maxIndexJ - 1)
		if (startIndexI, startIndexJ) in pickedIndexes:
			continue
		index = len(pickedIndexes)
		crop = image[startIndexI : startIndexI + windowSize[0], startIndexJ : startIndexJ + windowSize[1]]
		newImages[index] = crop
		pickedIndexes.append((startIndexI, startIndexJ))
	return newImages

# Given an image and a window size, generate one by one all the possible crops, given a stride
def generateAllCrops(image, windowSize, stride):
	assert image.shape[0] > windowSize[0] and image.shape[1] > windowSize[1]
	maxIndexI, maxIndexJ = image.shape[0] - windowSize[0], image.shape[1] - windowSize[1]
	assert stride > 0 and stride < maxIndexI and stride < maxIndexJ
	for i in range(0, maxIndexI, stride):
		for j in range(0, maxIndexJ, stride):
			crop = image[i : i + windowSize[0], j : j + windowSize[1]]
			yield crop, ((i, j), (i + windowSize[0], j + windowSize[1]))

# Given an image and a window size, collect all generated crops from previous function and return them as one array
def getAllCrops(image, windowSize, stride):
	crops = []
	indexes = []
	for crop, index in generateAllCrops(image, windowSize, stride):
		crops.append(crop)
		indexes.append(index)
	return np.array(crops), np.array(indexes)
import numpy as np
from utilities.old_plotter import plot_points
from utilities.constants import *
from utilities.others import *
from utilities.data_utils import *
from mathematics.linear_algebra.solvers import solveLeastSquares

class Camera:
	def __init__(self, focalLength=1, pixelShape=(1, 1), principalPoint=(0, 0), pose=np.eye(4), \
		resolution=(1280, 1024)):
		assert pose.shape == (4, 4) and pose[3, 3] == 1
		self.focalLength = focalLength
		self.pixelShape = pixelShape
		self.principalPoint = principalPoint
		self.resolution = resolution

		# Notations: P - camera matrix, M = P[0 : 3, 0 : 3], C - camera center and P = [M | -MC].
		# P = K * E, K - intrinsic matrix, E - extrinsic matrix, E = [R | t], R - rotation matrix,
		# t - translation vector and t = -RC
		# P = (3x4), M = (3x3), E = (3x4), R = (3x3), t = (3x1), K = (3x3)
		self.K = np.array([
			[self.focalLength / self.pixelShape[X], 0, self.principalPoint[X]],
			[0, self.focalLength / self.pixelShape[Y], self.principalPoint[Y]],
			[0, 0, 1]
		])
		self.setExtrinsic(pose)

	def setExtrinsic(self, extrinsic):
		assert extrinsic.shape == (4, 4) and extrinsic[3, 3] == 1
		self.E = extrinsic
		self.R = extrinsic[0 : 3, 0 : 3]
		self.t = extrinsic[0 : 3, 3]

		# Extrinsic parameters must be inversed, because formula is: intrinsic(4x4) * Tc^-1 * P
		self.P = self.K @ np.array([[1, 0, 0 ,0], [0, 1, 0, 0], [0, 0, 1, 0]]) @ np.linalg.inv(self.E)
		self.M = self.P[0 : 3, 0 : 3]

	def setIntrinsic(self, intrinsic):
		assert intrinsic.shape == (3, 4)
		self.K = intrinsic
		# These are gone now. TODO: see if they can be recovered from the new intrinsic matrix
		self.focalLength = None
		self.pixelShape = None
		self.principalPoint = None

		# Extrinsic parameters must be inversed, because formula is: intrinsic(4x4) * Tc^-1 * P
		self.P = self.K @ np.array([[1, 0, 0 ,0], [0, 1, 0, 0], [0, 0, 1, 0]]) @ np.linalg.inv(self.E)
		self.M = self.P[0 : 3, 0 : 3]

	# Pretty print
	def __str__(self):
		return "Camera object. Focal length: {}, Pixel shape: {}. Resolution: {}. Principal point: {}. \
Translation: {}. Rotation: {}.".format(self.focalLength, self.pixelShape, self.resolution, self.principalPoint,
		list(self.translation), list(self.rotation.reshape((9, ))))

	def __repr__(self):
		return self.__str__()

	# Projects an array of world points in camera coordinates.
	def project(self, points, homogeneousResult=False):
		points = np.array(points)
		if(points.shape == (3, )):
			points = points.reshape((1, 3))
		# points must be given as columns: [1, 2, 3]', for example (1 x 3 shape) or (N x 3) for more points
		assert len(points.shape) == 2 and points.shape[1] == 3
		# Convert to homogeneous coordinates by adding a column of ones for each point
		newPoints = np.ones((points.shape[0], points.shape[1] + 1))
		newPoints[:, 0 : -1] = points
		# This is the dot product that does matrix multiplication correctly of (3 x 4) wih (N x 4) => (3 x N) result,
		#  so it must be transposed back
		dotResult = np.tensordot(self.P, newPoints, axes=([-1], [1])).T

		# Finally, convert to 2D space from homogeneous coordinates. To broadcast correctly, first array must be T
		return (dotResult[:, 0 : 2].T / dotResult[:, 2]).T if homogeneousResult == False else dotResult

	# # Given a set of corespondence points, use homography to compute the C matrix (extrinsic + intrinsic)
	def calibrateCamera(self, worldPoints, cameraPoints):
		assert len(worldPoints) == len(cameraPoints)
		assert len(worldPoints) >= 6

		A = []
		for i in range(len(worldPoints)):
			worldPoint = worldPoints[i]
			cameraPoint = cameraPoints[i]
			Xi, Yi, Zi = worldPoint[X], worldPoint[Y], worldPoint[Z]
			xi, yi = cameraPoint[X], cameraPoint[Y]
			A.append([Xi, Yi, Zi, 1, 0, 0, 0, 0, -xi * Xi, -xi * Yi, -xi * Zi, -xi])
			A.append([0, 0, 0, 0, Xi, Yi, Zi, 1, -yi * Xi, -yi * Yi, -yi * Zi, -yi])
		A = np.array(A)
		result = solveLeastSquares(A).reshape((3, 4))
		# optional: use the fact that last line can have norm 1. This actually gives identical matrix as original
		result /= np.linalg.norm(result[2, 0 : 3])

		self.P = result
		# Recompute K, E R, t and M
		self.M = self.P[0 : 3, 0 : 3]
		self.K, self.R, self.t = self.factorCameraMatrix()
		self.E = np.hstack((self.R, self.t.reshape(3, 1)))

	# Factors the camera matrix P, using the RQ algorithm to get K, R and t
	# Based on: https://github.com/marktao99/python/blob/master/CVP/samples/camera.py#L26
	def factorCameraMatrix(self):
		# M is the 3x3 component of P
		# R - upper triungular, Q - orthogonal
		R, Q = rq(self.M)
		T = np.diag(np.sign(np.diag(R)))

		_K = R @ T
		_R = T @ Q
		t = -np.linalg.inv(self.M) @ self.P[0 : 3, 3]
		return _K, _R.T, t

	# Projects in 2D space and plots a series of world points
	def plotPoints(self, worldPoints, **kwargs):
		cameraPoints = self.project(worldPoints)
		plot_points(cameraPoints, **kwargs)

	# C = -M^-1 * p4, where M is 3x3 part (p1, p2, p3) of the C matrix and p4 is 1x3 last column vector of C
	# Based on http://ksimek.github.io/2012/08/14/decompose/ and book
	def getCenter_alt(self):
		# Probably alread stored as self.t
		p4 = self.P[0 : 3, 3]
		return -np.linalg.inv(self.M) @ p4

	# From multiple geometry book, page 163
	def getCenter(self, homogeneousResult=False):
		p1 = self.P[0 : 3, 0]
		p2 = self.P[0 : 3, 1]
		p3 = self.P[0 : 3, 2]
		p4 = self.P[0 : 3, 3]
		X = np.linalg.det(np.column_stack((p2, p3, p4)))
		Y = -np.linalg.det(np.column_stack((p1, p3, p4)))
		Z = np.linalg.det(np.column_stack((p1, p2, p4)))
		T = -np.linalg.det(np.column_stack((p1, p2, p3)))
		return np.array([X/T, Y/T, Z/T]) if homogeneousResult == False else np.array(np.array([X, Y, Z, T]))

	# Algorithm from internet, also page 412, eq. 17.3 in multiple view geometry in CV book.
	def computeFundamentalFromProjection(self, otherCamera):
		X0 = otherCamera.P[np.ix_([1, 2])]
		X1 = otherCamera.P[np.ix_([2, 0])]
		X2 = otherCamera.P[np.ix_([0, 1])]
		Y0 = self.P[np.ix_([1, 2])]
		Y1 = self.P[np.ix_([2, 0])]
		Y2 = self.P[np.ix_([0, 1])]

		det00 = np.linalg.det(np.concatenate((X0, Y0)))
		det01 = np.linalg.det(np.concatenate((X0, Y1)))
		det02 = np.linalg.det(np.concatenate((X0, Y2)))
		det10 = np.linalg.det(np.concatenate((X1, Y0)))
		det11 = np.linalg.det(np.concatenate((X1, Y1)))
		det12 = np.linalg.det(np.concatenate((X1, Y2)))
		det20 = np.linalg.det(np.concatenate((X2, Y0)))
		det21 = np.linalg.det(np.concatenate((X2, Y1)))
		det22 = np.linalg.det(np.concatenate((X2, Y2)))

		fundamental = np.array([
			[det00, det01, det02],
			[det10, det11, det12],
			[det20, det21, det22]
		])
		return fundamental

	# Formula taken from Peter Corke's book, page 389. This is identical to computing fundamental from essential.
	def computeFundamentalFromParameters(self, otherCamera):
		fundamental = np.linalg.inv(otherCamera.K) @ otherCamera.computeEssential(self) @ otherCamera.K
		return fundamental

	# Essential matrix, as described in Peter Corke's book, page 390. Represents the rotation and translation pose
	#  between two cameras.
	def computeEssential(self, otherCamera):
		relativePose = self.getRelativePose(otherCamera)
		skew_translation = skewSymmetricMatrix(relativePose[0 : 3, 3])
		rotation = relativePose[0 : 3, 0 : 3]
		essential =  skew_translation @ rotation
		return essential

	# Given this camera and another camera, return the matrix that is the relative pose between the two.
	# This means dot(this.extrinsic, relativePose) == other.extrinsic
	def getRelativePose(self, otherCamera):
		extrinsicInv = np.linalg.inv(self.E)
		relativePose = np.dot(extrinsicInv, otherCamera.E)
		return relativePose

# Given N cameras and N projection points, estimate the 3D point and their scales
def solveTriangulation(cameras, points):
	assert len(cameras) == len(points)

	# See forelas4.pdf for matrix explanation. Conceptually you try to solve the basic camera formula:
	# s * xi = C * Xi => C * Xi - si * xi = 0 => M * b = 0 where M = [ Ci -xi 0 ] and b = [X s0 ... sn]
	M = np.zeros((3 * len(cameras), 4 + len(cameras)))
	for i in range(len(cameras)):
		M[3 * i : 3 * i + 3, 0 : 4] = cameras[i].P
		M[3 * i : 3 * i + 2, 4 + i] = -np.array(points[i])
		M[3 * i + 2, 4 + i] = -1
	result = solveLeastSquares(M)
	return (result[0 : 3] / result[3])[0 : 3]

# Find the fundamental matrix, given the N>=8 correspondence coordinate points.
# Solution based on multiple view geometry book, page 279-281
# @param[in] correspondencePoints A list of (x, y), (x', y') points
# @param[in] isotropicScaling Whether or not to transform the values using isotropic scaling (centroid in origin and
# @return fundamental matrix based on the correspondences
def solveFundamentalMatrix(correspondencePoints, isotropicScaling=True):
	assert len(correspondencePoints) >= 8
	correspondencePoints = np.array(correspondencePoints)
	T, _T = np.eye(3), np.eye(3)
	if isotropicScaling:
		correspondencePoints[:, 0], T = isotropicNormalizationData(correspondencePoints[:, 0])
		correspondencePoints[:, 1], _T = isotropicNormalizationData(correspondencePoints[:, 1])

	M = np.zeros((len(correspondencePoints), 9))
	for i in range(len(correspondencePoints)):
		(x, y), (_x, _y) = correspondencePoints[i]
		M[i] = _x * x, _x * y, _x, _y * x, _y * y, _y, x, y, 1
	F = solveLeastSquares(M).reshape((3, 3))
	# Use SVD on F matrix to ensure that it has rank 2 (page 280).
	# Warning, sometimes this will still give rank 3 !!
	U, S, V = np.linalg.svd(F)
	S[2] = 0
	F = U @ np.diag(S) @ V

	result = _T.T @ F @ T
	rank = np.linalg.matrix_rank(result)
	if rank != 2:
		print("Warning, fundamental matrix does not have rank 2, but", rank)

	return result

# Find the homography matrix, given the N==4 correspondence coordinate points.
# Solution based on multiple view geometry book, page 89-91
# @param[in] correspondencePoints A list of (x, y), (x', y') points
# @return homography matrix based on the correspondences
# TODO: Make work with isotropic scaling and N>4.
def solveHomography(correspondencePoints):
	assert len(correspondencePoints) == 4
	correspondencePoints = np.array(correspondencePoints)

	M = np.zeros((2 * len(correspondencePoints), 9))
	for i in range(len(correspondencePoints)):
		p1, p2 = correspondencePoints[i]
		M[2 * i] = 0, 0, 0, -p1[X], -p1[Y], -1, p1[X]*p2[Y], p1[Y]*p2[Y], p2[Y]
		M[2 * i + 1] = p1[X], p1[Y], 1, 0, 0, 0, -p1[X] * p2[X], -p1[Y] * p2[X], -p2[X]

	return solveLeastSquares(M).reshape((3, 3))

# Given a set of match points in two images, finds the best homography using RANSAC algorithm based on nmber of inliers
# @param[in] matches A list of matches [(match_img1, match_img2)]
# @param[in] countSteps Number of steps in RANSAC algorithm
# @param[in] maxDistance Maximum distance for an inlier to be accepted
# @param[in] mincCountPercent Minimum number of inliers for a model to be accepted
# @return A homography matrix (3x3) that best fits the given matches
def ransacHomography(matches, countSteps, maxDistance, minCountPercent):
	bestCount, bestHomography = 0, None
	matches = np.array(matches)
	for i in range(countSteps):
		pickedPoints = random_pick(matches, 4)
		homography = solveHomography(pickedPoints)

		# Check inliers and outliers
		res = reshapeData(toHomogeneousData(matches[:, 0]), (3, 1))
		res = reshapeData(np.matmul(homography, res), (1, 3))
		res = reshapeData(fromHomogeneousData(res), (2, ))
		res = np.abs(res - matches[:, 1])
		dist = np.linalg.norm(res, axis=1)
		inlierCount = len(np.where(dist < maxDistance)[0])
		outlierCount = len(matches) - inlierCount

		# Check if model is good (more inliers than min percent)
		if (inlierCount >= minCountPercent * len(matches) / 100) and (inlierCount > bestCount):
			bestCount, bestHomography = inlierCount, homography

	assert bestCount != 0, "No good homography could be found"
	# print("Best homography after", countSteps, "steps:", bestCount, "inliers")
	return bestHomography
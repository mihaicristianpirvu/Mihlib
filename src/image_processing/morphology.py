# morphology.py Module that implements morphological operations (dilation, erosion)
import numpy as np
from utilities.constants import *

def binary_dilation_1d(image, element):
	assert(element.shape[0] % 2 == 1)
	new_image = np.zeros(image.shape, dtype=int)

	if len(element.shape) == 1:
		assert(element.shape[0] % 2 == 1)
		half_element = int(element.shape[0] / 2)
		assert(element[half_element] == 1)
		for i in range(image.shape[I]):
			image_element_index = range(i - half_element, i + half_element + 1)
			element_i = -1
			if image[i] == element[half_element]:
				for image_i in image_element_index:
					element_i += 1
					assert(element[element_i] == 1)
					if image_i < 0 or image_i >= image.shape[I]:
						continue
					new_image[image_i] = new_image[image_i] or element[element_i]
	return new_image

def binary_dilation_2d(image, element):
	assert(element.shape[0] % 2 == 1 and element.shape[1] % 2 == 1)
	half_element = (int(element.shape[0]/2), int(element.shape[1]/2))
	assert(element[half_element[0]][half_element[1]] == 1)
	new_image = np.zeros(image.shape, dtype=int)

	for i in range(image.shape[I]):
		for j in range(image.shape[J]):
			image_element_index = (range(i - half_element[0], i + half_element[0] + 1), \
				range(j - half_element[1], j + half_element[1] + 1))
			element_i = -1
			if image[i][j] == element[half_element[0]][half_element[1]]:
				for image_i in image_element_index[0]:
					element_i += 1
					element_j = -1
					if image_i < 0 or image_i >= image.shape[I]:
						continue
					for image_j in image_element_index[1]:
						element_j += 1
						if image_j < 0 or image_j >= image.shape[J]:
							continue
						new_image[image_i][image_j] = new_image[image_i][image_j] or element[element_i][element_j]
	return new_image

# Given an image and an element, dilate the image using that element.
# Only binary dilation is supported now.
def binary_dilation(image, element):
	assert(len(image.shape) == len(element.shape))
	if len(element.shape) == 1:
		return binary_dilation_1d(image, element)
	elif len(element.shape) == 2:
		return binary_dilation_2d(image, element)
	else:
		raise RuntimeError("Only 1D and 2D dilation is supported")

def binary_erosion_1d(image, element):
	assert(element.shape[0] % 2 == 1)
	half_element = int(element.shape[0] / 2)
	new_image = np.zeros(image.shape, dtype=int)

	for i in range(image.shape[I]):
		image_element_index = range(i - half_element, i + half_element + 1)
		applyErosion = True
		element_i = -1
		for image_i in image_element_index:
			element_i += 1
			if image_i < 0 or image_i >= image.shape[I]:
				continue
			if image[image_i] != element[element_i]:
				applyErosion = False
				break
		if applyErosion == True:
			new_image[i] = new_image[i] or element[half_element]
	return new_image

def binary_erosion_2d(image, element):
	assert(element.shape[0] % 2 == 1 and element.shape[1] % 2 == 1)
	half_element = (int(element.shape[0]/2), int(element.shape[1]/2))
	new_image = np.zeros(image.shape, dtype=int)

	for i in range(image.shape[I]):
		for j in range(image.shape[J]):
			image_element_index = (range(i - half_element[0], i + half_element[0] + 1), \
				range(j - half_element[1], j + half_element[1] + 1))
			applyErosion = True
			element_i = -1
			for image_i in image_element_index[0]:
				if applyErosion == False:
					break
				element_i += 1
				element_j = -1
				if image_i < 0 or image_i >= image.shape[I]:
					continue
				for image_j in image_element_index[1]:
					if image_j < 0 or image_j >= image.shape[J]:
						continue
					if image[image_i][image_j] != element[element_i][element_j]:
						applyErosion = False
						break
			if applyErosion == True:
				new_image[i][j] = new_image[i][j] or element[half_element[0]][half_element[1]]
	return new_image

# Given an image and an element, erode that image using the element
def binary_erosion(image, element):
	assert(len(image.shape) == len(element.shape))
	if len(element.shape) == 1:
		return binary_erosion_1d(image, element)
	elif len(element.shape) == 2:
		return binary_erosion_2d(image, element)
	else:
		raise RuntimeError("Only 1D and 2D erosion is supported")

def binary_image_open(image, element_erode, element_dilate):
	return binary_dilation(binary_erosion(image, element_erode), element_dilate)

def binary_image_close(image, element_dilate, element_erode):
	return binary_erosion(binary_dilation(image, element_dilate), element_erode)
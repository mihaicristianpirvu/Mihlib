import numpy as np
from moviepy.editor import ImageSequenceClip, VideoClip
from pims import Video

def readVideo(path):
	# pims.Video is a great wrapper that does not load the entire video in memory and can access frames by indexes
	return Video(path)

def saveVideo(npData, fileName, fps, progress_bar=False, verbose=False):
    assert len(npData.shape) == 4
    assert npData.shape[1] >= 20 or npData.shape[3] >= 20, "Video too small, at lest 20 width required"
    assert npData.shape[1] == 3 or npData.shape[3] == 3, "Only RGB is supported, if grayscale, duplicate the data"
    # Move colors on last channel if needed: Tx3xHxW => TxHxWx3
    if npData.shape[1] == 3:
        npData = np.swapaxes(np.swapaxes(npData, 2, 3), 1, 3)

    newClip = ImageSequenceClip(list(npData), fps=fps)
    newClip.write_videofile(fileName, progress_bar=progress_bar, verbose=verbose, fps=fps)
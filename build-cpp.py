#!/bin/python3
import os
import sys
import subprocess
import argparse

def getCwd():
	return os.path.dirname(os.path.realpath(__file__))

def getIncludeDirectory():
	return getCwd() + os.sep + "src" + os.sep + "cpp" + os.sep + "include"

def getSrcDirectory():
	return getCwd() + os.sep + "src" + os.sep + "cpp"

def getCompileFlags():
	flags = ["-I", getIncludeDirectory(), "-Wall", "-Wextra", "-Werror", "-DNDEBUG", "-O3", "-shared", "-fPIC", \
		"-std=c++14"]
	if os.name == "nt":
		flags.append("-D_USE_MATH_DEFINES")
	return flags

def getExtension():
	return ".dll" if os.name == "nt" else ".so"

def getCompiler(args, type):
	if args.compiler == "gcc":
		if type == "cpp":
			return "g++"
		else:
			return "gcc"
	elif args.compiler == "clang":
		if type == "cpp":
			return "clang++"
		else:
			return "clang"
	else:
		return args.compiler

def buildFile(path, args, type):
	print("Building {}".format(path))

	baseName = os.path.splitext(os.path.basename(path))[0]
	basePath = os.path.dirname(path)
	extension = getExtension()

	CC = getCompiler(args, type)

	flags = [CC, path] + getCompileFlags() + ["-o", basePath + os.sep + baseName + extension]
	returnCode = subprocess.call(flags)
	assert returnCode == 0

def recursivelyBuild(path, args):
	files = os.listdir(path)

	for file in files:
		newPath = path + os.sep + file
		if file.endswith(".cpp"):
			buildFile(newPath, args, "cpp")
		elif file.endswith(".c"):
			buildFile(newPath, args, "c")
		elif not file in (".", "..", "__pycache__") and os.path.isdir(newPath):
			recursivelyBuild(newPath, args)

def recursivelyClean(path):
	files = os.listdir(path)

	for file in files:
		newPath = path + os.sep + file
		if file.endswith(getExtension()):
			print("Removing {}".format(newPath))
			os.unlink(newPath)
		elif not file in (".", "..", "__pycache__") and os.path.isdir(newPath):
			recursivelyClean(newPath)

def parseArguments():
	parser = argparse.ArgumentParser()
	parser.add_argument("--type", type=str, help="Type to run: build/clean/install. Default: build", default="build")
	parser.add_argument("--compiler", type=str, help="Which compiler is used to build CPP files. Default: gcc", \
		default="gcc")

	args = parser.parse_args()
	assert args.type in ("build", "clean", "install")
	assert args.compiler in ("gcc", "clang", "icc")
	return args

def main():
	args = parseArguments()	
	print("Compiler is: {}".format(args.compiler))

	if args.type == "build":
		recursivelyBuild(getSrcDirectory(), args)
	elif args.type == "clean":
		recursivelyClean(getSrcDirectory())
	else:
		print("Install lib in system is NYI")
		pass

if __name__ == "__main__":
	main()